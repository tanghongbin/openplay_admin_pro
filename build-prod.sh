# export PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/Applications/Server.app/Contents/ServerRoot/usr/bin:/Applications/Server.app/Contents/ServerRoot/usr/sbin
#变量定义
PROJECTNAME="uptownfunk"
SPONIAWORKSPACE="uptownfunk.xcworkspace"
BUILDSCEME="uptownfunk_inhouse"
PPFILE="adminpro_inhouse_diss"
IPA_PREFIX="OpenPlay_ap"
JK_IPA_DIR="ap_ios"
BUNDLE_ID="com.sponia.adminpro"
WORKSPACE="."
BUILD_NUMBER=$1
IPADIR="../../ios_ipa/admin-pro"

export LANG=en_US.UTF-8
# pod install --verbose --no-repo-update

#修改工程build number
 /usr/libexec/PlistBuddy -c "Set :CFBundleVersion "$BUILD_NUMBER $WORKSPACE"/"$BUILDSCEME".plist"

#运行测试
#xcodebuild -sdk iphonesimulator -workspace $SPONIAWORKSPACE -scheme $TESTTSCHEME -configuration Debug RUN_APPLICATION_TESTS_WITH_IOS_SIM=YES ONLY_ACTIVE_ARCH=NO clean test | ocunit2junit
#打包
xcodebuild -workspace $SPONIAWORKSPACE -scheme $BUILDSCEME archive -archivePath $WORKSPACE"/build/"$BUILD_NUMBER"/"$PROJECTNAME""
xcodebuild -exportArchive -exportFormat IPA -archivePath $WORKSPACE"/build/"$BUILD_NUMBER"/"$PROJECTNAME".xcarchive" -exportPath $IPADIR"/"$IPA_PREFIX"_"$BUILD_NUMBER"_product.ipa" -exportProvisioningProfile "$PPFILE"


ipaurl="https://jk.sponia.com/"$JK_IPA_DIR"/"$IPA_PREFIX"_"$BUILD_NUMBER"_product.ipa"

cat << EOF > $IPADIR"/"$IPA_PREFIX"_"$BUILD_NUMBER"_product.plist"
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>items</key>
  <array>
    <dict>
      <key>assets</key>
      <array>
        <dict>
          <key>kind</key>
          <string>software-package</string>
          <key>url</key>
          <string>$ipaurl</string>
        </dict>
      </array>
      <key>metadata</key>
      <dict>
        <key>bundle-identifier</key>
        <string>$BUNDLE_ID</string>
        <key>bundle-version</key>
        <string>1.0</string>
        <key>kind</key>
        <string>software</string>
        <key>title</key>
        <string>Admin pro</string>
      </dict>
    </dict>
  </array>
</dict>
</plist>
EOF
