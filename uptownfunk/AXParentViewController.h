//
//  AXParentViewController.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/28.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXMatch.h"

@interface AXParentViewController : UIViewController
@property (nonatomic, strong)AXMatch *match;
@end
