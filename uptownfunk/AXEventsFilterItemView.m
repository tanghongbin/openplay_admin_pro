//
//  AXEventsFilterItemView.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/29.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXEventsFilterItemView.h"
typedef void(^selected)(AXEventsFilterItemView* item);
#pragma mark - AXEventsFilterItemView
static inline float radians(double degrees) {return degrees * M_PI / 180;}
@interface AXEventsFilterItemView()
@property (nonatomic, strong)UIButton *selectedButton;
@property (nonatomic, strong)UILabel *itemTitleLabel;
@property (nonatomic, strong)UIImageView *indicatorImageView;

@property (nonatomic, strong)UIView *containerView;
@property (nonatomic, strong)UIView *seperateView;

@property (nonatomic, copy)selected selected;
@end

@implementation AXEventsFilterItemView

-(instancetype)initWithFrame:(CGRect)frame Selected:(selected)selected
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.containerView];
        [_containerView addSubview:self.seperateView];
        [_containerView addSubview:self.itemTitleLabel];
        [_containerView addSubview:self.indicatorImageView];
        [_containerView addSubview:self.selectedButton];
        [_containerView addSubview:self.seperateView];
        
        self.selected = selected;
    }
    return self;
}

#pragma matk - Setter

-(void)setItemTitle:(NSString *)itemTitle
{
    _itemTitle = itemTitle;
    [_itemTitleLabel setText:_itemTitle];
}

-(void)setIsOpened:(BOOL)isOpened
{
    _isOpened = isOpened;
    if (_isOpened) {
        [UIView animateWithDuration:0.3 animations:^{
            [_indicatorImageView setTransform:CGAffineTransformMakeRotation(radians(-180))];
        }];
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            [_indicatorImageView setTransform:CGAffineTransformIdentity];
        }];
    }
}
#pragma mark - Getter
-(UIView *)containerView
{
    if (!_containerView) {
        _containerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.width, self.height)];
        [_containerView setBackgroundColor:[UIColor defaultTextColor]];
    }
    return _containerView;
}

-(UILabel *)itemTitleLabel
{
    if (!_itemTitleLabel) {
        _itemTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.width - 20, _containerView.height)];
        [_itemTitleLabel setCenter:CGPointMake(_containerView.width/2 - 10, _containerView.height/2)];
        [_itemTitleLabel setTextAlignment:NSTextAlignmentRight];
        [_itemTitleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [_itemTitleLabel setTextColor:[UIColor defaultBackGroundColor]];
    }
    return _itemTitleLabel;
}

-(UIImageView *)indicatorImageView
{
    if (!_indicatorImageView) {
        _indicatorImageView = [[UIImageView alloc]initWithFrame:CGRectMake(_itemTitleLabel.x + _itemTitleLabel.width + 5, 15, 10, 10)];
        [_indicatorImageView setImage:[UIImage imageNamed:@"Common_Icon_Arrow_down"]];
    }
    return _indicatorImageView;
}

-(UIButton *)selectedButton
{
    if (!_selectedButton) {
        _selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selectedButton setFrame:CGRectMake(0, 0, _containerView.width, _containerView.height)];
        [_selectedButton setBackgroundImage:[UIImage imageFromColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.8]] forState:UIControlStateSelected];
        [_selectedButton setBackgroundImage:[UIImage imageFromColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.8]] forState:UIControlStateHighlighted];
        [_selectedButton setBackgroundImage:[UIImage imageFromColor:[UIColor clearColor]] forState:UIControlStateNormal];
        [_selectedButton bk_addEventHandler:^(UIButton* sender) {
            [self.containerView endEditing:YES];
            if (_isOpened) {
                self.isOpened = NO;
                if (_selected) {
                    _selected(self);
                }
                return ;
            }else{
                self.isOpened = YES;
                if (_selected) {
                    _selected(self);
                }
                
            }
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectedButton;
}

-(UIView *)seperateView
{
    if (!_seperateView) {
        _seperateView = [[UIView alloc]initWithFrame:CGRectMake(0, self.height - 2, self.width, 2)];
        [_seperateView setBackgroundColor:UIColorWithRGB(0x4a4a4a)];
    }
    return _seperateView;
}

@end