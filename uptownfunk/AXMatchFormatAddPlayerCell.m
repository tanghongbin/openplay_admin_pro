//
//  AXMatchFormatAddPlayerCell.m
//  uptownfunk
//
//  Created by yellow on 16/4/22.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXMatchFormatAddPlayerCell.h"

@implementation AXMatchFormatAddPlayerCell

- (void)awakeFromNib
{
    self.addBtn.layer.cornerRadius = 40;
    self.addBtn.layer.masksToBounds = YES;
}
@end
