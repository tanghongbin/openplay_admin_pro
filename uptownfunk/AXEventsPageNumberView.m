//
//  AXEventsPageNumberView.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/13.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#define containerPageSize 8
#define pageSize 60

#import "AXEventsPageNumberView.h"
@interface AXEventsPageNumberView()
@property (nonatomic, strong)UIScrollView *containerScrollView;
@property (nonatomic, strong)UIView *onView;
@property (nonatomic, assign)CGFloat pageButtonWidth;

@property (nonatomic, strong)NSMutableArray<UIButton*> *pageButtonArray;
@end
@implementation AXEventsPageNumberView

- (instancetype)initWithFrame:(CGRect)frame onView:(UIView*)onView
{
    self = [super initWithFrame:frame];
    if (self) {
        self.pageButtonWidth = onView.width/containerPageSize;
        self.onView = onView;
        [self addSubview:self.containerScrollView];
    }
    return self;
}

-(void)setTotal:(NSInteger)total
{
    _total = total;
    NSInteger pageCount = total/pageSize;
    if (total%pageSize != 0) {
        pageCount ++;
    }
    
    NSInteger containerScrollViewPage = pageCount/containerPageSize;
    if (pageCount%containerPageSize != 0) {
        containerScrollViewPage ++;
    }
    
    [_pageButtonArray enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
        obj = nil;
    }];
    [_containerScrollView setContentSize:CGSizeMake(containerScrollViewPage*_onView.width, self.height)];
    
    NSMutableArray *btnArray = [NSMutableArray new];
    for (int i = 0; i < pageCount; i ++) {
        UIButton *btn = [self pageButton:i+1];
        [btn setX: btn.width * i ];
        [_containerScrollView addSubview:btn];
        [btnArray addObject:btn];
    }
    self.pageButtonArray = [NSMutableArray arrayWithArray:btnArray];
    
}

-(void)setSelectedPage:(NSInteger)selectedPage
{
    _selectedPage = selectedPage;
    
    [_pageButtonArray enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.tag == selectedPage) {
            [obj setSelected:YES];
        }else{
            [obj setSelected:NO];
        }
    }];
}

#pragma mark - Getter 

-(NSMutableArray *)pageButtonArray
{
    if (!_pageButtonArray) {
        _pageButtonArray = [NSMutableArray new];
    }
    return _pageButtonArray;
}

-(UIScrollView *)containerScrollView
{
    if (!_containerScrollView) {
        _containerScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, _onView.width, 40)];
        [_containerScrollView setPagingEnabled:YES];
        [_containerScrollView setContentSize:CGSizeMake(_onView.width, 40)];
        [_containerScrollView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.86]];
    }
    return _containerScrollView;
}

-(UIButton*)pageButton:(NSInteger)page
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, _pageButtonWidth, self.height)];
    [btn setTitle:[NSString stringWithFormat:@"%ld",page] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn.layer setBorderWidth:2];
    [btn.layer setBorderColor:[[UIColor defaultBackGroundColor] colorWithAlphaComponent:0.65].CGColor];
    [btn setBackgroundImage:[UIImage imageNamed:@"opf_btn_delete_bg_highlight"] forState:UIControlStateHighlighted];
    [btn setBackgroundImage:[UIImage imageNamed:@"opf_btn_delete_bg_highlight"] forState:UIControlStateSelected];
    [btn setTag:page];
    
    [btn bk_addEventHandler:^(UIButton* button) {
        if (button.isSelected) {
            return ;
        }
        [button setSelected:YES];
        [_pageButtonArray enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj != button) {
                [obj setSelected:NO];
            }
        }];
        
        if ([_delegate respondsToSelector:@selector(pageNumberView:didSelectedPage:)]) {
            [_delegate pageNumberView:self didSelectedPage:button.tag];
        }
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}

@end
