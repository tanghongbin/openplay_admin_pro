//
//  AXEventEditViewController.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/4.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXEvent.h"
#import "AXMatch.h"
@interface AXEventEditViewController : UIViewController
@property (nonatomic, strong)AXEvent *event;
@property (nonatomic, strong)AXMatch *match;
@end
