//
//  AXSplitViewController.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXSplitViewController : UISplitViewController<UISplitViewControllerDelegate>

@end
