//
//  AXEventsFilterItemCell.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/4.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXEventsFilterItemCell : UITableViewCell
@property (nonatomic, strong)UILabel *teamLabel;
@end
