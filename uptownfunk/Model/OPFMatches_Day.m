//
//  OPFBaseClass.m
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "OPFMatches_Day.h"
#import "AXMatch.h"


NSString *const kOPFBaseClassList = @"list";
NSString *const kOPFBaseClassLastId = @"last_id";
NSString *const kOPFBaseClassLastLeft = @"last_left";
NSString *const kOPFBaseClassTotal = @"total";


@interface OPFMatches_Day ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OPFMatches_Day

@synthesize list = _list;
@synthesize lastId = _lastId;
@synthesize lastLeft = _lastLeft;
@synthesize total = _total;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedOPFList = [dict objectForKey:kOPFBaseClassList];
    NSMutableArray *parsedOPFList = [NSMutableArray array];
    if ([receivedOPFList isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedOPFList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedOPFList addObject:[AXMatch modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedOPFList isKindOfClass:[NSDictionary class]]) {
       [parsedOPFList addObject:[AXMatch modelObjectWithDictionary:(NSDictionary *)receivedOPFList]];
    }

    self.list = [NSArray arrayWithArray:parsedOPFList];
            self.lastId = [[self objectOrNilForKey:kOPFBaseClassLastId fromDictionary:dict] doubleValue];
            self.lastLeft = [[self objectOrNilForKey:kOPFBaseClassLastLeft fromDictionary:dict] doubleValue];
            self.total = [[self objectOrNilForKey:kOPFBaseClassTotal fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForList = [NSMutableArray array];
    for (NSObject *subArrayObject in self.list) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForList addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForList addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForList] forKey:kOPFBaseClassList];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lastId] forKey:kOPFBaseClassLastId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lastLeft] forKey:kOPFBaseClassLastLeft];
    [mutableDict setValue:[NSNumber numberWithDouble:self.total] forKey:kOPFBaseClassTotal];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.list = [aDecoder decodeObjectForKey:kOPFBaseClassList];
    self.lastId = [aDecoder decodeDoubleForKey:kOPFBaseClassLastId];
    self.lastLeft = [aDecoder decodeDoubleForKey:kOPFBaseClassLastLeft];
    self.total = [aDecoder decodeDoubleForKey:kOPFBaseClassTotal];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_list forKey:kOPFBaseClassList];
    [aCoder encodeDouble:_lastId forKey:kOPFBaseClassLastId];
    [aCoder encodeDouble:_lastLeft forKey:kOPFBaseClassLastLeft];
    [aCoder encodeDouble:_total forKey:kOPFBaseClassTotal];
}

- (id)copyWithZone:(NSZone *)zone
{
    OPFMatches_Day *copy = [[OPFMatches_Day alloc] init];
    
    if (copy) {

        copy.list = [self.list copyWithZone:zone];
        copy.lastId = self.lastId;
        copy.lastLeft = self.lastLeft;
        copy.total = self.total;
    }
    
    return copy;
}


@end
