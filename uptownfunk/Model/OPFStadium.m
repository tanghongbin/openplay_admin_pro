//
//  OPFStadium.m
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "OPFStadium.h"
#import "OPFVenues.h"
#import "OPFArea.h"


NSString *const kOPFStadiumVenues = @"venues";
NSString *const kOPFStadiumArea = @"area";
NSString *const kOPFStadiumLongitude = @"longitude";
NSString *const kOPFStadiumId = @"id";
NSString *const kOPFStadiumMapName = @"map_name";
NSString *const kOPFStadiumAddress = @"address";
NSString *const kOPFStadiumLatitude = @"latitude";
NSString *const kOPFStadiumType = @"type";
NSString *const kOPFStadiumName = @"name";


@interface OPFStadium ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OPFStadium

@synthesize venues = _venues;
@synthesize area = _area;
@synthesize longitude = _longitude;
@synthesize stadiumIdentifier = _stadiumIdentifier;
@synthesize mapName = _mapName;
@synthesize address = _address;
@synthesize latitude = _latitude;
@synthesize type = _type;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedOPFVenues = [dict objectForKey:kOPFStadiumVenues];
    NSMutableArray *parsedOPFVenues = [NSMutableArray array];
    if ([receivedOPFVenues isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedOPFVenues) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedOPFVenues addObject:[OPFVenues modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedOPFVenues isKindOfClass:[NSDictionary class]]) {
       [parsedOPFVenues addObject:[OPFVenues modelObjectWithDictionary:(NSDictionary *)receivedOPFVenues]];
    }

    self.venues = [NSArray arrayWithArray:parsedOPFVenues];
            self.area = [OPFArea modelObjectWithDictionary:[dict objectForKey:kOPFStadiumArea]];
            self.longitude = [[self objectOrNilForKey:kOPFStadiumLongitude fromDictionary:dict] doubleValue];
            self.stadiumIdentifier = [self objectOrNilForKey:kOPFStadiumId fromDictionary:dict];
            self.mapName = [self objectOrNilForKey:kOPFStadiumMapName fromDictionary:dict];
            self.address = [self objectOrNilForKey:kOPFStadiumAddress fromDictionary:dict];
            self.latitude = [[self objectOrNilForKey:kOPFStadiumLatitude fromDictionary:dict] doubleValue];
            self.type = [[self objectOrNilForKey:kOPFStadiumType fromDictionary:dict] doubleValue];
            self.name = [self objectOrNilForKey:kOPFStadiumName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForVenues = [NSMutableArray array];
    for (NSObject *subArrayObject in self.venues) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForVenues addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForVenues addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForVenues] forKey:kOPFStadiumVenues];
    [mutableDict setValue:[self.area dictionaryRepresentation] forKey:kOPFStadiumArea];
    [mutableDict setValue:[NSNumber numberWithDouble:self.longitude] forKey:kOPFStadiumLongitude];
    [mutableDict setValue:self.stadiumIdentifier forKey:kOPFStadiumId];
    [mutableDict setValue:self.mapName forKey:kOPFStadiumMapName];
    [mutableDict setValue:self.address forKey:kOPFStadiumAddress];
    [mutableDict setValue:[NSNumber numberWithDouble:self.latitude] forKey:kOPFStadiumLatitude];
    [mutableDict setValue:[NSNumber numberWithDouble:self.type] forKey:kOPFStadiumType];
    [mutableDict setValue:self.name forKey:kOPFStadiumName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.venues = [aDecoder decodeObjectForKey:kOPFStadiumVenues];
    self.area = [aDecoder decodeObjectForKey:kOPFStadiumArea];
    self.longitude = [aDecoder decodeDoubleForKey:kOPFStadiumLongitude];
    self.stadiumIdentifier = [aDecoder decodeObjectForKey:kOPFStadiumId];
    self.mapName = [aDecoder decodeObjectForKey:kOPFStadiumMapName];
    self.address = [aDecoder decodeObjectForKey:kOPFStadiumAddress];
    self.latitude = [aDecoder decodeDoubleForKey:kOPFStadiumLatitude];
    self.type = [aDecoder decodeDoubleForKey:kOPFStadiumType];
    self.name = [aDecoder decodeObjectForKey:kOPFStadiumName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_venues forKey:kOPFStadiumVenues];
    [aCoder encodeObject:_area forKey:kOPFStadiumArea];
    [aCoder encodeDouble:_longitude forKey:kOPFStadiumLongitude];
    [aCoder encodeObject:_stadiumIdentifier forKey:kOPFStadiumId];
    [aCoder encodeObject:_mapName forKey:kOPFStadiumMapName];
    [aCoder encodeObject:_address forKey:kOPFStadiumAddress];
    [aCoder encodeDouble:_latitude forKey:kOPFStadiumLatitude];
    [aCoder encodeDouble:_type forKey:kOPFStadiumType];
    [aCoder encodeObject:_name forKey:kOPFStadiumName];
}

- (id)copyWithZone:(NSZone *)zone
{
    OPFStadium *copy = [[OPFStadium alloc] init];
    
    if (copy) {

        copy.venues = [self.venues copyWithZone:zone];
        copy.area = [self.area copyWithZone:zone];
        copy.longitude = self.longitude;
        copy.stadiumIdentifier = [self.stadiumIdentifier copyWithZone:zone];
        copy.mapName = [self.mapName copyWithZone:zone];
        copy.address = [self.address copyWithZone:zone];
        copy.latitude = self.latitude;
        copy.type = self.type;
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}


@end
