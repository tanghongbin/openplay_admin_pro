//
//  OPFStadium.h
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OPFArea;

@interface OPFStadium : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *venues;
@property (nonatomic, strong) OPFArea *area;
@property (nonatomic, assign) double longitude;
@property (nonatomic, strong) NSString *stadiumIdentifier;
@property (nonatomic, strong) NSString *mapName;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double type;
@property (nonatomic, strong) NSString *name;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
