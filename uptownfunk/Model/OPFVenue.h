//
//  OPFVenue.h
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OPFStadium;

@interface OPFVenue : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *venueIdentifier;
@property (nonatomic, strong) OPFStadium *stadium;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double type;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
