//
//  OPLDataManager.h
//  OpenPlayer
//
//  Created by laihj on 3/26/15.
//  Copyright (c) 2015 sponia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AXUser.h"

typedef NS_ENUM(NSInteger, HTTPRequestMethod) {
    HTTPRequestMethodGET    = 1,
    HTTPRequestMethodPOST   = 2,
    HTTPRequestMethodPUT    = 3,
    HTTPRequestMethodDelete = 4
};

extern NSString *const kAPIParaTeamIDKey;
extern NSString *const kAPIParaYearKey;
extern NSString *const kAPIParaCompetitionIDKey;
extern NSString *const kAPIParaScheduleModel;

@interface OPLDataManager : NSObject

@property (nonatomic, strong) AXUser *user;

+ (OPLDataManager *) manager;

- (void) logout;

- (NSURLSessionDataTask *) userLoginWithEmail:(NSString *) email
                                     password:(NSString *) password
                                     response:(void (^)(AXUser *user,NSString *errorMessage))response;

- (NSURLSessionDataTask *) fetchMatches:(NSString*)date
                               response:(void(^)(id matches, NSString* errorMessage))response;

- (NSURLSessionDataTask *) fetchFormations:(NSString*)matchId
                                  response:(void(^)(id matches, NSString* errorMessage))response;

- (NSURLSessionDataTask *) saveFormations:(NSString*)matchId datas:(NSArray*)datas
                                 response:(void(^)(id matches, NSString* errorMessage))response;

- (NSURLSessionDataTask *) findMatchPlayers:(NSString*)text
                                   response:(void(^)(id matches, NSString* errorMessage))response;

- (NSURLSessionDataTask *) addMatchPlayer:(NSDictionary *)data teamId:(NSString *)teamId
                                 response:(void(^)(id matches, NSString* errorMessage))response;

- (NSURLSessionDataTask *) fetchTestMatchInfoWithMatchId:(NSString*)matchId
                                                response:(void(^)(id matches, NSString* errorMessage))response;

- (NSURLSessionDataTask *) saveTestMatchInfoWithMatchId:(NSString*)matchId data:(NSDictionary *)data
                                               response:(void(^)(id matches, NSString* errorMessage))response;

- (NSURLSessionDataTask *) fetchEventsQueriesWith:(NSString*)matchId
                                         response:(void(^)(id queries, NSString* errorMessage))response;

-(NSURLSessionDataTask *)fetchEventsWith:(NSString *)matchId
                                     pageNumber:(NSInteger)pageNumber
                                       pageSize:(NSInteger)pageSize
                                filterItemArray:(NSArray*)fArray
                                       response:(void (^)(id queries, NSString *errorMessage))response;

-(NSURLSessionDataTask *)addEventWithMatchId:(NSString*)matchId
                                         data:(id)data
                                     response:(void (^)(id event, NSString *errorMessage))response;

-(NSURLSessionDataTask *)saveEventWithEventId:(NSString*)eventId
                                         data:(NSDictionary *)data
                                     response:(void (^)(id event, NSString *errorMessage))response;

-(NSURLSessionDataTask *)deleteEventWithEventId:(NSString*)eventId
                                     response:(void (^)(id event, NSString *errorMessage))response;

-(NSURLSessionDataTask *)addTestMatchWithData:(id)data
                                    response:(void (^)(id match, NSString *errorMessage))response;

-(NSURLSessionDataTask *)saveStatisticiansWithMatchId:(NSString*)matchId
                                                 data:(id)data
                                     response:(void (^)(id data, NSString *errorMessage))response;

-(NSURLSessionDataTask *)searchStatisticiansWithKey:(NSString*)key
                                             response:(void (^)(id data, NSString *errorMessage))response;

@end
