//
//  AXFormation.m
//
//  Created by Kelvin Tong on 16/5/9
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXFormation.h"
#import "AXTeam.h"
#import "AXArea.h"


NSString *const kAXFormationTeam = @"team";
NSString *const kAXFormationWeight = @"weight";
NSString *const kAXFormationStatus = @"status";
NSString *const kAXFormationCountryCode = @"country_code";
NSString *const kAXFormationAccountRoles = @"account_roles";
NSString *const kAXFormationUpdatedAt = @"updated_at";
NSString *const kAXFormationIdCard = @"id_card";
NSString *const kAXFormationMatchId = @"match_id";
NSString *const kAXFormationNationality = @"nationality";
NSString *const kAXFormationAvatarUri = @"avatar_uri";
NSString *const kAXFormationName = @"name";
NSString *const kAXFormationPassport = @"passport";
NSString *const kAXFormationId = @"id";
NSString *const kAXFormationRoles = @"roles";
NSString *const kAXFormationBirth = @"birth";
NSString *const kAXFormationGender = @"gender";
NSString *const kAXFormationEmail = @"email";
NSString *const kAXFormationPhone = @"phone";
NSString *const kAXFormationHeight = @"height";
NSString *const kAXFormationShirtNumber = @"shirt_number";
NSString *const kAXFormationArea = @"area";
NSString *const kAXFormationStatAreas = @"stat_areas";
NSString *const kAXFormationCreatedAt = @"created_at";
NSString *const kAXFormationRelationId = @"relation_id";
NSString *const kAXFormationTeamId = @"team_id";
NSString *const kAXFormationDescription = @"description";


@interface AXFormation ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXFormation

@synthesize team = _team;
@synthesize weight = _weight;
@synthesize status = _status;
@synthesize countryCode = _countryCode;
@synthesize accountRoles = _accountRoles;
@synthesize updatedAt = _updatedAt;
@synthesize idCard = _idCard;
@synthesize matchId = _matchId;
@synthesize nationality = _nationality;
@synthesize avatarUri = _avatarUri;
@synthesize name = _name;
@synthesize passport = _passport;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize roles = _roles;
@synthesize birth = _birth;
@synthesize gender = _gender;
@synthesize email = _email;
@synthesize phone = _phone;
@synthesize height = _height;
@synthesize shirtNumber = _shirtNumber;
@synthesize area = _area;
@synthesize statAreas = _statAreas;
@synthesize createdAt = _createdAt;
@synthesize relationId = _relationId;
@synthesize teamId = _teamId;
@synthesize internalBaseClassDescription = _internalBaseClassDescription;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.team = [AXTeam modelObjectWithDictionary:[dict objectForKey:kAXFormationTeam]];
            self.weight = [[self objectOrNilForKey:kAXFormationWeight fromDictionary:dict] doubleValue];
            self.status = [[self objectOrNilForKey:kAXFormationStatus fromDictionary:dict] doubleValue];
            self.countryCode = [self objectOrNilForKey:kAXFormationCountryCode fromDictionary:dict];
            self.accountRoles = [self objectOrNilForKey:kAXFormationAccountRoles fromDictionary:dict];
            self.updatedAt = [self objectOrNilForKey:kAXFormationUpdatedAt fromDictionary:dict];
            self.idCard = [self objectOrNilForKey:kAXFormationIdCard fromDictionary:dict];
            self.matchId = [self objectOrNilForKey:kAXFormationMatchId fromDictionary:dict];
            self.nationality = [self objectOrNilForKey:kAXFormationNationality fromDictionary:dict];
            self.avatarUri = [self objectOrNilForKey:kAXFormationAvatarUri fromDictionary:dict];
            self.name = [self objectOrNilForKey:kAXFormationName fromDictionary:dict];
            self.passport = [self objectOrNilForKey:kAXFormationPassport fromDictionary:dict];
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kAXFormationId fromDictionary:dict];
            self.roles = [self objectOrNilForKey:kAXFormationRoles fromDictionary:dict];
            self.birth = [self objectOrNilForKey:kAXFormationBirth fromDictionary:dict];
            self.gender = [[self objectOrNilForKey:kAXFormationGender fromDictionary:dict] doubleValue];
            self.email = [self objectOrNilForKey:kAXFormationEmail fromDictionary:dict];
            self.phone = [self objectOrNilForKey:kAXFormationPhone fromDictionary:dict];
            self.height = [[self objectOrNilForKey:kAXFormationHeight fromDictionary:dict] doubleValue];
            self.shirtNumber = [[self objectOrNilForKey:kAXFormationShirtNumber fromDictionary:dict] doubleValue];
            self.area = [AXArea modelObjectWithDictionary:[dict objectForKey:kAXFormationArea]];
            self.statAreas = [self objectOrNilForKey:kAXFormationStatAreas fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kAXFormationCreatedAt fromDictionary:dict];
            self.relationId = [self objectOrNilForKey:kAXFormationRelationId fromDictionary:dict];
            self.teamId = [self objectOrNilForKey:kAXFormationTeamId fromDictionary:dict];
            self.internalBaseClassDescription = [self objectOrNilForKey:kAXFormationDescription fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.team dictionaryRepresentation] forKey:kAXFormationTeam];
    [mutableDict setValue:[NSNumber numberWithDouble:self.weight] forKey:kAXFormationWeight];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kAXFormationStatus];
    [mutableDict setValue:self.countryCode forKey:kAXFormationCountryCode];
    NSMutableArray *tempArrayForAccountRoles = [NSMutableArray array];
    for (NSObject *subArrayObject in self.accountRoles) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForAccountRoles addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForAccountRoles addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForAccountRoles] forKey:kAXFormationAccountRoles];
    [mutableDict setValue:self.updatedAt forKey:kAXFormationUpdatedAt];
    [mutableDict setValue:self.idCard forKey:kAXFormationIdCard];
    [mutableDict setValue:self.matchId forKey:kAXFormationMatchId];
    [mutableDict setValue:self.nationality forKey:kAXFormationNationality];
    [mutableDict setValue:self.avatarUri forKey:kAXFormationAvatarUri];
    [mutableDict setValue:self.name forKey:kAXFormationName];
    [mutableDict setValue:self.passport forKey:kAXFormationPassport];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kAXFormationId];
    NSMutableArray *tempArrayForRoles = [NSMutableArray array];
    for (NSObject *subArrayObject in self.roles) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForRoles addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForRoles addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForRoles] forKey:kAXFormationRoles];
    [mutableDict setValue:self.birth forKey:kAXFormationBirth];
    [mutableDict setValue:[NSNumber numberWithDouble:self.gender] forKey:kAXFormationGender];
    [mutableDict setValue:self.email forKey:kAXFormationEmail];
    [mutableDict setValue:self.phone forKey:kAXFormationPhone];
    [mutableDict setValue:[NSNumber numberWithDouble:self.height] forKey:kAXFormationHeight];
    [mutableDict setValue:[NSNumber numberWithDouble:self.shirtNumber] forKey:kAXFormationShirtNumber];
    [mutableDict setValue:[self.area dictionaryRepresentation] forKey:kAXFormationArea];
    NSMutableArray *tempArrayForStatAreas = [NSMutableArray array];
    for (NSObject *subArrayObject in self.statAreas) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForStatAreas addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForStatAreas addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForStatAreas] forKey:kAXFormationStatAreas];
    [mutableDict setValue:self.createdAt forKey:kAXFormationCreatedAt];
    [mutableDict setValue:self.relationId forKey:kAXFormationRelationId];
    [mutableDict setValue:self.teamId forKey:kAXFormationTeamId];
    [mutableDict setValue:self.internalBaseClassDescription forKey:kAXFormationDescription];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.team = [aDecoder decodeObjectForKey:kAXFormationTeam];
    self.weight = [aDecoder decodeDoubleForKey:kAXFormationWeight];
    self.status = [aDecoder decodeDoubleForKey:kAXFormationStatus];
    self.countryCode = [aDecoder decodeObjectForKey:kAXFormationCountryCode];
    self.accountRoles = [aDecoder decodeObjectForKey:kAXFormationAccountRoles];
    self.updatedAt = [aDecoder decodeObjectForKey:kAXFormationUpdatedAt];
    self.idCard = [aDecoder decodeObjectForKey:kAXFormationIdCard];
    self.matchId = [aDecoder decodeObjectForKey:kAXFormationMatchId];
    self.nationality = [aDecoder decodeObjectForKey:kAXFormationNationality];
    self.avatarUri = [aDecoder decodeObjectForKey:kAXFormationAvatarUri];
    self.name = [aDecoder decodeObjectForKey:kAXFormationName];
    self.passport = [aDecoder decodeObjectForKey:kAXFormationPassport];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kAXFormationId];
    self.roles = [aDecoder decodeObjectForKey:kAXFormationRoles];
    self.birth = [aDecoder decodeObjectForKey:kAXFormationBirth];
    self.gender = [aDecoder decodeDoubleForKey:kAXFormationGender];
    self.email = [aDecoder decodeObjectForKey:kAXFormationEmail];
    self.phone = [aDecoder decodeObjectForKey:kAXFormationPhone];
    self.height = [aDecoder decodeDoubleForKey:kAXFormationHeight];
    self.shirtNumber = [aDecoder decodeDoubleForKey:kAXFormationShirtNumber];
    self.area = [aDecoder decodeObjectForKey:kAXFormationArea];
    self.statAreas = [aDecoder decodeObjectForKey:kAXFormationStatAreas];
    self.createdAt = [aDecoder decodeObjectForKey:kAXFormationCreatedAt];
    self.relationId = [aDecoder decodeObjectForKey:kAXFormationRelationId];
    self.teamId = [aDecoder decodeObjectForKey:kAXFormationTeamId];
    self.internalBaseClassDescription = [aDecoder decodeObjectForKey:kAXFormationDescription];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_team forKey:kAXFormationTeam];
    [aCoder encodeDouble:_weight forKey:kAXFormationWeight];
    [aCoder encodeDouble:_status forKey:kAXFormationStatus];
    [aCoder encodeObject:_countryCode forKey:kAXFormationCountryCode];
    [aCoder encodeObject:_accountRoles forKey:kAXFormationAccountRoles];
    [aCoder encodeObject:_updatedAt forKey:kAXFormationUpdatedAt];
    [aCoder encodeObject:_idCard forKey:kAXFormationIdCard];
    [aCoder encodeObject:_matchId forKey:kAXFormationMatchId];
    [aCoder encodeObject:_nationality forKey:kAXFormationNationality];
    [aCoder encodeObject:_avatarUri forKey:kAXFormationAvatarUri];
    [aCoder encodeObject:_name forKey:kAXFormationName];
    [aCoder encodeObject:_passport forKey:kAXFormationPassport];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kAXFormationId];
    [aCoder encodeObject:_roles forKey:kAXFormationRoles];
    [aCoder encodeObject:_birth forKey:kAXFormationBirth];
    [aCoder encodeDouble:_gender forKey:kAXFormationGender];
    [aCoder encodeObject:_email forKey:kAXFormationEmail];
    [aCoder encodeObject:_phone forKey:kAXFormationPhone];
    [aCoder encodeDouble:_height forKey:kAXFormationHeight];
    [aCoder encodeDouble:_shirtNumber forKey:kAXFormationShirtNumber];
    [aCoder encodeObject:_area forKey:kAXFormationArea];
    [aCoder encodeObject:_statAreas forKey:kAXFormationStatAreas];
    [aCoder encodeObject:_createdAt forKey:kAXFormationCreatedAt];
    [aCoder encodeObject:_relationId forKey:kAXFormationRelationId];
    [aCoder encodeObject:_teamId forKey:kAXFormationTeamId];
    [aCoder encodeObject:_internalBaseClassDescription forKey:kAXFormationDescription];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXFormation *copy = [[AXFormation alloc] init];
    
    if (copy) {

        copy.team = [self.team copyWithZone:zone];
        copy.weight = self.weight;
        copy.status = self.status;
        copy.countryCode = [self.countryCode copyWithZone:zone];
        copy.accountRoles = [self.accountRoles copyWithZone:zone];
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.idCard = [self.idCard copyWithZone:zone];
        copy.matchId = [self.matchId copyWithZone:zone];
        copy.nationality = [self.nationality copyWithZone:zone];
        copy.avatarUri = [self.avatarUri copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.passport = [self.passport copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.roles = [self.roles copyWithZone:zone];
        copy.birth = [self.birth copyWithZone:zone];
        copy.gender = self.gender;
        copy.email = [self.email copyWithZone:zone];
        copy.phone = [self.phone copyWithZone:zone];
        copy.height = self.height;
        copy.shirtNumber = self.shirtNumber;
        copy.area = [self.area copyWithZone:zone];
        copy.statAreas = [self.statAreas copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.relationId = [self.relationId copyWithZone:zone];
        copy.teamId = [self.teamId copyWithZone:zone];
        copy.internalBaseClassDescription = [self.internalBaseClassDescription copyWithZone:zone];
    }
    
    return copy;
}


@end
