//
//  AXArea.m
//
//  Created by Kelvin Tong on 16/4/26
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXArea.h"


NSString *const kAXAreaDistrict = @"district";
NSString *const kAXAreaCountry = @"country";
NSString *const kAXAreaProvince = @"province";
NSString *const kAXAreaCity = @"city";


@interface AXArea ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXArea

@synthesize district = _district;
@synthesize country = _country;
@synthesize province = _province;
@synthesize city = _city;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.district = [self objectOrNilForKey:kAXAreaDistrict fromDictionary:dict];
            self.country = [self objectOrNilForKey:kAXAreaCountry fromDictionary:dict];
            self.province = [self objectOrNilForKey:kAXAreaProvince fromDictionary:dict];
            self.city = [self objectOrNilForKey:kAXAreaCity fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.district forKey:kAXAreaDistrict];
    [mutableDict setValue:self.country forKey:kAXAreaCountry];
    [mutableDict setValue:self.province forKey:kAXAreaProvince];
    [mutableDict setValue:self.city forKey:kAXAreaCity];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.district = [aDecoder decodeObjectForKey:kAXAreaDistrict];
    self.country = [aDecoder decodeObjectForKey:kAXAreaCountry];
    self.province = [aDecoder decodeObjectForKey:kAXAreaProvince];
    self.city = [aDecoder decodeObjectForKey:kAXAreaCity];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_district forKey:kAXAreaDistrict];
    [aCoder encodeObject:_country forKey:kAXAreaCountry];
    [aCoder encodeObject:_province forKey:kAXAreaProvince];
    [aCoder encodeObject:_city forKey:kAXAreaCity];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXArea *copy = [[AXArea alloc] init];
    
    if (copy) {

        copy.district = [self.district copyWithZone:zone];
        copy.country = [self.country copyWithZone:zone];
        copy.province = [self.province copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
    }
    
    return copy;
}


@end
