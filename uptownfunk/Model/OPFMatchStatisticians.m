//
//  OPFMatchStatisticians.m
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "OPFMatchStatisticians.h"
#import "AXStatistician.h"


NSString *const kOPFMatchStatisticiansStatus = @"status";
NSString *const kOPFMatchStatisticiansKvalue = @"kvalue";
NSString *const kOPFMatchStatisticiansStartAt = @"start_at";
NSString *const kOPFMatchStatisticiansId = @"id";
NSString *const kOPFMatchStatisticiansRole = @"role";
NSString *const kOPFMatchStatisticiansMatchId = @"match_id";
NSString *const kOPFMatchStatisticiansTeamId = @"team_id";
NSString *const kOPFMatchStatisticiansStatistician = @"statistician";


@interface OPFMatchStatisticians ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OPFMatchStatisticians

@synthesize status = _status;
@synthesize kvalue = _kvalue;
@synthesize startAt = _startAt;
@synthesize matchStatisticiansIdentifier = _matchStatisticiansIdentifier;
@synthesize role = _role;
@synthesize matchId = _matchId;
@synthesize teamId = _teamId;
@synthesize statistician = _statistician;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [[self objectOrNilForKey:kOPFMatchStatisticiansStatus fromDictionary:dict] doubleValue];
            self.kvalue = [[self objectOrNilForKey:kOPFMatchStatisticiansKvalue fromDictionary:dict] doubleValue];
            self.startAt = [self objectOrNilForKey:kOPFMatchStatisticiansStartAt fromDictionary:dict];
            self.matchStatisticiansIdentifier = [self objectOrNilForKey:kOPFMatchStatisticiansId fromDictionary:dict];
            self.role = [self objectOrNilForKey:kOPFMatchStatisticiansRole fromDictionary:dict];
            self.matchId = [self objectOrNilForKey:kOPFMatchStatisticiansMatchId fromDictionary:dict];
            self.teamId = [self objectOrNilForKey:kOPFMatchStatisticiansTeamId fromDictionary:dict];
            self.statistician = [AXStatistician modelObjectWithDictionary:[dict objectForKey:kOPFMatchStatisticiansStatistician]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kOPFMatchStatisticiansStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.kvalue] forKey:kOPFMatchStatisticiansKvalue];
    [mutableDict setValue:self.startAt forKey:kOPFMatchStatisticiansStartAt];
    [mutableDict setValue:self.matchStatisticiansIdentifier forKey:kOPFMatchStatisticiansId];
    [mutableDict setValue:self.role forKey:kOPFMatchStatisticiansRole];
    [mutableDict setValue:self.matchId forKey:kOPFMatchStatisticiansMatchId];
    [mutableDict setValue:self.teamId forKey:kOPFMatchStatisticiansTeamId];
    [mutableDict setValue:[self.statistician dictionaryRepresentation] forKey:kOPFMatchStatisticiansStatistician];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.status = [aDecoder decodeDoubleForKey:kOPFMatchStatisticiansStatus];
    self.kvalue = [aDecoder decodeDoubleForKey:kOPFMatchStatisticiansKvalue];
    self.startAt = [aDecoder decodeObjectForKey:kOPFMatchStatisticiansStartAt];
    self.matchStatisticiansIdentifier = [aDecoder decodeObjectForKey:kOPFMatchStatisticiansId];
    self.role = [aDecoder decodeObjectForKey:kOPFMatchStatisticiansRole];
    self.matchId = [aDecoder decodeObjectForKey:kOPFMatchStatisticiansMatchId];
    self.teamId = [aDecoder decodeObjectForKey:kOPFMatchStatisticiansTeamId];
    self.statistician = [aDecoder decodeObjectForKey:kOPFMatchStatisticiansStatistician];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_status forKey:kOPFMatchStatisticiansStatus];
    [aCoder encodeDouble:_kvalue forKey:kOPFMatchStatisticiansKvalue];
    [aCoder encodeObject:_startAt forKey:kOPFMatchStatisticiansStartAt];
    [aCoder encodeObject:_matchStatisticiansIdentifier forKey:kOPFMatchStatisticiansId];
    [aCoder encodeObject:_role forKey:kOPFMatchStatisticiansRole];
    [aCoder encodeObject:_matchId forKey:kOPFMatchStatisticiansMatchId];
    [aCoder encodeObject:_teamId forKey:kOPFMatchStatisticiansTeamId];
    [aCoder encodeObject:_statistician forKey:kOPFMatchStatisticiansStatistician];
}

- (id)copyWithZone:(NSZone *)zone
{
    OPFMatchStatisticians *copy = [[OPFMatchStatisticians alloc] init];
    
    if (copy) {

        copy.status = self.status;
        copy.kvalue = self.kvalue;
        copy.startAt = [self.startAt copyWithZone:zone];
        copy.matchStatisticiansIdentifier = [self.matchStatisticiansIdentifier copyWithZone:zone];
        copy.role = [self.role copyWithZone:zone];
        copy.matchId = [self.matchId copyWithZone:zone];
        copy.teamId = [self.teamId copyWithZone:zone];
        copy.statistician = [self.statistician copyWithZone:zone];
    }
    
    return copy;
}


@end
