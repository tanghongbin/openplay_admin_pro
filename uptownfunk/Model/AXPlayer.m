//
//  AXPlayer.m
//
//  Created by Kelvin Tong on 16/4/26
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXPlayer.h"
#import "AXArea.h"
#import "AXStatAreas.h"


NSString *const kAXPlayerWeight = @"weight";
NSString *const kAXPlayerStatus = @"status";
NSString *const kAXPlayerCountryCode = @"country_code";
NSString *const kAXPlayerAccountRoles = @"account_roles";
NSString *const kAXPlayerUpdatedAt = @"updated_at";
NSString *const kAXPlayerIdCard = @"id_card";
NSString *const kAXPlayerName = @"name";
NSString *const kAXPlayerNationality = @"nationality";
NSString *const kAXPlayerPassport = @"passport";
NSString *const kAXPlayerAvatarUri = @"avatar_uri";
NSString *const kAXPlayerId = @"id";
NSString *const kAXPlayerRoles = @"roles";
NSString *const kAXPlayerBirth = @"birth";
NSString *const kAXPlayerEmail = @"email";
NSString *const kAXPlayerGender = @"gender";
NSString *const kAXPlayerPhone = @"phone";
NSString *const kAXPlayerHeight = @"height";
NSString *const kAXPlayerArea = @"area";
NSString *const kAXPlayerStatAreas = @"stat_areas";
NSString *const kAXPlayerCreatedAt = @"created_at";
NSString *const kAXPlayerDescription = @"description";


@interface AXPlayer ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXPlayer

@synthesize weight = _weight;
@synthesize status = _status;
@synthesize countryCode = _countryCode;
@synthesize accountRoles = _accountRoles;
@synthesize updatedAt = _updatedAt;
@synthesize idCard = _idCard;
@synthesize name = _name;
@synthesize nationality = _nationality;
@synthesize passport = _passport;
@synthesize avatarUri = _avatarUri;
@synthesize playerIdentifier = _playerIdentifier;
@synthesize roles = _roles;
@synthesize birth = _birth;
@synthesize email = _email;
@synthesize gender = _gender;
@synthesize phone = _phone;
@synthesize height = _height;
@synthesize area = _area;
@synthesize statAreas = _statAreas;
@synthesize createdAt = _createdAt;
@synthesize playerDescription = _playerDescription;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.weight = [[self objectOrNilForKey:kAXPlayerWeight fromDictionary:dict] doubleValue];
            self.status = [[self objectOrNilForKey:kAXPlayerStatus fromDictionary:dict] doubleValue];
            self.countryCode = [self objectOrNilForKey:kAXPlayerCountryCode fromDictionary:dict];
            self.accountRoles = [self objectOrNilForKey:kAXPlayerAccountRoles fromDictionary:dict];
            self.updatedAt = [self objectOrNilForKey:kAXPlayerUpdatedAt fromDictionary:dict];
            self.idCard = [self objectOrNilForKey:kAXPlayerIdCard fromDictionary:dict];
            self.name = [self objectOrNilForKey:kAXPlayerName fromDictionary:dict];
            self.nationality = [self objectOrNilForKey:kAXPlayerNationality fromDictionary:dict];
            self.passport = [self objectOrNilForKey:kAXPlayerPassport fromDictionary:dict];
            self.avatarUri = [self objectOrNilForKey:kAXPlayerAvatarUri fromDictionary:dict];
            self.playerIdentifier = [self objectOrNilForKey:kAXPlayerId fromDictionary:dict];
            self.roles = [self objectOrNilForKey:kAXPlayerRoles fromDictionary:dict];
            self.birth = [self objectOrNilForKey:kAXPlayerBirth fromDictionary:dict];
            self.email = [self objectOrNilForKey:kAXPlayerEmail fromDictionary:dict];
            self.gender = [[self objectOrNilForKey:kAXPlayerGender fromDictionary:dict] doubleValue];
            self.phone = [self objectOrNilForKey:kAXPlayerPhone fromDictionary:dict];
            self.height = [[self objectOrNilForKey:kAXPlayerHeight fromDictionary:dict] doubleValue];
            self.area = [AXArea modelObjectWithDictionary:[dict objectForKey:kAXPlayerArea]];
    NSObject *receivedAXStatAreas = [dict objectForKey:kAXPlayerStatAreas];
    NSMutableArray *parsedAXStatAreas = [NSMutableArray array];
    if ([receivedAXStatAreas isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedAXStatAreas) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedAXStatAreas addObject:[AXStatAreas modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedAXStatAreas isKindOfClass:[NSDictionary class]]) {
       [parsedAXStatAreas addObject:[AXStatAreas modelObjectWithDictionary:(NSDictionary *)receivedAXStatAreas]];
    }

    self.statAreas = [NSArray arrayWithArray:parsedAXStatAreas];
            self.createdAt = [self objectOrNilForKey:kAXPlayerCreatedAt fromDictionary:dict];
            self.playerDescription = [self objectOrNilForKey:kAXPlayerDescription fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.weight] forKey:kAXPlayerWeight];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kAXPlayerStatus];
    [mutableDict setValue:self.countryCode forKey:kAXPlayerCountryCode];
    NSMutableArray *tempArrayForAccountRoles = [NSMutableArray array];
    for (NSObject *subArrayObject in self.accountRoles) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForAccountRoles addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForAccountRoles addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForAccountRoles] forKey:kAXPlayerAccountRoles];
    [mutableDict setValue:self.updatedAt forKey:kAXPlayerUpdatedAt];
    [mutableDict setValue:self.idCard forKey:kAXPlayerIdCard];
    [mutableDict setValue:self.name forKey:kAXPlayerName];
    [mutableDict setValue:self.nationality forKey:kAXPlayerNationality];
    [mutableDict setValue:self.passport forKey:kAXPlayerPassport];
    [mutableDict setValue:self.avatarUri forKey:kAXPlayerAvatarUri];
    [mutableDict setValue:self.playerIdentifier forKey:kAXPlayerId];
    NSMutableArray *tempArrayForRoles = [NSMutableArray array];
    for (NSObject *subArrayObject in self.roles) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForRoles addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForRoles addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForRoles] forKey:kAXPlayerRoles];
    [mutableDict setValue:self.birth forKey:kAXPlayerBirth];
    [mutableDict setValue:self.email forKey:kAXPlayerEmail];
    [mutableDict setValue:[NSNumber numberWithDouble:self.gender] forKey:kAXPlayerGender];
    [mutableDict setValue:self.phone forKey:kAXPlayerPhone];
    [mutableDict setValue:[NSNumber numberWithDouble:self.height] forKey:kAXPlayerHeight];
    [mutableDict setValue:[self.area dictionaryRepresentation] forKey:kAXPlayerArea];
    NSMutableArray *tempArrayForStatAreas = [NSMutableArray array];
    for (NSObject *subArrayObject in self.statAreas) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForStatAreas addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForStatAreas addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForStatAreas] forKey:kAXPlayerStatAreas];
    [mutableDict setValue:self.createdAt forKey:kAXPlayerCreatedAt];
    [mutableDict setValue:self.playerDescription forKey:kAXPlayerDescription];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.weight = [aDecoder decodeDoubleForKey:kAXPlayerWeight];
    self.status = [aDecoder decodeDoubleForKey:kAXPlayerStatus];
    self.countryCode = [aDecoder decodeObjectForKey:kAXPlayerCountryCode];
    self.accountRoles = [aDecoder decodeObjectForKey:kAXPlayerAccountRoles];
    self.updatedAt = [aDecoder decodeObjectForKey:kAXPlayerUpdatedAt];
    self.idCard = [aDecoder decodeObjectForKey:kAXPlayerIdCard];
    self.name = [aDecoder decodeObjectForKey:kAXPlayerName];
    self.nationality = [aDecoder decodeObjectForKey:kAXPlayerNationality];
    self.passport = [aDecoder decodeObjectForKey:kAXPlayerPassport];
    self.avatarUri = [aDecoder decodeObjectForKey:kAXPlayerAvatarUri];
    self.playerIdentifier = [aDecoder decodeObjectForKey:kAXPlayerId];
    self.roles = [aDecoder decodeObjectForKey:kAXPlayerRoles];
    self.birth = [aDecoder decodeObjectForKey:kAXPlayerBirth];
    self.email = [aDecoder decodeObjectForKey:kAXPlayerEmail];
    self.gender = [aDecoder decodeDoubleForKey:kAXPlayerGender];
    self.phone = [aDecoder decodeObjectForKey:kAXPlayerPhone];
    self.height = [aDecoder decodeDoubleForKey:kAXPlayerHeight];
    self.area = [aDecoder decodeObjectForKey:kAXPlayerArea];
    self.statAreas = [aDecoder decodeObjectForKey:kAXPlayerStatAreas];
    self.createdAt = [aDecoder decodeObjectForKey:kAXPlayerCreatedAt];
    self.playerDescription = [aDecoder decodeObjectForKey:kAXPlayerDescription];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_weight forKey:kAXPlayerWeight];
    [aCoder encodeDouble:_status forKey:kAXPlayerStatus];
    [aCoder encodeObject:_countryCode forKey:kAXPlayerCountryCode];
    [aCoder encodeObject:_accountRoles forKey:kAXPlayerAccountRoles];
    [aCoder encodeObject:_updatedAt forKey:kAXPlayerUpdatedAt];
    [aCoder encodeObject:_idCard forKey:kAXPlayerIdCard];
    [aCoder encodeObject:_name forKey:kAXPlayerName];
    [aCoder encodeObject:_nationality forKey:kAXPlayerNationality];
    [aCoder encodeObject:_passport forKey:kAXPlayerPassport];
    [aCoder encodeObject:_avatarUri forKey:kAXPlayerAvatarUri];
    [aCoder encodeObject:_playerIdentifier forKey:kAXPlayerId];
    [aCoder encodeObject:_roles forKey:kAXPlayerRoles];
    [aCoder encodeObject:_birth forKey:kAXPlayerBirth];
    [aCoder encodeObject:_email forKey:kAXPlayerEmail];
    [aCoder encodeDouble:_gender forKey:kAXPlayerGender];
    [aCoder encodeObject:_phone forKey:kAXPlayerPhone];
    [aCoder encodeDouble:_height forKey:kAXPlayerHeight];
    [aCoder encodeObject:_area forKey:kAXPlayerArea];
    [aCoder encodeObject:_statAreas forKey:kAXPlayerStatAreas];
    [aCoder encodeObject:_createdAt forKey:kAXPlayerCreatedAt];
    [aCoder encodeObject:_playerDescription forKey:kAXPlayerDescription];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXPlayer *copy = [[AXPlayer alloc] init];
    
    if (copy) {

        copy.weight = self.weight;
        copy.status = self.status;
        copy.countryCode = [self.countryCode copyWithZone:zone];
        copy.accountRoles = [self.accountRoles copyWithZone:zone];
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.idCard = [self.idCard copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.nationality = [self.nationality copyWithZone:zone];
        copy.passport = [self.passport copyWithZone:zone];
        copy.avatarUri = [self.avatarUri copyWithZone:zone];
        copy.playerIdentifier = [self.playerIdentifier copyWithZone:zone];
        copy.roles = [self.roles copyWithZone:zone];
        copy.birth = [self.birth copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.gender = self.gender;
        copy.phone = [self.phone copyWithZone:zone];
        copy.height = self.height;
        copy.area = [self.area copyWithZone:zone];
        copy.statAreas = [self.statAreas copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.playerDescription = [self.playerDescription copyWithZone:zone];
    }
    
    return copy;
}


@end
