//
//  AXTeam.h
//
//  Created by Kelvin Tong on 16/4/26
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AXArea;

@interface AXTeam : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double status;
@property (nonatomic, strong) AXArea *area;
@property (nonatomic, strong) NSString *opId;
@property (nonatomic, strong) NSString *teamIdentifier;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *teamStyle;
@property (nonatomic, strong) NSString *logoUri;
@property (nonatomic, strong) NSString *shortName;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, strong) NSString *name;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
