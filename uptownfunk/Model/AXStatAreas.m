//
//  AXStatAreas.m
//
//  Created by Kelvin Tong on 16/5/9
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXStatAreas.h"


NSString *const kAXStatAreasDistrict = @"district";
NSString *const kAXStatAreasCountry = @"country";
NSString *const kAXStatAreasProvince = @"province";
NSString *const kAXStatAreasCity = @"city";


@interface AXStatAreas ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXStatAreas

@synthesize district = _district;
@synthesize country = _country;
@synthesize province = _province;
@synthesize city = _city;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.district = [self objectOrNilForKey:kAXStatAreasDistrict fromDictionary:dict];
            self.country = [self objectOrNilForKey:kAXStatAreasCountry fromDictionary:dict];
            self.province = [self objectOrNilForKey:kAXStatAreasProvince fromDictionary:dict];
            self.city = [self objectOrNilForKey:kAXStatAreasCity fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.district forKey:kAXStatAreasDistrict];
    [mutableDict setValue:self.country forKey:kAXStatAreasCountry];
    [mutableDict setValue:self.province forKey:kAXStatAreasProvince];
    [mutableDict setValue:self.city forKey:kAXStatAreasCity];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.district = [aDecoder decodeObjectForKey:kAXStatAreasDistrict];
    self.country = [aDecoder decodeObjectForKey:kAXStatAreasCountry];
    self.province = [aDecoder decodeObjectForKey:kAXStatAreasProvince];
    self.city = [aDecoder decodeObjectForKey:kAXStatAreasCity];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_district forKey:kAXStatAreasDistrict];
    [aCoder encodeObject:_country forKey:kAXStatAreasCountry];
    [aCoder encodeObject:_province forKey:kAXStatAreasProvince];
    [aCoder encodeObject:_city forKey:kAXStatAreasCity];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXStatAreas *copy = [[AXStatAreas alloc] init];
    
    if (copy) {

        copy.district = [self.district copyWithZone:zone];
        copy.country = [self.country copyWithZone:zone];
        copy.province = [self.province copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
    }
    
    return copy;
}


@end
