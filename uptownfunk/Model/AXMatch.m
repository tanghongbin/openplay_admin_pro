//
//  OPFList.m
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXMatch.h"
#import "OPFMatchStatisticians.h"
#import "OPFStadium.h"
#import "AXTeam.h"
#import "OPFVenue.h"
#import "OPFWeather.h"


NSString *const kOPFListMatchStatisticians = @"match_statisticians";
NSString *const kOPFListMatchType = @"match_type";
NSString *const kOPFListStatus = @"status";
NSString *const kOPFListHsB = @"hs_b";
NSString *const kOPFListEndAt = @"end_at";
NSString *const kOPFListRuleExtraTime = @"rule_extra_time";
NSString *const kOPFListTeamB = @"team_b";
NSString *const kOPFListStadium = @"stadium";
NSString *const kOPFListFsB = @"fs_b";
NSString *const kOPFListRuleStoppedWatch = @"rule_stopped_watch";
NSString *const kOPFListOpId = @"op_id";
NSString *const kOPFListId = @"id";
NSString *const kOPFListMatchTitle = @"match_title";
NSString *const kOPFListRuleHalfTime = @"rule_half_time";
NSString *const kOPFListTeamA = @"team_a";
NSString *const kOPFListHsA = @"hs_a";
NSString *const kOPFListSA = @"s_a";
NSString *const kOPFListVenue = @"venue";
NSString *const kOPFListStatsMode = @"stats_mode";
NSString *const kOPFListMatchStyle = @"match_style";
NSString *const kOPFListWeather = @"weather";
NSString *const kOPFListMatchPeriod = @"match_period";
NSString *const kOPFListSB = @"s_b";
NSString *const kOPFListStartAt = @"start_at";
NSString *const kOPFListFsA = @"fs_a";


@interface AXMatch ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXMatch

@synthesize matchStatisticians = _matchStatisticians;
@synthesize matchType = _matchType;
@synthesize status = _status;
@synthesize hsB = _hsB;
@synthesize endAt = _endAt;
@synthesize ruleExtraTime = _ruleExtraTime;
@synthesize teamB = _teamB;
@synthesize stadium = _stadium;
@synthesize fsB = _fsB;
@synthesize ruleStoppedWatch = _ruleStoppedWatch;
@synthesize opId = _opId;
@synthesize listIdentifier = _listIdentifier;
@synthesize matchTitle = _matchTitle;
@synthesize ruleHalfTime = _ruleHalfTime;
@synthesize teamA = _teamA;
@synthesize hsA = _hsA;
@synthesize sA = _sA;
@synthesize venue = _venue;
@synthesize statsMode = _statsMode;
@synthesize matchStyle = _matchStyle;
@synthesize weather = _weather;
@synthesize matchPeriod = _matchPeriod;
@synthesize sB = _sB;
@synthesize startAt = _startAt;
@synthesize fsA = _fsA;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedOPFMatchStatisticians = [dict objectForKey:kOPFListMatchStatisticians];
    NSMutableArray *parsedOPFMatchStatisticians = [NSMutableArray array];
    if ([receivedOPFMatchStatisticians isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedOPFMatchStatisticians) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedOPFMatchStatisticians addObject:[OPFMatchStatisticians modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedOPFMatchStatisticians isKindOfClass:[NSDictionary class]]) {
       [parsedOPFMatchStatisticians addObject:[OPFMatchStatisticians modelObjectWithDictionary:(NSDictionary *)receivedOPFMatchStatisticians]];
    }

    self.matchStatisticians = [NSArray arrayWithArray:parsedOPFMatchStatisticians];
            self.matchType = [[self objectOrNilForKey:kOPFListMatchType fromDictionary:dict] doubleValue];
            self.status = [self objectOrNilForKey:kOPFListStatus fromDictionary:dict];
            self.hsB = [[self objectOrNilForKey:kOPFListHsB fromDictionary:dict] doubleValue];
            self.endAt = [self objectOrNilForKey:kOPFListEndAt fromDictionary:dict];
            self.ruleExtraTime = [[self objectOrNilForKey:kOPFListRuleExtraTime fromDictionary:dict] doubleValue];
            self.teamB = [AXTeam modelObjectWithDictionary:[dict objectForKey:kOPFListTeamB]];
            self.stadium = [OPFStadium modelObjectWithDictionary:[dict objectForKey:kOPFListStadium]];
            self.fsB = [[self objectOrNilForKey:kOPFListFsB fromDictionary:dict] doubleValue];
            self.ruleStoppedWatch = [[self objectOrNilForKey:kOPFListRuleStoppedWatch fromDictionary:dict] doubleValue];
            self.opId = [self objectOrNilForKey:kOPFListOpId fromDictionary:dict];
            self.listIdentifier = [self objectOrNilForKey:kOPFListId fromDictionary:dict];
            self.matchTitle = [self objectOrNilForKey:kOPFListMatchTitle fromDictionary:dict];
            self.ruleHalfTime = [[self objectOrNilForKey:kOPFListRuleHalfTime fromDictionary:dict] doubleValue];
            self.teamA = [AXTeam modelObjectWithDictionary:[dict objectForKey:kOPFListTeamA]];
            self.hsA = [[self objectOrNilForKey:kOPFListHsA fromDictionary:dict] doubleValue];
            self.sA = [[self objectOrNilForKey:kOPFListSA fromDictionary:dict] doubleValue];
            self.venue = [OPFVenue modelObjectWithDictionary:[dict objectForKey:kOPFListVenue]];
            self.statsMode = [[self objectOrNilForKey:kOPFListStatsMode fromDictionary:dict] doubleValue];
            self.matchStyle = [self objectOrNilForKey:kOPFListMatchStyle fromDictionary:dict];
            self.weather = [OPFWeather modelObjectWithDictionary:[dict objectForKey:kOPFListWeather]];
            self.matchPeriod = [self objectOrNilForKey:kOPFListMatchPeriod fromDictionary:dict];
            self.sB = [[self objectOrNilForKey:kOPFListSB fromDictionary:dict] doubleValue];
            self.startAt = [self objectOrNilForKey:kOPFListStartAt fromDictionary:dict];
            self.fsA = [[self objectOrNilForKey:kOPFListFsA fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForMatchStatisticians = [NSMutableArray array];
    for (NSObject *subArrayObject in self.matchStatisticians) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForMatchStatisticians addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForMatchStatisticians addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForMatchStatisticians] forKey:kOPFListMatchStatisticians];
    [mutableDict setValue:[NSNumber numberWithDouble:self.matchType] forKey:kOPFListMatchType];
    [mutableDict setValue:self.status forKey:kOPFListStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.hsB] forKey:kOPFListHsB];
    [mutableDict setValue:self.endAt forKey:kOPFListEndAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.ruleExtraTime] forKey:kOPFListRuleExtraTime];
    [mutableDict setValue:[self.teamB dictionaryRepresentation] forKey:kOPFListTeamB];
    [mutableDict setValue:[self.stadium dictionaryRepresentation] forKey:kOPFListStadium];
    [mutableDict setValue:[NSNumber numberWithDouble:self.fsB] forKey:kOPFListFsB];
    [mutableDict setValue:[NSNumber numberWithDouble:self.ruleStoppedWatch] forKey:kOPFListRuleStoppedWatch];
    [mutableDict setValue:self.opId forKey:kOPFListOpId];
    [mutableDict setValue:self.listIdentifier forKey:kOPFListId];
    [mutableDict setValue:self.matchTitle forKey:kOPFListMatchTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.ruleHalfTime] forKey:kOPFListRuleHalfTime];
    [mutableDict setValue:[self.teamA dictionaryRepresentation] forKey:kOPFListTeamA];
    [mutableDict setValue:[NSNumber numberWithDouble:self.hsA] forKey:kOPFListHsA];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sA] forKey:kOPFListSA];
    [mutableDict setValue:[self.venue dictionaryRepresentation] forKey:kOPFListVenue];
    [mutableDict setValue:[NSNumber numberWithDouble:self.statsMode] forKey:kOPFListStatsMode];
    [mutableDict setValue:self.matchStyle forKey:kOPFListMatchStyle];
    [mutableDict setValue:[self.weather dictionaryRepresentation] forKey:kOPFListWeather];
    [mutableDict setValue:self.matchPeriod forKey:kOPFListMatchPeriod];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sB] forKey:kOPFListSB];
    [mutableDict setValue:self.startAt forKey:kOPFListStartAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.fsA] forKey:kOPFListFsA];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.matchStatisticians = [aDecoder decodeObjectForKey:kOPFListMatchStatisticians];
    self.matchType = [aDecoder decodeDoubleForKey:kOPFListMatchType];
    self.status = [aDecoder decodeObjectForKey:kOPFListStatus];
    self.hsB = [aDecoder decodeDoubleForKey:kOPFListHsB];
    self.endAt = [aDecoder decodeObjectForKey:kOPFListEndAt];
    self.ruleExtraTime = [aDecoder decodeDoubleForKey:kOPFListRuleExtraTime];
    self.teamB = [aDecoder decodeObjectForKey:kOPFListTeamB];
    self.stadium = [aDecoder decodeObjectForKey:kOPFListStadium];
    self.fsB = [aDecoder decodeDoubleForKey:kOPFListFsB];
    self.ruleStoppedWatch = [aDecoder decodeDoubleForKey:kOPFListRuleStoppedWatch];
    self.opId = [aDecoder decodeObjectForKey:kOPFListOpId];
    self.listIdentifier = [aDecoder decodeObjectForKey:kOPFListId];
    self.matchTitle = [aDecoder decodeObjectForKey:kOPFListMatchTitle];
    self.ruleHalfTime = [aDecoder decodeDoubleForKey:kOPFListRuleHalfTime];
    self.teamA = [aDecoder decodeObjectForKey:kOPFListTeamA];
    self.hsA = [aDecoder decodeDoubleForKey:kOPFListHsA];
    self.sA = [aDecoder decodeDoubleForKey:kOPFListSA];
    self.venue = [aDecoder decodeObjectForKey:kOPFListVenue];
    self.statsMode = [aDecoder decodeDoubleForKey:kOPFListStatsMode];
    self.matchStyle = [aDecoder decodeObjectForKey:kOPFListMatchStyle];
    self.weather = [aDecoder decodeObjectForKey:kOPFListWeather];
    self.matchPeriod = [aDecoder decodeObjectForKey:kOPFListMatchPeriod];
    self.sB = [aDecoder decodeDoubleForKey:kOPFListSB];
    self.startAt = [aDecoder decodeObjectForKey:kOPFListStartAt];
    self.fsA = [aDecoder decodeDoubleForKey:kOPFListFsA];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_matchStatisticians forKey:kOPFListMatchStatisticians];
    [aCoder encodeDouble:_matchType forKey:kOPFListMatchType];
    [aCoder encodeObject:_status forKey:kOPFListStatus];
    [aCoder encodeDouble:_hsB forKey:kOPFListHsB];
    [aCoder encodeObject:_endAt forKey:kOPFListEndAt];
    [aCoder encodeDouble:_ruleExtraTime forKey:kOPFListRuleExtraTime];
    [aCoder encodeObject:_teamB forKey:kOPFListTeamB];
    [aCoder encodeObject:_stadium forKey:kOPFListStadium];
    [aCoder encodeDouble:_fsB forKey:kOPFListFsB];
    [aCoder encodeDouble:_ruleStoppedWatch forKey:kOPFListRuleStoppedWatch];
    [aCoder encodeObject:_opId forKey:kOPFListOpId];
    [aCoder encodeObject:_listIdentifier forKey:kOPFListId];
    [aCoder encodeObject:_matchTitle forKey:kOPFListMatchTitle];
    [aCoder encodeDouble:_ruleHalfTime forKey:kOPFListRuleHalfTime];
    [aCoder encodeObject:_teamA forKey:kOPFListTeamA];
    [aCoder encodeDouble:_hsA forKey:kOPFListHsA];
    [aCoder encodeDouble:_sA forKey:kOPFListSA];
    [aCoder encodeObject:_venue forKey:kOPFListVenue];
    [aCoder encodeDouble:_statsMode forKey:kOPFListStatsMode];
    [aCoder encodeObject:_matchStyle forKey:kOPFListMatchStyle];
    [aCoder encodeObject:_weather forKey:kOPFListWeather];
    [aCoder encodeObject:_matchPeriod forKey:kOPFListMatchPeriod];
    [aCoder encodeDouble:_sB forKey:kOPFListSB];
    [aCoder encodeObject:_startAt forKey:kOPFListStartAt];
    [aCoder encodeDouble:_fsA forKey:kOPFListFsA];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXMatch *copy = [[AXMatch alloc] init];
    
    if (copy) {

        copy.matchStatisticians = [self.matchStatisticians copyWithZone:zone];
        copy.matchType = self.matchType;
        copy.status = [self.status copyWithZone:zone];
        copy.hsB = self.hsB;
        copy.endAt = [self.endAt copyWithZone:zone];
        copy.ruleExtraTime = self.ruleExtraTime;
        copy.teamB = [self.teamB copyWithZone:zone];
        copy.stadium = [self.stadium copyWithZone:zone];
        copy.fsB = self.fsB;
        copy.ruleStoppedWatch = self.ruleStoppedWatch;
        copy.opId = [self.opId copyWithZone:zone];
        copy.listIdentifier = [self.listIdentifier copyWithZone:zone];
        copy.matchTitle = [self.matchTitle copyWithZone:zone];
        copy.ruleHalfTime = self.ruleHalfTime;
        copy.teamA = [self.teamA copyWithZone:zone];
        copy.hsA = self.hsA;
        copy.sA = self.sA;
        copy.venue = [self.venue copyWithZone:zone];
        copy.statsMode = self.statsMode;
        copy.matchStyle = [self.matchStyle copyWithZone:zone];
        copy.weather = [self.weather copyWithZone:zone];
        copy.matchPeriod = [self.matchPeriod copyWithZone:zone];
        copy.sB = self.sB;
        copy.startAt = [self.startAt copyWithZone:zone];
        copy.fsA = self.fsA;
    }
    
    return copy;
}


@end
