//
//  AXBaseClass.m
//
//  Created by Kelvin Tong on 16/4/26
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXEvents_PerPage.h"
#import "AXEvent.h"


NSString *const kAXBaseClassList = @"list";
NSString *const kAXBaseClassLastId = @"last_id";
NSString *const kAXBaseClassLastLeft = @"last_left";
NSString *const kAXBaseClassTotal = @"total";


@interface AXEvents_PerPage ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXEvents_PerPage

@synthesize list = _list;
@synthesize lastId = _lastId;
@synthesize lastLeft = _lastLeft;
@synthesize total = _total;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedAXList = [dict objectForKey:kAXBaseClassList];
    NSMutableArray *parsedAXList = [NSMutableArray array];
    if ([receivedAXList isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedAXList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedAXList addObject:[AXEvent modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedAXList isKindOfClass:[NSDictionary class]]) {
       [parsedAXList addObject:[AXEvent modelObjectWithDictionary:(NSDictionary *)receivedAXList]];
    }

    self.list = [NSArray arrayWithArray:parsedAXList];
            self.lastId = [[self objectOrNilForKey:kAXBaseClassLastId fromDictionary:dict] doubleValue];
            self.lastLeft = [[self objectOrNilForKey:kAXBaseClassLastLeft fromDictionary:dict] doubleValue];
            self.total = [[self objectOrNilForKey:kAXBaseClassTotal fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForList = [NSMutableArray array];
    for (NSObject *subArrayObject in self.list) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForList addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForList addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForList] forKey:kAXBaseClassList];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lastId] forKey:kAXBaseClassLastId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lastLeft] forKey:kAXBaseClassLastLeft];
    [mutableDict setValue:[NSNumber numberWithDouble:self.total] forKey:kAXBaseClassTotal];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.list = [aDecoder decodeObjectForKey:kAXBaseClassList];
    self.lastId = [aDecoder decodeDoubleForKey:kAXBaseClassLastId];
    self.lastLeft = [aDecoder decodeDoubleForKey:kAXBaseClassLastLeft];
    self.total = [aDecoder decodeDoubleForKey:kAXBaseClassTotal];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_list forKey:kAXBaseClassList];
    [aCoder encodeDouble:_lastId forKey:kAXBaseClassLastId];
    [aCoder encodeDouble:_lastLeft forKey:kAXBaseClassLastLeft];
    [aCoder encodeDouble:_total forKey:kAXBaseClassTotal];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXEvents_PerPage *copy = [[AXEvents_PerPage alloc] init];
    
    if (copy) {

        copy.list = [self.list copyWithZone:zone];
        copy.lastId = self.lastId;
        copy.lastLeft = self.lastLeft;
        copy.total = self.total;
    }
    
    return copy;
}


@end
