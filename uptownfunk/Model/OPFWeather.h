//
//  OPFWeather.h
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface OPFWeather : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *temperature;
@property (nonatomic, strong) NSString *wind;
@property (nonatomic, strong) NSString *precipitation;
@property (nonatomic, strong) NSString *weatherDescription;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
