//
//  OPFMatchStatisticians.h
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AXStatistician;

@interface OPFMatchStatisticians : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double status;
@property (nonatomic, assign) double kvalue;
@property (nonatomic, strong) NSString *startAt;
@property (nonatomic, strong) NSString *matchStatisticiansIdentifier;
@property (nonatomic, strong) NSString *role;
@property (nonatomic, strong) NSString *matchId;
@property (nonatomic, strong) NSString *teamId;
@property (nonatomic, strong) AXStatistician *statistician;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
