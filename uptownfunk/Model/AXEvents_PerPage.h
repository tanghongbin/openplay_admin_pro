//
//  AXBaseClass.h
//
//  Created by Kelvin Tong on 16/4/26
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface AXEvents_PerPage : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *list;
@property (nonatomic, assign) double lastId;
@property (nonatomic, assign) double lastLeft;
@property (nonatomic, assign) double total;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
