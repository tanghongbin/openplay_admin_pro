//
//  AXBaseClass.h
//
//  Created by Kelvin Tong on 16/4/18
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface AXUser : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *roles;
@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *lastOpTime;
@property (nonatomic, strong) NSString *lastLoginAt;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *name;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

- (void) userSave;

+ (void) userRemove;

+ (instancetype) userLoad;

@end
