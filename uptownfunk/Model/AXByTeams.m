//
//  AXByTeams.m
//
//  Created by Kelvin Tong on 16/5/3
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXByTeams.h"


NSString *const kAXByTeamsValue = @"value";
NSString *const kAXByTeamsLabel = @"label";
NSString *const kAXByTeamsName = @"name";


@interface AXByTeams ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXByTeams

@synthesize value = _value;
@synthesize label = _label;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.value = [self objectOrNilForKey:kAXByTeamsValue fromDictionary:dict];
            self.label = [self objectOrNilForKey:kAXByTeamsLabel fromDictionary:dict];
            self.name = [self objectOrNilForKey:kAXByTeamsName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.value forKey:kAXByTeamsValue];
    [mutableDict setValue:self.label forKey:kAXByTeamsLabel];
    [mutableDict setValue:self.name forKey:kAXByTeamsName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.value = [aDecoder decodeObjectForKey:kAXByTeamsValue];
    self.label = [aDecoder decodeObjectForKey:kAXByTeamsLabel];
    self.name = [aDecoder decodeObjectForKey:kAXByTeamsName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_value forKey:kAXByTeamsValue];
    [aCoder encodeObject:_label forKey:kAXByTeamsLabel];
    [aCoder encodeObject:_name forKey:kAXByTeamsName];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXByTeams *copy = [[AXByTeams alloc] init];
    
    if (copy) {

        copy.value = [self.value copyWithZone:zone];
        copy.label = [self.label copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}


@end
