//
//  AXQueries.m
//
//  Created by Kelvin Tong on 16/5/3
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXQueries.h"
#import "AXByTeams.h"
#import "AXByPlayers.h"
#import "AXByStatisticians.h"


NSString *const kAXQueriesByTeams = @"by_teams";
NSString *const kAXQueriesByPlayers = @"by_players";
NSString *const kAXQueriesByStatisticians = @"by_statisticians";


@interface AXQueries ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXQueries

@synthesize byTeams = _byTeams;
@synthesize byPlayers = _byPlayers;
@synthesize byStatisticians = _byStatisticians;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedAXByTeams = [dict objectForKey:kAXQueriesByTeams];
    NSMutableArray *parsedAXByTeams = [NSMutableArray array];
    if ([receivedAXByTeams isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedAXByTeams) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedAXByTeams addObject:[AXByTeams modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedAXByTeams isKindOfClass:[NSDictionary class]]) {
       [parsedAXByTeams addObject:[AXByTeams modelObjectWithDictionary:(NSDictionary *)receivedAXByTeams]];
    }

    self.byTeams = [NSArray arrayWithArray:parsedAXByTeams];
    NSObject *receivedAXByPlayers = [dict objectForKey:kAXQueriesByPlayers];
    NSMutableArray *parsedAXByPlayers = [NSMutableArray array];
    if ([receivedAXByPlayers isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedAXByPlayers) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedAXByPlayers addObject:[AXByPlayers modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedAXByPlayers isKindOfClass:[NSDictionary class]]) {
       [parsedAXByPlayers addObject:[AXByPlayers modelObjectWithDictionary:(NSDictionary *)receivedAXByPlayers]];
    }

    self.byPlayers = [NSArray arrayWithArray:parsedAXByPlayers];
    NSObject *receivedAXByStatisticians = [dict objectForKey:kAXQueriesByStatisticians];
    NSMutableArray *parsedAXByStatisticians = [NSMutableArray array];
    if ([receivedAXByStatisticians isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedAXByStatisticians) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedAXByStatisticians addObject:[AXByStatisticians modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedAXByStatisticians isKindOfClass:[NSDictionary class]]) {
       [parsedAXByStatisticians addObject:[AXByStatisticians modelObjectWithDictionary:(NSDictionary *)receivedAXByStatisticians]];
    }

    self.byStatisticians = [NSArray arrayWithArray:parsedAXByStatisticians];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForByTeams = [NSMutableArray array];
    for (NSObject *subArrayObject in self.byTeams) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForByTeams addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForByTeams addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForByTeams] forKey:kAXQueriesByTeams];
    NSMutableArray *tempArrayForByPlayers = [NSMutableArray array];
    for (NSObject *subArrayObject in self.byPlayers) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForByPlayers addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForByPlayers addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForByPlayers] forKey:kAXQueriesByPlayers];
    NSMutableArray *tempArrayForByStatisticians = [NSMutableArray array];
    for (NSObject *subArrayObject in self.byStatisticians) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForByStatisticians addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForByStatisticians addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForByStatisticians] forKey:kAXQueriesByStatisticians];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.byTeams = [aDecoder decodeObjectForKey:kAXQueriesByTeams];
    self.byPlayers = [aDecoder decodeObjectForKey:kAXQueriesByPlayers];
    self.byStatisticians = [aDecoder decodeObjectForKey:kAXQueriesByStatisticians];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_byTeams forKey:kAXQueriesByTeams];
    [aCoder encodeObject:_byPlayers forKey:kAXQueriesByPlayers];
    [aCoder encodeObject:_byStatisticians forKey:kAXQueriesByStatisticians];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXQueries *copy = [[AXQueries alloc] init];
    
    if (copy) {

        copy.byTeams = [self.byTeams copyWithZone:zone];
        copy.byPlayers = [self.byPlayers copyWithZone:zone];
        copy.byStatisticians = [self.byStatisticians copyWithZone:zone];
    }
    
    return copy;
}


@end
