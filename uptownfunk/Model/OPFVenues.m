//
//  OPFVenues.m
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "OPFVenues.h"


NSString *const kOPFVenuesId = @"id";
NSString *const kOPFVenuesName = @"name";
NSString *const kOPFVenuesType = @"type";


@interface OPFVenues ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OPFVenues

@synthesize venuesIdentifier = _venuesIdentifier;
@synthesize name = _name;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.venuesIdentifier = [self objectOrNilForKey:kOPFVenuesId fromDictionary:dict];
            self.name = [self objectOrNilForKey:kOPFVenuesName fromDictionary:dict];
            self.type = [[self objectOrNilForKey:kOPFVenuesType fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.venuesIdentifier forKey:kOPFVenuesId];
    [mutableDict setValue:self.name forKey:kOPFVenuesName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.type] forKey:kOPFVenuesType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.venuesIdentifier = [aDecoder decodeObjectForKey:kOPFVenuesId];
    self.name = [aDecoder decodeObjectForKey:kOPFVenuesName];
    self.type = [aDecoder decodeDoubleForKey:kOPFVenuesType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_venuesIdentifier forKey:kOPFVenuesId];
    [aCoder encodeObject:_name forKey:kOPFVenuesName];
    [aCoder encodeDouble:_type forKey:kOPFVenuesType];
}

- (id)copyWithZone:(NSZone *)zone
{
    OPFVenues *copy = [[OPFVenues alloc] init];
    
    if (copy) {

        copy.venuesIdentifier = [self.venuesIdentifier copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.type = self.type;
    }
    
    return copy;
}


@end
