//
//  AXStatistician.m
//
//  Created by Kelvin Tong on 16/4/26
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXStatistician.h"
#import "AXArea.h"


NSString *const kAXStatisticianWeight = @"weight";
NSString *const kAXStatisticianStatus = @"status";
NSString *const kAXStatisticianCountryCode = @"country_code";
NSString *const kAXStatisticianAccountRoles = @"account_roles";
NSString *const kAXStatisticianUpdatedAt = @"updated_at";
NSString *const kAXStatisticianIdCard = @"id_card";
NSString *const kAXStatisticianName = @"name";
NSString *const kAXStatisticianNationality = @"nationality";
NSString *const kAXStatisticianPassport = @"passport";
NSString *const kAXStatisticianAvatarUri = @"avatar_uri";
NSString *const kAXStatisticianId = @"id";
NSString *const kAXStatisticianRoles = @"roles";
NSString *const kAXStatisticianBirth = @"birth";
NSString *const kAXStatisticianEmail = @"email";
NSString *const kAXStatisticianGender = @"gender";
NSString *const kAXStatisticianPhone = @"phone";
NSString *const kAXStatisticianHeight = @"height";
NSString *const kAXStatisticianArea = @"area";
NSString *const kAXStatisticianStatAreas = @"stat_areas";
NSString *const kAXStatisticianCreatedAt = @"created_at";
NSString *const kAXStatisticianDescription = @"description";


@interface AXStatistician ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXStatistician

@synthesize weight = _weight;
@synthesize status = _status;
@synthesize countryCode = _countryCode;
@synthesize accountRoles = _accountRoles;
@synthesize updatedAt = _updatedAt;
@synthesize idCard = _idCard;
@synthesize name = _name;
@synthesize nationality = _nationality;
@synthesize passport = _passport;
@synthesize avatarUri = _avatarUri;
@synthesize statisticianIdentifier = _statisticianIdentifier;
@synthesize roles = _roles;
@synthesize birth = _birth;
@synthesize email = _email;
@synthesize gender = _gender;
@synthesize phone = _phone;
@synthesize height = _height;
@synthesize area = _area;
@synthesize statAreas = _statAreas;
@synthesize createdAt = _createdAt;
@synthesize statisticianDescription = _statisticianDescription;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.weight = [[self objectOrNilForKey:kAXStatisticianWeight fromDictionary:dict] doubleValue];
            self.status = [[self objectOrNilForKey:kAXStatisticianStatus fromDictionary:dict] doubleValue];
            self.countryCode = [self objectOrNilForKey:kAXStatisticianCountryCode fromDictionary:dict];
            self.accountRoles = [self objectOrNilForKey:kAXStatisticianAccountRoles fromDictionary:dict];
            self.updatedAt = [self objectOrNilForKey:kAXStatisticianUpdatedAt fromDictionary:dict];
            self.idCard = [self objectOrNilForKey:kAXStatisticianIdCard fromDictionary:dict];
            self.name = [self objectOrNilForKey:kAXStatisticianName fromDictionary:dict];
            self.nationality = [self objectOrNilForKey:kAXStatisticianNationality fromDictionary:dict];
            self.passport = [self objectOrNilForKey:kAXStatisticianPassport fromDictionary:dict];
            self.avatarUri = [self objectOrNilForKey:kAXStatisticianAvatarUri fromDictionary:dict];
            self.statisticianIdentifier = [self objectOrNilForKey:kAXStatisticianId fromDictionary:dict];
            self.roles = [self objectOrNilForKey:kAXStatisticianRoles fromDictionary:dict];
            self.birth = [self objectOrNilForKey:kAXStatisticianBirth fromDictionary:dict];
            self.email = [self objectOrNilForKey:kAXStatisticianEmail fromDictionary:dict];
            self.gender = [[self objectOrNilForKey:kAXStatisticianGender fromDictionary:dict] doubleValue];
            self.phone = [self objectOrNilForKey:kAXStatisticianPhone fromDictionary:dict];
            self.height = [[self objectOrNilForKey:kAXStatisticianHeight fromDictionary:dict] doubleValue];
            self.area = [AXArea modelObjectWithDictionary:[dict objectForKey:kAXStatisticianArea]];
            self.statAreas = [self objectOrNilForKey:kAXStatisticianStatAreas fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kAXStatisticianCreatedAt fromDictionary:dict];
            self.statisticianDescription = [self objectOrNilForKey:kAXStatisticianDescription fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.weight] forKey:kAXStatisticianWeight];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kAXStatisticianStatus];
    [mutableDict setValue:self.countryCode forKey:kAXStatisticianCountryCode];
    NSMutableArray *tempArrayForAccountRoles = [NSMutableArray array];
    for (NSObject *subArrayObject in self.accountRoles) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForAccountRoles addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForAccountRoles addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForAccountRoles] forKey:kAXStatisticianAccountRoles];
    [mutableDict setValue:self.updatedAt forKey:kAXStatisticianUpdatedAt];
    [mutableDict setValue:self.idCard forKey:kAXStatisticianIdCard];
    [mutableDict setValue:self.name forKey:kAXStatisticianName];
    [mutableDict setValue:self.nationality forKey:kAXStatisticianNationality];
    [mutableDict setValue:self.passport forKey:kAXStatisticianPassport];
    [mutableDict setValue:self.avatarUri forKey:kAXStatisticianAvatarUri];
    [mutableDict setValue:self.statisticianIdentifier forKey:kAXStatisticianId];
    NSMutableArray *tempArrayForRoles = [NSMutableArray array];
    for (NSObject *subArrayObject in self.roles) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForRoles addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForRoles addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForRoles] forKey:kAXStatisticianRoles];
    [mutableDict setValue:self.birth forKey:kAXStatisticianBirth];
    [mutableDict setValue:self.email forKey:kAXStatisticianEmail];
    [mutableDict setValue:[NSNumber numberWithDouble:self.gender] forKey:kAXStatisticianGender];
    [mutableDict setValue:self.phone forKey:kAXStatisticianPhone];
    [mutableDict setValue:[NSNumber numberWithDouble:self.height] forKey:kAXStatisticianHeight];
    [mutableDict setValue:[self.area dictionaryRepresentation] forKey:kAXStatisticianArea];
    NSMutableArray *tempArrayForStatAreas = [NSMutableArray array];
    for (NSObject *subArrayObject in self.statAreas) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForStatAreas addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForStatAreas addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForStatAreas] forKey:kAXStatisticianStatAreas];
    [mutableDict setValue:self.createdAt forKey:kAXStatisticianCreatedAt];
    [mutableDict setValue:self.statisticianDescription forKey:kAXStatisticianDescription];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.weight = [aDecoder decodeDoubleForKey:kAXStatisticianWeight];
    self.status = [aDecoder decodeDoubleForKey:kAXStatisticianStatus];
    self.countryCode = [aDecoder decodeObjectForKey:kAXStatisticianCountryCode];
    self.accountRoles = [aDecoder decodeObjectForKey:kAXStatisticianAccountRoles];
    self.updatedAt = [aDecoder decodeObjectForKey:kAXStatisticianUpdatedAt];
    self.idCard = [aDecoder decodeObjectForKey:kAXStatisticianIdCard];
    self.name = [aDecoder decodeObjectForKey:kAXStatisticianName];
    self.nationality = [aDecoder decodeObjectForKey:kAXStatisticianNationality];
    self.passport = [aDecoder decodeObjectForKey:kAXStatisticianPassport];
    self.avatarUri = [aDecoder decodeObjectForKey:kAXStatisticianAvatarUri];
    self.statisticianIdentifier = [aDecoder decodeObjectForKey:kAXStatisticianId];
    self.roles = [aDecoder decodeObjectForKey:kAXStatisticianRoles];
    self.birth = [aDecoder decodeObjectForKey:kAXStatisticianBirth];
    self.email = [aDecoder decodeObjectForKey:kAXStatisticianEmail];
    self.gender = [aDecoder decodeDoubleForKey:kAXStatisticianGender];
    self.phone = [aDecoder decodeObjectForKey:kAXStatisticianPhone];
    self.height = [aDecoder decodeDoubleForKey:kAXStatisticianHeight];
    self.area = [aDecoder decodeObjectForKey:kAXStatisticianArea];
    self.statAreas = [aDecoder decodeObjectForKey:kAXStatisticianStatAreas];
    self.createdAt = [aDecoder decodeObjectForKey:kAXStatisticianCreatedAt];
    self.statisticianDescription = [aDecoder decodeObjectForKey:kAXStatisticianDescription];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_weight forKey:kAXStatisticianWeight];
    [aCoder encodeDouble:_status forKey:kAXStatisticianStatus];
    [aCoder encodeObject:_countryCode forKey:kAXStatisticianCountryCode];
    [aCoder encodeObject:_accountRoles forKey:kAXStatisticianAccountRoles];
    [aCoder encodeObject:_updatedAt forKey:kAXStatisticianUpdatedAt];
    [aCoder encodeObject:_idCard forKey:kAXStatisticianIdCard];
    [aCoder encodeObject:_name forKey:kAXStatisticianName];
    [aCoder encodeObject:_nationality forKey:kAXStatisticianNationality];
    [aCoder encodeObject:_passport forKey:kAXStatisticianPassport];
    [aCoder encodeObject:_avatarUri forKey:kAXStatisticianAvatarUri];
    [aCoder encodeObject:_statisticianIdentifier forKey:kAXStatisticianId];
    [aCoder encodeObject:_roles forKey:kAXStatisticianRoles];
    [aCoder encodeObject:_birth forKey:kAXStatisticianBirth];
    [aCoder encodeObject:_email forKey:kAXStatisticianEmail];
    [aCoder encodeDouble:_gender forKey:kAXStatisticianGender];
    [aCoder encodeObject:_phone forKey:kAXStatisticianPhone];
    [aCoder encodeDouble:_height forKey:kAXStatisticianHeight];
    [aCoder encodeObject:_area forKey:kAXStatisticianArea];
    [aCoder encodeObject:_statAreas forKey:kAXStatisticianStatAreas];
    [aCoder encodeObject:_createdAt forKey:kAXStatisticianCreatedAt];
    [aCoder encodeObject:_statisticianDescription forKey:kAXStatisticianDescription];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXStatistician *copy = [[AXStatistician alloc] init];
    
    if (copy) {

        copy.weight = self.weight;
        copy.status = self.status;
        copy.countryCode = [self.countryCode copyWithZone:zone];
        copy.accountRoles = [self.accountRoles copyWithZone:zone];
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.idCard = [self.idCard copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.nationality = [self.nationality copyWithZone:zone];
        copy.passport = [self.passport copyWithZone:zone];
        copy.avatarUri = [self.avatarUri copyWithZone:zone];
        copy.statisticianIdentifier = [self.statisticianIdentifier copyWithZone:zone];
        copy.roles = [self.roles copyWithZone:zone];
        copy.birth = [self.birth copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.gender = self.gender;
        copy.phone = [self.phone copyWithZone:zone];
        copy.height = self.height;
        copy.area = [self.area copyWithZone:zone];
        copy.statAreas = [self.statAreas copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.statisticianDescription = [self.statisticianDescription copyWithZone:zone];
    }
    
    return copy;
}


@end
