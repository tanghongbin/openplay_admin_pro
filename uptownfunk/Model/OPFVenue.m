//
//  OPFVenue.m
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "OPFVenue.h"
#import "OPFStadium.h"


NSString *const kOPFVenueId = @"id";
NSString *const kOPFVenueStadium = @"stadium";
NSString *const kOPFVenueName = @"name";
NSString *const kOPFVenueType = @"type";


@interface OPFVenue ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OPFVenue

@synthesize venueIdentifier = _venueIdentifier;
@synthesize stadium = _stadium;
@synthesize name = _name;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.venueIdentifier = [self objectOrNilForKey:kOPFVenueId fromDictionary:dict];
            self.stadium = [OPFStadium modelObjectWithDictionary:[dict objectForKey:kOPFVenueStadium]];
            self.name = [self objectOrNilForKey:kOPFVenueName fromDictionary:dict];
            self.type = [[self objectOrNilForKey:kOPFVenueType fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.venueIdentifier forKey:kOPFVenueId];
    [mutableDict setValue:[self.stadium dictionaryRepresentation] forKey:kOPFVenueStadium];
    [mutableDict setValue:self.name forKey:kOPFVenueName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.type] forKey:kOPFVenueType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.venueIdentifier = [aDecoder decodeObjectForKey:kOPFVenueId];
    self.stadium = [aDecoder decodeObjectForKey:kOPFVenueStadium];
    self.name = [aDecoder decodeObjectForKey:kOPFVenueName];
    self.type = [aDecoder decodeDoubleForKey:kOPFVenueType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_venueIdentifier forKey:kOPFVenueId];
    [aCoder encodeObject:_stadium forKey:kOPFVenueStadium];
    [aCoder encodeObject:_name forKey:kOPFVenueName];
    [aCoder encodeDouble:_type forKey:kOPFVenueType];
}

- (id)copyWithZone:(NSZone *)zone
{
    OPFVenue *copy = [[OPFVenue alloc] init];
    
    if (copy) {

        copy.venueIdentifier = [self.venueIdentifier copyWithZone:zone];
        copy.stadium = [self.stadium copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.type = self.type;
    }
    
    return copy;
}


@end
