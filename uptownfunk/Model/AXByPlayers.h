//
//  AXByPlayers.h
//
//  Created by Kelvin Tong on 16/5/3
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface AXByPlayers : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double shirtNumber;
@property (nonatomic, strong) NSString *byPlayersIdentifier;
@property (nonatomic, strong) NSString *teamType;
@property (nonatomic, strong) NSString *teamLabel;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *teamId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
