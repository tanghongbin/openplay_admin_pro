//
//  AXByPlayers.m
//
//  Created by Kelvin Tong on 16/5/3
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXByPlayers.h"


NSString *const kAXByPlayersShirtNumber = @"shirt_number";
NSString *const kAXByPlayersId = @"id";
NSString *const kAXByPlayersTeamType = @"team_type";
NSString *const kAXByPlayersTeamLabel = @"team_label";
NSString *const kAXByPlayersName = @"name";
NSString *const kAXByPlayersTeamId = @"team_id";


@interface AXByPlayers ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXByPlayers

@synthesize shirtNumber = _shirtNumber;
@synthesize byPlayersIdentifier = _byPlayersIdentifier;
@synthesize teamType = _teamType;
@synthesize teamLabel = _teamLabel;
@synthesize name = _name;
@synthesize teamId = _teamId;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.shirtNumber = [[self objectOrNilForKey:kAXByPlayersShirtNumber fromDictionary:dict] doubleValue];
            self.byPlayersIdentifier = [self objectOrNilForKey:kAXByPlayersId fromDictionary:dict];
            self.teamType = [self objectOrNilForKey:kAXByPlayersTeamType fromDictionary:dict];
            self.teamLabel = [self objectOrNilForKey:kAXByPlayersTeamLabel fromDictionary:dict];
            self.name = [self objectOrNilForKey:kAXByPlayersName fromDictionary:dict];
            self.teamId = [self objectOrNilForKey:kAXByPlayersTeamId fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.shirtNumber] forKey:kAXByPlayersShirtNumber];
    [mutableDict setValue:self.byPlayersIdentifier forKey:kAXByPlayersId];
    [mutableDict setValue:self.teamType forKey:kAXByPlayersTeamType];
    [mutableDict setValue:self.teamLabel forKey:kAXByPlayersTeamLabel];
    [mutableDict setValue:self.name forKey:kAXByPlayersName];
    [mutableDict setValue:self.teamId forKey:kAXByPlayersTeamId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.shirtNumber = [aDecoder decodeDoubleForKey:kAXByPlayersShirtNumber];
    self.byPlayersIdentifier = [aDecoder decodeObjectForKey:kAXByPlayersId];
    self.teamType = [aDecoder decodeObjectForKey:kAXByPlayersTeamType];
    self.teamLabel = [aDecoder decodeObjectForKey:kAXByPlayersTeamLabel];
    self.name = [aDecoder decodeObjectForKey:kAXByPlayersName];
    self.teamId = [aDecoder decodeObjectForKey:kAXByPlayersTeamId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_shirtNumber forKey:kAXByPlayersShirtNumber];
    [aCoder encodeObject:_byPlayersIdentifier forKey:kAXByPlayersId];
    [aCoder encodeObject:_teamType forKey:kAXByPlayersTeamType];
    [aCoder encodeObject:_teamLabel forKey:kAXByPlayersTeamLabel];
    [aCoder encodeObject:_name forKey:kAXByPlayersName];
    [aCoder encodeObject:_teamId forKey:kAXByPlayersTeamId];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXByPlayers *copy = [[AXByPlayers alloc] init];
    
    if (copy) {

        copy.shirtNumber = self.shirtNumber;
        copy.byPlayersIdentifier = [self.byPlayersIdentifier copyWithZone:zone];
        copy.teamType = [self.teamType copyWithZone:zone];
        copy.teamLabel = [self.teamLabel copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.teamId = [self.teamId copyWithZone:zone];
    }
    
    return copy;
}


@end
