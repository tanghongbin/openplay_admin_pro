//
//  OPFWeather.m
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "OPFWeather.h"


NSString *const kOPFWeatherTemperature = @"temperature";
NSString *const kOPFWeatherWind = @"wind";
NSString *const kOPFWeatherPrecipitation = @"precipitation";
NSString *const kOPFWeatherDescription = @"description";


@interface OPFWeather ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OPFWeather

@synthesize temperature = _temperature;
@synthesize wind = _wind;
@synthesize precipitation = _precipitation;
@synthesize weatherDescription = _weatherDescription;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.temperature = [self objectOrNilForKey:kOPFWeatherTemperature fromDictionary:dict];
            self.wind = [self objectOrNilForKey:kOPFWeatherWind fromDictionary:dict];
            self.precipitation = [self objectOrNilForKey:kOPFWeatherPrecipitation fromDictionary:dict];
            self.weatherDescription = [self objectOrNilForKey:kOPFWeatherDescription fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.temperature forKey:kOPFWeatherTemperature];
    [mutableDict setValue:self.wind forKey:kOPFWeatherWind];
    [mutableDict setValue:self.precipitation forKey:kOPFWeatherPrecipitation];
    [mutableDict setValue:self.weatherDescription forKey:kOPFWeatherDescription];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.temperature = [aDecoder decodeObjectForKey:kOPFWeatherTemperature];
    self.wind = [aDecoder decodeObjectForKey:kOPFWeatherWind];
    self.precipitation = [aDecoder decodeObjectForKey:kOPFWeatherPrecipitation];
    self.weatherDescription = [aDecoder decodeObjectForKey:kOPFWeatherDescription];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_temperature forKey:kOPFWeatherTemperature];
    [aCoder encodeObject:_wind forKey:kOPFWeatherWind];
    [aCoder encodeObject:_precipitation forKey:kOPFWeatherPrecipitation];
    [aCoder encodeObject:_weatherDescription forKey:kOPFWeatherDescription];
}

- (id)copyWithZone:(NSZone *)zone
{
    OPFWeather *copy = [[OPFWeather alloc] init];
    
    if (copy) {

        copy.temperature = [self.temperature copyWithZone:zone];
        copy.wind = [self.wind copyWithZone:zone];
        copy.precipitation = [self.precipitation copyWithZone:zone];
        copy.weatherDescription = [self.weatherDescription copyWithZone:zone];
    }
    
    return copy;
}


@end
