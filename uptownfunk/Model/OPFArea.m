//
//  OPFArea.m
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "OPFArea.h"


NSString *const kOPFAreaDistrict = @"district";
NSString *const kOPFAreaCountry = @"country";
NSString *const kOPFAreaProvince = @"province";
NSString *const kOPFAreaCity = @"city";


@interface OPFArea ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OPFArea

@synthesize district = _district;
@synthesize country = _country;
@synthesize province = _province;
@synthesize city = _city;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.district = [self objectOrNilForKey:kOPFAreaDistrict fromDictionary:dict];
            self.country = [self objectOrNilForKey:kOPFAreaCountry fromDictionary:dict];
            self.province = [self objectOrNilForKey:kOPFAreaProvince fromDictionary:dict];
            self.city = [self objectOrNilForKey:kOPFAreaCity fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.district forKey:kOPFAreaDistrict];
    [mutableDict setValue:self.country forKey:kOPFAreaCountry];
    [mutableDict setValue:self.province forKey:kOPFAreaProvince];
    [mutableDict setValue:self.city forKey:kOPFAreaCity];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.district = [aDecoder decodeObjectForKey:kOPFAreaDistrict];
    self.country = [aDecoder decodeObjectForKey:kOPFAreaCountry];
    self.province = [aDecoder decodeObjectForKey:kOPFAreaProvince];
    self.city = [aDecoder decodeObjectForKey:kOPFAreaCity];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_district forKey:kOPFAreaDistrict];
    [aCoder encodeObject:_country forKey:kOPFAreaCountry];
    [aCoder encodeObject:_province forKey:kOPFAreaProvince];
    [aCoder encodeObject:_city forKey:kOPFAreaCity];
}

- (id)copyWithZone:(NSZone *)zone
{
    OPFArea *copy = [[OPFArea alloc] init];
    
    if (copy) {

        copy.district = [self.district copyWithZone:zone];
        copy.country = [self.country copyWithZone:zone];
        copy.province = [self.province copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
    }
    
    return copy;
}


@end
