//
//  OPFList.h
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AXTeam, OPFStadium, OPFVenue, OPFWeather;

@interface AXMatch : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *matchStatisticians;
@property (nonatomic, assign) double matchType;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, assign) double hsB;
@property (nonatomic, strong) NSString *endAt;
@property (nonatomic, assign) double ruleExtraTime;
@property (nonatomic, strong) AXTeam *teamB;
@property (nonatomic, strong) OPFStadium *stadium;
@property (nonatomic, assign) double fsB;
@property (nonatomic, assign) double ruleStoppedWatch;
@property (nonatomic, strong) NSString *opId;
@property (nonatomic, strong) NSString *listIdentifier;
@property (nonatomic, strong) NSString *matchTitle;
@property (nonatomic, assign) double ruleHalfTime;
@property (nonatomic, strong) AXTeam *teamA;
@property (nonatomic, assign) double hsA;
@property (nonatomic, assign) double sA;
@property (nonatomic, strong) OPFVenue *venue;
@property (nonatomic, assign) double statsMode;
@property (nonatomic, strong) NSString *matchStyle;
@property (nonatomic, strong) OPFWeather *weather;
@property (nonatomic, strong) NSString *matchPeriod;
@property (nonatomic, assign) double sB;
@property (nonatomic, strong) NSString *startAt;
@property (nonatomic, assign) double fsA;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
