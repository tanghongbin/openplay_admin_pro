//
//  AXList.h
//
//  Created by Kelvin Tong on 16/4/26
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AXStatistician, AXMatch, AXTeam, AXPlayer;

@interface AXEvent : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *listIdentifier;
@property (nonatomic, assign) double clientMatchClock;
@property (nonatomic, assign) double shirtNumber;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) AXStatistician *statistician;
@property (nonatomic, strong) AXMatch *match;
@property (nonatomic, strong) AXTeam *team;
@property (nonatomic, assign) double code;
@property (nonatomic, assign) double set_piece;
@property (nonatomic, assign) double clientDuration;
@property (nonatomic, strong) NSString *clientEndedAt;
@property (nonatomic, strong) NSString *clientStartedAt;
@property (nonatomic, strong) AXPlayer *player;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double status;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
