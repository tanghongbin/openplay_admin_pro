//
//  AXByStatisticians.m
//
//  Created by Kelvin Tong on 16/5/3
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXByStatisticians.h"
#import "AXTeam.h"


NSString *const kAXByStatisticiansRelationId = @"relation_id";
NSString *const kAXByStatisticiansTeamLabel = @"team_label";
NSString *const kAXByStatisticiansId = @"id";
NSString *const kAXByStatisticiansPhone = @"phone";
NSString *const kAXByStatisticiansStatisticianId = @"statistician_id";
NSString *const kAXByStatisticiansTeam = @"team";
NSString *const kAXByStatisticiansAvatarUri = @"avatar_uri";
NSString *const kAXByStatisticiansTeamType = @"team_type";
NSString *const kAXByStatisticiansRole = @"role";
NSString *const kAXByStatisticiansTeamId = @"team_id";
NSString *const kAXByStatisticiansCountryCode = @"country_code";
NSString *const kAXByStatisticiansName = @"name";


@interface AXByStatisticians ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXByStatisticians

@synthesize relationId = _relationId;
@synthesize teamLabel = _teamLabel;
@synthesize byStatisticiansIdentifier = _byStatisticiansIdentifier;
@synthesize phone = _phone;
@synthesize statisticianId = _statisticianId;
@synthesize team = _team;
@synthesize avatarUri = _avatarUri;
@synthesize teamType = _teamType;
@synthesize role = _role;
@synthesize teamId = _teamId;
@synthesize countryCode = _countryCode;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.relationId = [self objectOrNilForKey:kAXByStatisticiansRelationId fromDictionary:dict];
            self.teamLabel = [self objectOrNilForKey:kAXByStatisticiansTeamLabel fromDictionary:dict];
            self.byStatisticiansIdentifier = [self objectOrNilForKey:kAXByStatisticiansId fromDictionary:dict];
            self.phone = [self objectOrNilForKey:kAXByStatisticiansPhone fromDictionary:dict];
            self.statisticianId = [self objectOrNilForKey:kAXByStatisticiansStatisticianId fromDictionary:dict];
            self.team = [AXTeam modelObjectWithDictionary:[dict objectForKey:kAXByStatisticiansTeam]];
            self.avatarUri = [self objectOrNilForKey:kAXByStatisticiansAvatarUri fromDictionary:dict];
            self.teamType = [self objectOrNilForKey:kAXByStatisticiansTeamType fromDictionary:dict];
            self.role = [self objectOrNilForKey:kAXByStatisticiansRole fromDictionary:dict];
            self.teamId = [self objectOrNilForKey:kAXByStatisticiansTeamId fromDictionary:dict];
            self.countryCode = [self objectOrNilForKey:kAXByStatisticiansCountryCode fromDictionary:dict];
            self.name = [self objectOrNilForKey:kAXByStatisticiansName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.relationId forKey:kAXByStatisticiansRelationId];
    [mutableDict setValue:self.teamLabel forKey:kAXByStatisticiansTeamLabel];
    [mutableDict setValue:self.byStatisticiansIdentifier forKey:kAXByStatisticiansId];
    [mutableDict setValue:self.phone forKey:kAXByStatisticiansPhone];
    [mutableDict setValue:self.statisticianId forKey:kAXByStatisticiansStatisticianId];
    [mutableDict setValue:[self.team dictionaryRepresentation] forKey:kAXByStatisticiansTeam];
    [mutableDict setValue:self.avatarUri forKey:kAXByStatisticiansAvatarUri];
    [mutableDict setValue:self.teamType forKey:kAXByStatisticiansTeamType];
    [mutableDict setValue:self.role forKey:kAXByStatisticiansRole];
    [mutableDict setValue:self.teamId forKey:kAXByStatisticiansTeamId];
    [mutableDict setValue:self.countryCode forKey:kAXByStatisticiansCountryCode];
    [mutableDict setValue:self.name forKey:kAXByStatisticiansName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.relationId = [aDecoder decodeObjectForKey:kAXByStatisticiansRelationId];
    self.teamLabel = [aDecoder decodeObjectForKey:kAXByStatisticiansTeamLabel];
    self.byStatisticiansIdentifier = [aDecoder decodeObjectForKey:kAXByStatisticiansId];
    self.phone = [aDecoder decodeObjectForKey:kAXByStatisticiansPhone];
    self.statisticianId = [aDecoder decodeObjectForKey:kAXByStatisticiansStatisticianId];
    self.team = [aDecoder decodeObjectForKey:kAXByStatisticiansTeam];
    self.avatarUri = [aDecoder decodeObjectForKey:kAXByStatisticiansAvatarUri];
    self.teamType = [aDecoder decodeObjectForKey:kAXByStatisticiansTeamType];
    self.role = [aDecoder decodeObjectForKey:kAXByStatisticiansRole];
    self.teamId = [aDecoder decodeObjectForKey:kAXByStatisticiansTeamId];
    self.countryCode = [aDecoder decodeObjectForKey:kAXByStatisticiansCountryCode];
    self.name = [aDecoder decodeObjectForKey:kAXByStatisticiansName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_relationId forKey:kAXByStatisticiansRelationId];
    [aCoder encodeObject:_teamLabel forKey:kAXByStatisticiansTeamLabel];
    [aCoder encodeObject:_byStatisticiansIdentifier forKey:kAXByStatisticiansId];
    [aCoder encodeObject:_phone forKey:kAXByStatisticiansPhone];
    [aCoder encodeObject:_statisticianId forKey:kAXByStatisticiansStatisticianId];
    [aCoder encodeObject:_team forKey:kAXByStatisticiansTeam];
    [aCoder encodeObject:_avatarUri forKey:kAXByStatisticiansAvatarUri];
    [aCoder encodeObject:_teamType forKey:kAXByStatisticiansTeamType];
    [aCoder encodeObject:_role forKey:kAXByStatisticiansRole];
    [aCoder encodeObject:_teamId forKey:kAXByStatisticiansTeamId];
    [aCoder encodeObject:_countryCode forKey:kAXByStatisticiansCountryCode];
    [aCoder encodeObject:_name forKey:kAXByStatisticiansName];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXByStatisticians *copy = [[AXByStatisticians alloc] init];
    
    if (copy) {

        copy.relationId = [self.relationId copyWithZone:zone];
        copy.teamLabel = [self.teamLabel copyWithZone:zone];
        copy.byStatisticiansIdentifier = [self.byStatisticiansIdentifier copyWithZone:zone];
        copy.phone = [self.phone copyWithZone:zone];
        copy.statisticianId = [self.statisticianId copyWithZone:zone];
        copy.team = [self.team copyWithZone:zone];
        copy.avatarUri = [self.avatarUri copyWithZone:zone];
        copy.teamType = [self.teamType copyWithZone:zone];
        copy.role = [self.role copyWithZone:zone];
        copy.teamId = [self.teamId copyWithZone:zone];
        copy.countryCode = [self.countryCode copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}


@end
