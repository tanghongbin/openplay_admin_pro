//
//  AXQueries.h
//
//  Created by Kelvin Tong on 16/5/3
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface AXQueries : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *byTeams;
@property (nonatomic, strong) NSArray *byPlayers;
@property (nonatomic, strong) NSArray *byStatisticians;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
