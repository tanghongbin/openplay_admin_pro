//
//  AXTeam.m
//
//  Created by Kelvin Tong on 16/4/26
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXTeam.h"
#import "AXArea.h"


NSString *const kAXTeamStatus = @"status";
NSString *const kAXTeamArea = @"area";
NSString *const kAXTeamOpId = @"op_id";
NSString *const kAXTeamId = @"id";
NSString *const kAXTeamCreatedAt = @"created_at";
NSString *const kAXTeamTeamStyle = @"team_style";
NSString *const kAXTeamLogoUri = @"logo_uri";
NSString *const kAXTeamShortName = @"short_name";
NSString *const kAXTeamUpdatedAt = @"updated_at";
NSString *const kAXTeamName = @"name";


@interface AXTeam ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXTeam

@synthesize status = _status;
@synthesize area = _area;
@synthesize opId = _opId;
@synthesize teamIdentifier = _teamIdentifier;
@synthesize createdAt = _createdAt;
@synthesize teamStyle = _teamStyle;
@synthesize logoUri = _logoUri;
@synthesize shortName = _shortName;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [[self objectOrNilForKey:kAXTeamStatus fromDictionary:dict] doubleValue];
            self.area = [AXArea modelObjectWithDictionary:[dict objectForKey:kAXTeamArea]];
            self.opId = [self objectOrNilForKey:kAXTeamOpId fromDictionary:dict];
            self.teamIdentifier = [self objectOrNilForKey:kAXTeamId fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kAXTeamCreatedAt fromDictionary:dict];
            self.teamStyle = [self objectOrNilForKey:kAXTeamTeamStyle fromDictionary:dict];
            self.logoUri = [self objectOrNilForKey:kAXTeamLogoUri fromDictionary:dict];
            self.shortName = [self objectOrNilForKey:kAXTeamShortName fromDictionary:dict];
            self.updatedAt = [self objectOrNilForKey:kAXTeamUpdatedAt fromDictionary:dict];
            self.name = [self objectOrNilForKey:kAXTeamName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kAXTeamStatus];
    [mutableDict setValue:[self.area dictionaryRepresentation] forKey:kAXTeamArea];
    [mutableDict setValue:self.opId forKey:kAXTeamOpId];
    [mutableDict setValue:self.teamIdentifier forKey:kAXTeamId];
    [mutableDict setValue:self.createdAt forKey:kAXTeamCreatedAt];
    [mutableDict setValue:self.teamStyle forKey:kAXTeamTeamStyle];
    [mutableDict setValue:self.logoUri forKey:kAXTeamLogoUri];
    [mutableDict setValue:self.shortName forKey:kAXTeamShortName];
    [mutableDict setValue:self.updatedAt forKey:kAXTeamUpdatedAt];
    [mutableDict setValue:self.name forKey:kAXTeamName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.status = [aDecoder decodeDoubleForKey:kAXTeamStatus];
    self.area = [aDecoder decodeObjectForKey:kAXTeamArea];
    self.opId = [aDecoder decodeObjectForKey:kAXTeamOpId];
    self.teamIdentifier = [aDecoder decodeObjectForKey:kAXTeamId];
    self.createdAt = [aDecoder decodeObjectForKey:kAXTeamCreatedAt];
    self.teamStyle = [aDecoder decodeObjectForKey:kAXTeamTeamStyle];
    self.logoUri = [aDecoder decodeObjectForKey:kAXTeamLogoUri];
    self.shortName = [aDecoder decodeObjectForKey:kAXTeamShortName];
    self.updatedAt = [aDecoder decodeObjectForKey:kAXTeamUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kAXTeamName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_status forKey:kAXTeamStatus];
    [aCoder encodeObject:_area forKey:kAXTeamArea];
    [aCoder encodeObject:_opId forKey:kAXTeamOpId];
    [aCoder encodeObject:_teamIdentifier forKey:kAXTeamId];
    [aCoder encodeObject:_createdAt forKey:kAXTeamCreatedAt];
    [aCoder encodeObject:_teamStyle forKey:kAXTeamTeamStyle];
    [aCoder encodeObject:_logoUri forKey:kAXTeamLogoUri];
    [aCoder encodeObject:_shortName forKey:kAXTeamShortName];
    [aCoder encodeObject:_updatedAt forKey:kAXTeamUpdatedAt];
    [aCoder encodeObject:_name forKey:kAXTeamName];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXTeam *copy = [[AXTeam alloc] init];
    
    if (copy) {

        copy.status = self.status;
        copy.area = [self.area copyWithZone:zone];
        copy.opId = [self.opId copyWithZone:zone];
        copy.teamIdentifier = [self.teamIdentifier copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.teamStyle = [self.teamStyle copyWithZone:zone];
        copy.logoUri = [self.logoUri copyWithZone:zone];
        copy.shortName = [self.shortName copyWithZone:zone];
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}


@end
