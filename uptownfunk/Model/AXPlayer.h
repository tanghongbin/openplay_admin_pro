//
//  AXPlayer.h
//
//  Created by Kelvin Tong on 16/4/26
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AXArea;

@interface AXPlayer : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double weight;
@property (nonatomic, assign) double status;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSArray *accountRoles;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, strong) NSString *idCard;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *nationality;
@property (nonatomic, strong) NSString *passport;
@property (nonatomic, strong) NSString *avatarUri;
@property (nonatomic, strong) NSString *playerIdentifier;
@property (nonatomic, strong) NSArray *roles;
@property (nonatomic, strong) NSString *birth;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) double gender;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, assign) double height;
@property (nonatomic, strong) AXArea *area;
@property (nonatomic, strong) NSArray *statAreas;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *playerDescription;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
