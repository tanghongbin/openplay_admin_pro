//
//  OPFBaseClass.h
//
//  Created by Kelvin Tong on 16/4/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface OPFMatches_Day : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *list;
@property (nonatomic, assign) double lastId;
@property (nonatomic, assign) double lastLeft;
@property (nonatomic, assign) double total;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
