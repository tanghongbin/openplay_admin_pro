//
//  AXByStatisticians.h
//
//  Created by Kelvin Tong on 16/5/3
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AXTeam;

@interface AXByStatisticians : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *relationId;
@property (nonatomic, strong) NSString *teamLabel;
@property (nonatomic, strong) NSString *byStatisticiansIdentifier;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *statisticianId;
@property (nonatomic, strong) AXTeam *team;
@property (nonatomic, strong) NSString *avatarUri;
@property (nonatomic, strong) NSString *teamType;
@property (nonatomic, strong) NSString *role;
@property (nonatomic, strong) NSString *teamId;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *name;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
