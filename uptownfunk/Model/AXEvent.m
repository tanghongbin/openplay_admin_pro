//
//  AXList.m
//
//  Created by Kelvin Tong on 16/4/26
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXEvent.h"
#import "AXStatistician.h"
#import "AXMatch.h"
#import "AXTeam.h"
#import "AXPlayer.h"


NSString *const kAXListId = @"id";
NSString *const kAXListClientMatchClock = @"client_match_clock";
NSString *const kAXListShirtNumber = @"shirt_number";
NSString *const kAXListCreatedAt = @"created_at";
NSString *const kAXListStatistician = @"statistician";
NSString *const kAXListMatch = @"match";
NSString *const kAXListTeam = @"team";
NSString *const kAXListCode = @"code";
NSString *const kAXListSetPiece = @"set_piece";
NSString *const kAXListClientDuration = @"client_duration";
NSString *const kAXListClientEndedAt = @"client_ended_at";
NSString *const kAXListClientStartedAt = @"client_started_at";
NSString *const kAXListPlayer = @"player";
NSString *const kAXListName = @"name";
NSString *const kAXListStatus = @"status";


@interface AXEvent ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXEvent

@synthesize listIdentifier = _listIdentifier;
@synthesize clientMatchClock = _clientMatchClock;
@synthesize shirtNumber = _shirtNumber;
@synthesize createdAt = _createdAt;
@synthesize statistician = _statistician;
@synthesize match = _match;
@synthesize team = _team;
@synthesize code = _code;
@synthesize set_piece = _set_piece;
@synthesize clientDuration = _clientDuration;
@synthesize clientEndedAt = _clientEndedAt;
@synthesize clientStartedAt = _clientStartedAt;
@synthesize player = _player;
@synthesize name = _name;
@synthesize status = _status;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.listIdentifier = [self objectOrNilForKey:kAXListId fromDictionary:dict];
        self.clientMatchClock = [[self objectOrNilForKey:kAXListClientMatchClock fromDictionary:dict] doubleValue];
        self.shirtNumber = [[self objectOrNilForKey:kAXListShirtNumber fromDictionary:dict] doubleValue];
        self.createdAt = [self objectOrNilForKey:kAXListCreatedAt fromDictionary:dict];
        self.statistician = [AXStatistician modelObjectWithDictionary:[dict objectForKey:kAXListStatistician]];
        self.match = [AXMatch modelObjectWithDictionary:[dict objectForKey:kAXListMatch]];
        self.team = [AXTeam modelObjectWithDictionary:[dict objectForKey:kAXListTeam]];
        self.code = [[self objectOrNilForKey:kAXListCode fromDictionary:dict] doubleValue];
        self.set_piece = [[self objectOrNilForKey:kAXListSetPiece fromDictionary:dict] doubleValue];
        self.clientDuration = [[self objectOrNilForKey:kAXListClientDuration fromDictionary:dict] doubleValue];
        self.clientEndedAt = [self objectOrNilForKey:kAXListClientEndedAt fromDictionary:dict];
        self.clientStartedAt = [self objectOrNilForKey:kAXListClientStartedAt fromDictionary:dict];
        self.player = [AXPlayer modelObjectWithDictionary:[dict objectForKey:kAXListPlayer]];
        self.name = [self objectOrNilForKey:kAXListName fromDictionary:dict];
        self.status = [[self objectOrNilForKey:kAXListStatus fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.listIdentifier forKey:kAXListId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.clientMatchClock] forKey:kAXListClientMatchClock];
    [mutableDict setValue:[NSNumber numberWithDouble:self.shirtNumber] forKey:kAXListShirtNumber];
    [mutableDict setValue:self.createdAt forKey:kAXListCreatedAt];
    [mutableDict setValue:[self.statistician dictionaryRepresentation] forKey:kAXListStatistician];
    [mutableDict setValue:[self.match dictionaryRepresentation] forKey:kAXListMatch];
    [mutableDict setValue:[self.team dictionaryRepresentation] forKey:kAXListTeam];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kAXListCode];
    [mutableDict setValue:[NSNumber numberWithDouble:self.set_piece] forKey:kAXListSetPiece];
    [mutableDict setValue:[NSNumber numberWithDouble:self.clientDuration] forKey:kAXListClientDuration];
    [mutableDict setValue:self.clientEndedAt forKey:kAXListClientEndedAt];
    [mutableDict setValue:self.clientStartedAt forKey:kAXListClientStartedAt];
    [mutableDict setValue:[self.player dictionaryRepresentation] forKey:kAXListPlayer];
    [mutableDict setValue:self.name forKey:kAXListName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kAXListStatus];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.listIdentifier = [aDecoder decodeObjectForKey:kAXListId];
    self.clientMatchClock = [aDecoder decodeDoubleForKey:kAXListClientMatchClock];
    self.shirtNumber = [aDecoder decodeDoubleForKey:kAXListShirtNumber];
    self.createdAt = [aDecoder decodeObjectForKey:kAXListCreatedAt];
    self.statistician = [aDecoder decodeObjectForKey:kAXListStatistician];
    self.match = [aDecoder decodeObjectForKey:kAXListMatch];
    self.team = [aDecoder decodeObjectForKey:kAXListTeam];
    self.code = [aDecoder decodeDoubleForKey:kAXListCode];
    self.set_piece = [aDecoder decodeDoubleForKey:kAXListSetPiece];
    self.clientDuration = [aDecoder decodeDoubleForKey:kAXListClientDuration];
    self.clientEndedAt = [aDecoder decodeObjectForKey:kAXListClientEndedAt];
    self.clientStartedAt = [aDecoder decodeObjectForKey:kAXListClientStartedAt];
    self.player = [aDecoder decodeObjectForKey:kAXListPlayer];
    self.name = [aDecoder decodeObjectForKey:kAXListName];
    self.status = [aDecoder decodeDoubleForKey:kAXListStatus];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_listIdentifier forKey:kAXListId];
    [aCoder encodeDouble:_clientMatchClock forKey:kAXListClientMatchClock];
    [aCoder encodeDouble:_shirtNumber forKey:kAXListShirtNumber];
    [aCoder encodeObject:_createdAt forKey:kAXListCreatedAt];
    [aCoder encodeObject:_statistician forKey:kAXListStatistician];
    [aCoder encodeObject:_match forKey:kAXListMatch];
    [aCoder encodeObject:_team forKey:kAXListTeam];
    [aCoder encodeDouble:_code forKey:kAXListCode];
    [aCoder encodeDouble:_set_piece forKey:kAXListSetPiece];
    [aCoder encodeDouble:_clientDuration forKey:kAXListClientDuration];
    [aCoder encodeObject:_clientEndedAt forKey:kAXListClientEndedAt];
    [aCoder encodeObject:_clientStartedAt forKey:kAXListClientStartedAt];
    [aCoder encodeObject:_player forKey:kAXListPlayer];
    [aCoder encodeObject:_name forKey:kAXListName];
    [aCoder encodeDouble:_status forKey:kAXListStatus];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXEvent *copy = [[AXEvent alloc] init];
    
    if (copy) {

        copy.listIdentifier = [self.listIdentifier copyWithZone:zone];
        copy.clientMatchClock = self.clientMatchClock;
        copy.shirtNumber = self.shirtNumber;
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.statistician = [self.statistician copyWithZone:zone];
        copy.match = [self.match copyWithZone:zone];
        copy.team = [self.team copyWithZone:zone];
        copy.code = self.code;
        copy.set_piece = self.set_piece;
        copy.clientDuration = self.clientDuration;
        copy.clientEndedAt = [self.clientEndedAt copyWithZone:zone];
        copy.clientStartedAt = [self.clientStartedAt copyWithZone:zone];
        copy.player = [self.player copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.status = self.status;
    }
    
    return copy;
}


@end
