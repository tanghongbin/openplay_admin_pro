//
//  AXBaseClass.m
//
//  Created by Kelvin Tong on 16/4/18
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AXUser.h"


NSString *const kAXBaseClassRoles = @"roles";
NSString *const kAXBaseClassId = @"id";
NSString *const kAXBaseClassCreatedAt = @"created_at";
NSString *const kAXBaseClassLastOpTime = @"last_op_time";
NSString *const kAXBaseClassLastLoginAt = @"last_login_at";
NSString *const kAXBaseClassEmail = @"email";
NSString *const kAXBaseClassUpdatedAt = @"updated_at";
NSString *const kAXBaseClassToken = @"token";
NSString *const kAXBaseClassName = @"name";


@interface AXUser ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AXUser

@synthesize roles = _roles;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize createdAt = _createdAt;
@synthesize lastOpTime = _lastOpTime;
@synthesize lastLoginAt = _lastLoginAt;
@synthesize email = _email;
@synthesize updatedAt = _updatedAt;
@synthesize token = _token;
@synthesize name = _name;

#pragma mark - instancetype method
- (void) userSave {
    NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:self];
    [[NSUserDefaults standardUserDefaults] setObject:userData forKey:@"loginedUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void) userRemove {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"loginedUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (instancetype) userLoad {
    NSData *userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"loginedUser"];
    if (userData) {
        AXUser *userModel = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
        return userModel;
    }
    
    return nil;
}

#pragma mark - auto generate
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.roles = [self objectOrNilForKey:kAXBaseClassRoles fromDictionary:dict];
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kAXBaseClassId fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kAXBaseClassCreatedAt fromDictionary:dict];
            self.lastOpTime = [self objectOrNilForKey:kAXBaseClassLastOpTime fromDictionary:dict];
            self.lastLoginAt = [self objectOrNilForKey:kAXBaseClassLastLoginAt fromDictionary:dict];
            self.email = [self objectOrNilForKey:kAXBaseClassEmail fromDictionary:dict];
            self.updatedAt = [self objectOrNilForKey:kAXBaseClassUpdatedAt fromDictionary:dict];
            self.token = [self objectOrNilForKey:kAXBaseClassToken fromDictionary:dict];
            self.name = [self objectOrNilForKey:kAXBaseClassName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForRoles = [NSMutableArray array];
    for (NSObject *subArrayObject in self.roles) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForRoles addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForRoles addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForRoles] forKey:kAXBaseClassRoles];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kAXBaseClassId];
    [mutableDict setValue:self.createdAt forKey:kAXBaseClassCreatedAt];
    [mutableDict setValue:self.lastOpTime forKey:kAXBaseClassLastOpTime];
    [mutableDict setValue:self.lastLoginAt forKey:kAXBaseClassLastLoginAt];
    [mutableDict setValue:self.email forKey:kAXBaseClassEmail];
    [mutableDict setValue:self.updatedAt forKey:kAXBaseClassUpdatedAt];
    [mutableDict setValue:self.token forKey:kAXBaseClassToken];
    [mutableDict setValue:self.name forKey:kAXBaseClassName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.roles = [aDecoder decodeObjectForKey:kAXBaseClassRoles];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kAXBaseClassId];
    self.createdAt = [aDecoder decodeObjectForKey:kAXBaseClassCreatedAt];
    self.lastOpTime = [aDecoder decodeObjectForKey:kAXBaseClassLastOpTime];
    self.lastLoginAt = [aDecoder decodeObjectForKey:kAXBaseClassLastLoginAt];
    self.email = [aDecoder decodeObjectForKey:kAXBaseClassEmail];
    self.updatedAt = [aDecoder decodeObjectForKey:kAXBaseClassUpdatedAt];
    self.token = [aDecoder decodeObjectForKey:kAXBaseClassToken];
    self.name = [aDecoder decodeObjectForKey:kAXBaseClassName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_roles forKey:kAXBaseClassRoles];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kAXBaseClassId];
    [aCoder encodeObject:_createdAt forKey:kAXBaseClassCreatedAt];
    [aCoder encodeObject:_lastOpTime forKey:kAXBaseClassLastOpTime];
    [aCoder encodeObject:_lastLoginAt forKey:kAXBaseClassLastLoginAt];
    [aCoder encodeObject:_email forKey:kAXBaseClassEmail];
    [aCoder encodeObject:_updatedAt forKey:kAXBaseClassUpdatedAt];
    [aCoder encodeObject:_token forKey:kAXBaseClassToken];
    [aCoder encodeObject:_name forKey:kAXBaseClassName];
}

- (id)copyWithZone:(NSZone *)zone
{
    AXUser *copy = [[AXUser alloc] init];
    
    if (copy) {

        copy.roles = [self.roles copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.lastOpTime = [self.lastOpTime copyWithZone:zone];
        copy.lastLoginAt = [self.lastLoginAt copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.token = [self.token copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}


@end
