//
//  OPLDataManager.m
//  OpenPlayer
//
//  Created by laihj on 3/26/15.
//  Copyright (c) 2015 sponia. All rights reserved.
//

#import "OPLDataManager.h"

@interface OPLDataManager ()
@property (nonatomic ,strong) AFHTTPSessionManager *manager;
@end

@implementation OPLDataManager

+ (OPLDataManager *) manager {
    static OPLDataManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [OPLDataManager new];
        manager.user = [AXUser userLoad];
        
    });
    return manager;
}

- (void) setUser:(AXUser *)user{
    _user = user;
    if (user) {
        [user userSave];
    } else {
        [AXUser userRemove];
    }
}

- (void) logout {
    self.user = nil;
}

#pragma mark - API method

- (NSURLSessionDataTask *) userLoginWithEmail:(NSString *) email
                                    password:(NSString *) password
                                    response:(void (^)(AXUser *user,NSString *errorMessage))response {
    
    NSString *url = [API_URL stringByAppendingString:@"/admin/auth/login/?v=2.0"];
    
    NSDictionary *parameters = nil;
    
    
    parameters = @{
                   @"email":email,
                   @"password": password
                   };
    
    return [self requestWithMethod:HTTPRequestMethodPOST
                         URLString:url
                        parameters:parameters
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   self.user = [[AXUser alloc] initWithDictionary:result];
                                   response(_user,nil);
                               }
                           }];
    
}

-(NSURLSessionDataTask *)fetchMatches:(NSString *)date response:(void (^)(id, NSString *))response
{
    
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/matches/?c=60&v=2.0&q=start_at:%@",date]];
    
    return [self requestWithMethod:HTTPRequestMethodGET
                         URLString:url
                        parameters:nil
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];
}

- (NSURLSessionDataTask *) fetchFormations:(NSString*)matchId
                                  response:(void(^)(id matches, NSString* errorMessage))response
{
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/matches/%@/formations?v=2.0",matchId]];
    
    return [self requestWithMethod:HTTPRequestMethodGET
                         URLString:url
                        parameters:nil
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];
}

- (NSURLSessionDataTask *) saveFormations:(NSString*)matchId datas:(NSArray*)datas
                                 response:(void(^)(id matches, NSString* errorMessage))response
{
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/matches/%@/formations?v=2.0",matchId]];
    
    return [self requestWithMethod:HTTPRequestMethodPUT
                         URLString:url
                        parameters:datas
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];
}

- (NSURLSessionDataTask *) findMatchPlayers:(NSString*)text
                                   response:(void(^)(id matches, NSString* errorMessage))response

{
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/search/users/?q=%@&v=2.0",text]];
    
    return [self requestWithMethod:HTTPRequestMethodGET
                         URLString:url
                        parameters:nil
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];
}

- (NSURLSessionDataTask *) addMatchPlayer:(NSDictionary *)data teamId:(NSString *)teamId
                                 response:(void(^)(id matches, NSString* errorMessage))response
{
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/teams/%@/players?v=2.0",teamId]];
    
    return [self requestWithMethod:HTTPRequestMethodPOST
                         URLString:url
                        parameters:data
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];
}

- (NSURLSessionDataTask *) fetchTestMatchInfoWithMatchId:(NSString*)matchId
                                                response:(void(^)(id matches, NSString* errorMessage))response
{
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/matches/%@/?style=test&v=2.0",matchId]];
    
    return [self requestWithMethod:HTTPRequestMethodGET
                         URLString:url
                        parameters:nil
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];
}

- (NSURLSessionDataTask *) saveTestMatchInfoWithMatchId:(NSString*)matchId data:(NSDictionary *)data
                                               response:(void(^)(id matches, NSString* errorMessage))response
{
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/matches/%@/?style=test&v=2.0",matchId]];
    
    return [self requestWithMethod:HTTPRequestMethodPUT
                         URLString:url
                        parameters:data
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];
}

//-(NSURLSessionDataTask *)fetchEventsWith:(NSString *)matchId
//                              pageNumber:(NSInteger)pageNumber
//                                pageSize:(NSInteger)pageSize
//                                response:(void (^)(id queries, NSString *errorMessage))response
//{
//    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/matches/%@/events?c=%@&l=%@&v=2.0",matchId,[NSString stringWithFormat:@"%d",pageSize],[NSString stringWithFormat:@"%d",pageNumber]]];
//    
//    return [self requestWithMethod:HTTPRequestMethodGET
//                         URLString:url
//                        parameters:nil
//                             cache:nil
//                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
//                               NSDictionary *result = (NSDictionary *) responseObject;
//                               if (error) {
//                                   response(nil, error[@"message"]);
//                               } else {
//                                   response(result,nil);
//                               }
//                           }];
//}

-(NSURLSessionDataTask *)fetchEventsQueriesWith:(NSString *)matchId
                                       response:(void (^)(id queries, NSString *errorMessage))response
{
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/search/queries/matches/%@/events?v=2.0",matchId]];
    
    return [self requestWithMethod:HTTPRequestMethodGET
                         URLString:url
                        parameters:nil
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];
}

-(NSURLSessionDataTask *)fetchEventsWith:(NSString *)matchId
                              pageNumber:(NSInteger)pageNumber
                                pageSize:(NSInteger)pageSize
                         filterItemArray:(NSArray*)fArray
                                response:(void (^)(id events, NSString *errorMessage))response
{
    NSMutableString *filterString = [[NSMutableString alloc] initWithString:@""];
    [fArray enumerateObjectsUsingBlock:^(NSString*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if (idx == 0) {
            [filterString appendString:[NSString stringWithFormat:@"&q=%@",obj]];
        }else{
            [filterString appendString:[NSString stringWithFormat:@"+AND+%@",obj]];
        }
    }];
    
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/matches/%@/events?c=%@&l=%@%@&v=2.0",matchId,[NSString stringWithFormat:@"%ld",pageSize],[NSString stringWithFormat:@"%ld",pageNumber],filterString]];
    
    return [self requestWithMethod:HTTPRequestMethodGET
                         URLString:url
                        parameters:nil
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];
}

-(NSURLSessionDataTask *)addEventWithMatchId:(NSString *)matchId data:(id)data response:(void (^)(id, NSString *))response
{
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/matches/%@/events?v=2.0",matchId]];
    
    return [self requestWithMethod:HTTPRequestMethodPOST
                         URLString:url
                        parameters:data
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];

}

-(NSURLSessionDataTask *)saveEventWithEventId:(NSString *)eventId data:(NSDictionary *)data response:(void (^)(id event, NSString *errorMessage))response
{
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/events/%@/?v=2.0",eventId]];
    
    return [self requestWithMethod:HTTPRequestMethodPUT
                         URLString:url
                        parameters:data
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];
}

-(NSURLSessionDataTask *)deleteEventWithEventId:(NSString *)eventId response:(void (^)(id, NSString *))response
{
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/events/%@/?v=2.0",eventId]];
    
    return [self requestWithMethod:HTTPRequestMethodDelete
                         URLString:url
                        parameters:nil
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];
 
}

-(NSURLSessionDataTask *)addTestMatchWithData:(id)data response:(void (^)(id, NSString *))response
{
    NSString *url = [API_URL stringByAppendingString:@"/admin/matches/?style=test&v=2.0"];
    
    return [self requestWithMethod:HTTPRequestMethodPOST
                         URLString:url
                        parameters:data
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];

}

-(NSURLSessionDataTask *)saveStatisticiansWithMatchId:(NSString *)matchId data:(id)data response:(void (^)(id, NSString *))response
{
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/matches/%@/statisticians/?v=2.0",matchId]];
    
    return [self requestWithMethod:HTTPRequestMethodPOST
                         URLString:url
                        parameters:data
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];
    
}

-(NSURLSessionDataTask *)searchStatisticiansWithKey:(NSString *)key response:(void (^)(id, NSString *))response
{
    NSString *url = [API_URL stringByAppendingString:[NSString stringWithFormat:@"/admin/search/users/?q=%@&type=5&v=2.0",key]];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [self requestWithMethod:HTTPRequestMethodGET
                         URLString:url
                        parameters:nil
                             cache:nil
                           success:^(NSURLSessionDataTask *task, id responseObject, NSDictionary *error) {
                               NSDictionary *result = (NSDictionary *) responseObject;
                               if (error) {
                                   response(nil, error[@"message"]);
                               } else {
                                   response(result,nil);
                               }
                           }];

}

#pragma mark - manager method
- (NSURLSessionDataTask *)requestWithMethod:(HTTPRequestMethod)method
                                  URLString:(NSString *)URLString
                                 parameters:(id)parameters
                                      cache:(void (^)(id cacheObject))cache
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject,NSDictionary *error))response {
    
    NSURLSessionDataTask *task = nil;
    // Create HTTPSession
    
    AFNetworkReachabilityStatus status = [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus;
    if (status == AFNetworkReachabilityStatusNotReachable) {
        response(task,nil,@{@"message":@"无网络连接"});
        AlertViewShowWith(@"虫总退钱!!")
        return task;
    }
    
    if (!_manager) {
        self.manager = [AFHTTPSessionManager manager];
        [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        _manager.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];
        [_manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    }
    
    if (self.user) {
        [_manager.requestSerializer setValue:_user.token forHTTPHeaderField:@"token"];
    }
    
    if (method == HTTPRequestMethodGET) {
        task = [_manager GET:URLString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            NSDictionary *result = (NSDictionary *)responseObject;
            [self successWith:task result:result response:response];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self failedWith:task error:error response:response];
        }];
    }
    if (method == HTTPRequestMethodPOST) {
        task = [_manager POST:URLString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            NSDictionary *result = (NSDictionary *)responseObject;
            [self successWith:task result:result response:response];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self failedWith:task error:error response:response];
        }];
    }
    if (method == HTTPRequestMethodPUT) {
        task = [_manager PUT:URLString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            NSDictionary *result = (NSDictionary *)responseObject;
            [self successWith:task result:result response:response];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self failedWith:task error:error response:response];
        }];
    }
    if (method == HTTPRequestMethodDelete) {
        task = [_manager DELETE:URLString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            NSDictionary *result = (NSDictionary *)responseObject;
            [self successWith:task result:result response:response];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self failedWith:task error:error response:response];
        }];
    }
    return task;
}

- (void) successWith:(NSURLSessionDataTask *) task
              result:(NSDictionary *) result
            response:(void (^)(NSURLSessionDataTask *task, id responseObject,NSDictionary *error))response {
    if ([result isKindOfClass:[NSDictionary class]]) {
        if (((NSString *)result[@"message"]).length > 0 && [result[@"error_code"] integerValue] > 0) {
            response(task, nil,result);
        } else {
            response(task, result,nil);
        }
    } else {
        response(task, result,nil);
    }
}

- (void) failedWith:(NSURLSessionDataTask *) task
              error:(NSError *) error
           response:(void (^)(NSURLSessionDataTask *task, id responseObject,NSDictionary *error))response {
    if (!(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]) {
        if (error.code == NSURLErrorTimedOut) {
            response(task, nil, @{@"message":@"加载失败，请检查网络或稍后再试"});
        } else {
            response(task, nil,@{@"message":@"请求失败"});
        }
        return;
    }
    NSDictionary *oplError = [self errorWithErrorResponse:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]];
    if (oplError) {
        response(task, nil,oplError);
    } else {
        response(task, nil,@{@"message":@"请求失败"});
    }
}

- (NSDictionary *) errorWithErrorResponse:(NSData *) errorResponse {
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:errorResponse
                                                         options:kNilOptions
                                                           error:&error];
    if (error) {
        return nil;
    }
    return json;
}
@end
