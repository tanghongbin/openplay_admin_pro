//
//  NSDate+OPBUtil.m
//  OpenPlay
//
//  Created by Kelvin Tong on 15/12/18.
//  Copyright © 2015年 laihj. All rights reserved.
//

#import "NSDate+OPBUtil.h"

@implementation NSDate (OPBUtil)
+(NSString *)intervalFromLastDateStr: (NSString *) dateString1 toTheDateStr:(NSString *) dateString2
{
    NSArray *timeArray1=[dateString1 componentsSeparatedByString:@"."];
    dateString1=[timeArray1 objectAtIndex:0];
    
    
    NSArray *timeArray2=[dateString2 componentsSeparatedByString:@"."];
    dateString2=[timeArray2 objectAtIndex:0];
    
    NSDateFormatter *date=[[NSDateFormatter alloc] init];
    [date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSDate *d1=[date dateFromString:dateString1];
    
    NSTimeInterval late1=[d1 timeIntervalSince1970]*1;
    
    
    
    NSDate *d2=[date dateFromString:dateString2];
    
    NSTimeInterval late2=[d2 timeIntervalSince1970]*1;
    
    
    
    NSTimeInterval cha=late2-late1;
    NSString *timeString=@"";
    NSString *house=@"";
    NSString *min=@"";
    NSString *sen=@"";
    
    sen = [NSString stringWithFormat:@"%d", (int)cha%60];
    // min = [min substringToIndex:min.length-7];
    // 秒
    sen=[NSString stringWithFormat:@"%@", sen];
    
    
    
    min = [NSString stringWithFormat:@"%d", (int)cha/60%60];
    // min = [min substringToIndex:min.length-7];
    // 分
    min=[NSString stringWithFormat:@"%@", min];
    
    
    // 小时
    house = [NSString stringWithFormat:@"%d", (int)cha/3600];
    // house = [house substringToIndex:house.length-7];
    house=[NSString stringWithFormat:@"%@", house];
    
    
    timeString=[NSString stringWithFormat:@"%@:%@:%@",house,min,sen];
    
    
    return timeString;
}

+(NSString *)intervalFromLastDate: (NSDate *) date1 toTheDate:(NSDate*) date2
{
    NSTimeInterval late1=[date1 timeIntervalSince1970];
    NSTimeInterval late2=[date2 timeIntervalSince1970];
    
    NSTimeInterval cha=late2-late1;
    NSString *timeString=@"";
    NSString *house=@"";
    NSString *min=@"";
    NSString *sen=@"";
    
    sen = [NSString stringWithFormat:@"%d", (int)cha%60];
    // min = [min substringToIndex:min.length-7];
    // 秒
    sen=[NSString stringWithFormat:@"%@", sen];
    
    
    
    min = [NSString stringWithFormat:@"%d", (int)cha/60%60];
    // min = [min substringToIndex:min.length-7];
    // 分
    min=[NSString stringWithFormat:@"%@", min];
    
    
    // 小时
    house = [NSString stringWithFormat:@"%d", (int)cha/3600];
    // house = [house substringToIndex:house.length-7];
    house=[NSString stringWithFormat:@"%@", house];
    
    
    timeString=[NSString stringWithFormat:@"%@:%@:%@",house,min,sen];
    return timeString;
}

+(NSTimeInterval)timeIntervalFromDate: (NSDate *) before toDate:(NSDate*) after
{
    NSTimeInterval late1=[before timeIntervalSince1970];
    NSTimeInterval late2=[after timeIntervalSince1970];
    
    NSTimeInterval cha=late2-late1;
    return (int)cha;
}

+ (NSDate *) dateFromLocalString:(NSString *) dateString {
    
    NSDateFormatter *_dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    [_dateFormatter setTimeZone:timeZone];
    NSDate *utcDate = [_dateFormatter dateFromString:dateString];
    return utcDate;
}

+(NSDate *)utcTimeToLocaleDate:(NSTimeInterval)utcTimeMilliSeconds //utc毫秒数转化为本地日期
{
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(utcTimeMilliSeconds/1000)];
    
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    
    NSInteger interval = [zone secondsFromGMTForDate: date];
    
    NSDate *localeDate = [date dateByAddingTimeInterval: interval];
    
    return localeDate;
    
}

+(NSTimeInterval)utcDateToTimeMilliSeconds:(NSDate *)date{
    NSTimeInterval time = [date timeIntervalSince1970]*1000;
    
    return time;
    
}

//+(NSTimeInterval)utcTimeMilliSecondsNow{ //返回当前时间的UTC值
//    NSDate *date = [NSDate date];
//    
//    NSTimeInterval time = [date timeIntervalSince1970]*1000;
//    
//    return time;
//    
//}

+(NSTimeInterval)utcTimeMilliSecondsNow{ //返回当前时间的UTC值（时分秒不计）
    NSString *todayStr = [NSDate dateToDateString:[NSDate date]];
    NSDate *date = [NSDate dateStringToDate:todayStr];
    NSTimeInterval time = [date timeIntervalSince1970]*1000;
    return time;
}
+(NSTimeInterval)dateStringToUtcTime:(NSString *)dateString{
    NSDate *date = [NSDate dateStringToDate:dateString];
    
    NSTimeInterval utcTime = [NSDate utcDateToTimeMilliSeconds:date];
    
    return utcTime;
    
}

+(NSString *)dateToDateString:(NSDate *)date{ //日期转化为字符串
    //实例化一个NSDateFormatter对象
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //设定时间格式,这里可以设置成自己需要的格式
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    
    //设置GMT市区，保证转化后日期字符串与日期一致
    
    NSTimeZone *timezone = [[NSTimeZone alloc] initWithName:@"GMT"];
    
    [dateFormatter setTimeZone:timezone];
    
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
    
}

+(NSDate *)dateStringToDate:(NSString *)dateString{ //字符串转化为日期
    //实例化一个NSDateFormatter对象
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //设定时间格式,这里可以设置成自己需要的格式
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //设置GMT市区，保证转化后日期字符串与日期一致
    
    NSTimeZone *timezone = [[NSTimeZone alloc] initWithName:@"GMT"];
    
    [dateFormatter setTimeZone:timezone];
    
    //转化date为日期字符串
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    return date;
    
}

+(NSString *)utcTimeToDataString:(NSNumber *)time{
    NSDate *date = [NSDate utcTimeToLocaleDate:[time longLongValue]];
    
    return [NSDate dateToDateString:date];
    
}

+(NSString *)timeStringFromDate:(NSDate *)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *destDateString = [dateFormatter stringFromDate:date];
    
    return destDateString;
    
}

+(NSString *)timeStringFromDateWithoutSecond:(NSDate *)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSString *destDateString = [dateFormatter stringFromDate:date];
    
    return destDateString;
    
}
@end
