//
//  AXEventsPageNumberView.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/13.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AXEventsPageNumberView;
@protocol AXEventsPageNumberViewDelegate <NSObject>

@required
-(void)pageNumberView:(AXEventsPageNumberView*)pageNumberView didSelectedPage:(NSInteger)page;

@end

@interface AXEventsPageNumberView : UIView
@property (nonatomic, assign)id<AXEventsPageNumberViewDelegate>delegate;
@property (nonatomic, assign)NSInteger total;
@property (nonatomic, assign)NSInteger selectedPage;

-(instancetype)initWithFrame:(CGRect)frame onView:(UIView *)onView;
@end
