//
//  UIView+Ext.h
//  uptownfunk
//
//  Created by yellow on 16/4/19.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Ext)

- (void)showToast:(NSString *)text;
- (void)showToast:(NSString *)text delay:(NSInteger)delay;

- (void)showLoadingView:(NSString *)text;
- (void)showLoadingView;
- (void)hiddenLoadingView;

@end
