//
//  AXLoginViewController.m
//  uptownfunk
//
//  Created by yellow on 16/4/19.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXLoginViewController.h"
#import "NSString+Ext.h"
#import "UIView+Ext.h"
#import "OPLDataManager.h"
#import "AXMatchFormatViewController.h"
#import "AXUser.h"


@interface AXLoginViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotButton;

@property (nonnull, copy)   NSString   *email;
@property (nonnull, copy)   NSString   *password;

@property (nonatomic, strong)IBOutlet UILabel *versionLabel;

@end

@implementation AXLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([AXUser userLoad]) {
        [self performSegueWithIdentifier:@"kLogin" sender:nil];
    }
    
    self.loginButton.layer.cornerRadius = 5;
    self.loginButton.layer.masksToBounds = YES;
    
#ifdef DEBUG
    [_versionLabel setText:[NSString stringWithFormat:@"version %@ build d%@",[[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"],[[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"]]];
#else
    [_versionLabel setText:[NSString stringWithFormat:@"version %@ build %@",[[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"],[[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"]]];
#endif
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self registerNotification];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self unregisterNotification];
}
#pragma mark -- Notification
- (void)registerNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark -- Keyboard notification
- (void)keyboardWillShow:(NSNotification *)notification
{
    [UIView animateWithDuration:.3 animations:^{
        self.view.y = - 60;
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:.3 animations:^{
        self.view.y = 0;
    }];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.emailTextField) {
        self.email = textField.text;
    } else if (textField == self.passwordTextField){
        self.password = textField.text;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(nullable id)sender
{
    [self.view endEditing:YES];
    if ([identifier isEqualToString:@"kLogin"]) {
        
        if (![self verifyEmail:self.email]  || [NSString isEmpty:self.password]) {
            [self.view showToast:@"邮箱或密码不能为空！"];
            return NO;
        }
        
        [self.view showLoadingView:@"登录中..."];
        
        [[OPLDataManager manager] userLoginWithEmail:self.email password:self.password response:^(AXUser *user, NSString *errorMessage) {
            [self.view hiddenLoadingView];
            
            if (!user) {
                [self.view showToast:errorMessage];
                return ;
            }
            
            [self performSegueWithIdentifier:identifier sender:sender];
        }];
        return NO;
    }
    
    return YES;
}


- (BOOL)verifyEmail:(NSString*) email{
    if ([NSString isEmpty:email]) {
        return NO;
    }
    NSString *integerRegex = @"\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",integerRegex];
    return [phoneTest evaluateWithObject:email];
}

@end
