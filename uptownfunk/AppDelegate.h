//
//  AppDelegate.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/16.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

