//
//  AXEventsViewController.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/26.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXEventsViewController.h"
#import "AXEventsTableViewCell.h"
#import "OPLDataManager.h"
#import "AXEvents_PerPage.h"
#import "AXEvent.h"
#import "AXEventsFilterView.h"
#import "AXQueries.h"
#import "AXEventEditViewController.h"
#import "AXEventsPageNumberView.h"
#import "AXStatistician.h"
#import "OPFMatchStatisticians.h"
#import "AXTeam.h"

@interface AXEventsViewController ()<UITableViewDelegate,UITableViewDataSource,AXEventsFilterViewDelegate,AXEventsPageNumberViewDelegate>
{
    NSArray *currentFilterArray;
}
@property (nonatomic, strong)UITableView *eventsTableView;
@property (nonatomic, strong)UILabel *totalLabel;
@property (nonatomic, strong)UIView *tableViewFooterView;
@property (nonatomic, strong)AXEvents_PerPage *events_PerPage;
@property (strong, nonatomic)UIBarButtonItem *addBarButton;
@property (nonatomic, strong)AXEventsFilterView *eventsFilterView;
@property (nonatomic, strong)AXQueries *eventsQueries;
@property (nonatomic, assign)BOOL hasUpdated;
@property (nonatomic, strong)AXEventsPageNumberView *pageNumberView;
@end

@implementation AXEventsViewController

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdatedEventNotificaitonName object:nil];
}

-(void)setMatch:(AXMatch *)match
{
    _match = match;
    [self requestData:^{
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _addBarButton = [[UIBarButtonItem alloc]initWithTitle:@"添加" style:UIBarButtonItemStyleDone target:self action:@selector(addBarButtonClicked:)];
    [_addBarButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0],NSForegroundColorAttributeName:[UIColor lightThemeColor]} forState:UIControlStateNormal];
    [_addBarButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0],NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateDisabled];
    self.parentViewController.navigationItem.rightBarButtonItem = _addBarButton;
    [self.view setBackgroundColor:[UIColor defaultTextColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedEvents:) name:kUpdatedEventNotificaitonName object:nil];
}

-(void)updatedEvents:(NSNotification*)notification
{
    self.hasUpdated = YES;
}

-(void)setHasUpdated:(BOOL)hasUpdated
{
    _hasUpdated = hasUpdated;
    _hasUpdated?[self requestData:^{
        
    }]:nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.parentViewController.navigationItem setRightBarButtonItem:_addBarButton animated:NO];
    
    [self.view addSubview:self.eventsTableView];
    [self.view addSubview:self.eventsFilterView];
    [self.view bringSubviewToFront:self.eventsFilterView];
    [self.view addSubview:self.pageNumberView];
    [self.view bringSubviewToFront:self.pageNumberView];
}

-(void)requestData:(void(^)())callBack
{
    [self.view showLoadingView];
    [[OPLDataManager manager] fetchEventsWith:self.match.listIdentifier pageNumber:1 pageSize:60 filterItemArray:currentFilterArray response:^(id events, NSString *errorMessage) {
        [self.view hiddenLoadingView];
        callBack();
        if (!errorMessage) {
            self.events_PerPage = [[AXEvents_PerPage alloc] initWithDictionary:events];
            [_events_PerPage.list enumerateObjectsUsingBlock:^(AXEvent*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([AXStaticDataHelper matchEvent:[NSString stringWithFormat:@"%0.0f",obj.code]]) {
                    [obj setPlayer:nil];
                }
            }];
            
            [self.eventsTableView reloadData];
            [self.totalLabel setText:[NSString stringWithFormat:@"总数：%.0f",_events_PerPage.total]];
            [self.pageNumberView setTotal:_events_PerPage.total];
            [self.pageNumberView setSelectedPage:1];
            
            [self.view showLoadingView];
            [[OPLDataManager manager] fetchEventsQueriesWith:self.match.listIdentifier response:^(id queries, NSString *errorMessage) {
                [self.view hiddenLoadingView];
                if (errorMessage) {
                    AlertViewShowWith(errorMessage);
                }else{
                    self.eventsQueries = [[AXQueries alloc]initWithDictionary:queries];
                    [self.eventsFilterView setQueries:_eventsQueries];
                }
            }];
            
        }else{
            AlertViewShowWith(errorMessage);
        }
        
        
    }];
}

-(void)pullRefreshAction:(MJRefreshHeader*)header
{
    [self requestData:^{
        [header endRefreshing];
    }];
}

-(void)addBarButtonClicked:(id)sender
{
    AXEvent *defaultEvent = [AXEvent modelObjectWithDictionary:@{@"code":@100,@"client_started_at":[NSDate dateToDateString:[NSDate date]]}];
    [defaultEvent setMatch:_match];
    [defaultEvent setTeam:_match.teamA];
    
    [_match.matchStatisticians enumerateObjectsUsingBlock:^(OPFMatchStatisticians*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.teamId isEqualToString:[_match.teamA teamIdentifier]]) {
            [defaultEvent setStatistician:obj.statistician];
            *stop = YES;
        }
    }];
    
    [self performSegueWithIdentifier:@"AXEventEditViewController" sender:defaultEvent];
}

#pragma mark - UITableViewDelegate && UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _events_PerPage.list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXEventsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AXEventsTableViewCell class]) forIndexPath:indexPath];
    [cell setEvent:_events_PerPage.list[indexPath.row]];
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 30)];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width - 0, 30)];
    [backgroundView setBackgroundColor:[[UIColor defaultTextColor] colorWithAlphaComponent:1.0]];
    [backgroundView setClipsToBounds:YES];
    [headerView addSubview:backgroundView];

    UILabel *teamLabel = [self headerLabelWithFrame:CGRectMake(10, 0, iPad?140:60, backgroundView.height) Title:@"球队"];
    [backgroundView addSubview:teamLabel];
    
    UILabel *playerLabel = [self headerLabelWithFrame:CGRectMake(teamLabel.x+teamLabel.width+10, 0, iPad?100:65, backgroundView.height) Title:@"球员"];
    [backgroundView addSubview:playerLabel];
    
    UILabel *eventLabel = [self headerLabelWithFrame:CGRectMake(playerLabel.x+playerLabel.width+10, 0, 80, backgroundView.height) Title:@"事件"];
    [backgroundView addSubview:eventLabel];
    
    UILabel *timeLabel = [self headerLabelWithFrame:CGRectMake(eventLabel.x+eventLabel.width+5, 0, 130, backgroundView.height) Title:@"时间"];
    [backgroundView addSubview:timeLabel];
    
    UIView* _seperateView = [[UIView alloc]initWithFrame:CGRectMake(0, backgroundView.height - 2, backgroundView.width, 2)];
    [_seperateView setBackgroundColor:UIColorWithRGB(0x4a4a4a)];
    [backgroundView addSubview:_seperateView];
    
    return headerView;
}

-(UILabel*)headerLabelWithFrame:(CGRect)frame Title:(NSString*)title
{
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:frame];
    [headerLabel setTextColor:[UIColor defaultBackGroundColor]];
    [headerLabel setTextAlignment:NSTextAlignmentLeft];
    [headerLabel setFont:[UIFont systemFontOfSize:12]];
    [headerLabel setText:title];
    return headerLabel;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"AXEventEditViewController" sender:_events_PerPage.list[indexPath.row]];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    AXEventEditViewController *vc = [segue destinationViewController];
    [vc setEvent:sender];
    [vc setMatch:_match];
}

#pragma mark - Setter
-(void)setEventsQueries:(AXQueries *)eventsQueries
{
    _eventsQueries = eventsQueries;
}

#pragma mark - AXEventsFilterViewDelegate
-(void)filterView:(AXEventsFilterView *)filterView didSelectedWithFilterItemArray:(NSArray *)fArray
{
    [self.view showLoadingView];
    currentFilterArray= [NSArray arrayWithArray:fArray];
    [[OPLDataManager manager] fetchEventsWith:_match.listIdentifier pageNumber:1 pageSize:60 filterItemArray:currentFilterArray response:^(id events, NSString *errorMessage) {
        [self.view hiddenLoadingView];
        if (!errorMessage) {
            self.events_PerPage = [[AXEvents_PerPage alloc] initWithDictionary:events];
            [_events_PerPage.list enumerateObjectsUsingBlock:^(AXEvent*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([AXStaticDataHelper matchEvent:[NSString stringWithFormat:@"%0.0f",obj.code]]) {
                    [obj setPlayer:nil];
                }
            }];
            [self.eventsTableView reloadData];
            [self.totalLabel setText:[NSString stringWithFormat:@"总数：%.0f",_events_PerPage.total]];
            [self.pageNumberView setTotal:_events_PerPage.total];
            [self.pageNumberView setSelectedPage:1];
        }else{
            AlertViewShowWith(errorMessage);
        }
    }];

}

#pragma mark - AXEventsPageNumberViewDelegate
-(void)pageNumberView:(AXEventsPageNumberView *)pageNumberView didSelectedPage:(NSInteger)page
{
    [self.view showLoadingView];
    [[OPLDataManager manager] fetchEventsWith:_match.listIdentifier pageNumber:page pageSize:60 filterItemArray:currentFilterArray response:^(id events, NSString *errorMessage) {
        [self.view hiddenLoadingView];
        if (!errorMessage) {
            self.events_PerPage = [[AXEvents_PerPage alloc] initWithDictionary:events];
            [_events_PerPage.list enumerateObjectsUsingBlock:^(AXEvent*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([AXStaticDataHelper matchEvent:[NSString stringWithFormat:@"%0.0f",obj.code]]) {
                    [obj setPlayer:nil];
                }
            }];
            [self.eventsTableView reloadData];
            [self.totalLabel setText:[NSString stringWithFormat:@"总数：%.0f",_events_PerPage.total]];
        }else{
            AlertViewShowWith(errorMessage);
        }
    }];

}

#pragma mark - Getter
-(UITableView *)eventsTableView
{
    if (!_eventsTableView) {
        _eventsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, self.view.width, iPad?self.view.height - 104:self.view.height - 44) style:UITableViewStylePlain];
        [_eventsTableView setBackgroundColor:[UIColor clearColor]];
        [_eventsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _eventsTableView.delegate = self;
        _eventsTableView.dataSource = self;
        [_eventsTableView registerClass:[AXEventsTableViewCell class] forCellReuseIdentifier:@"AXEventsTableViewCell"];
        [_eventsTableView setTableFooterView:self.tableViewFooterView];
        [_eventsTableView setContentInsetBottom:40];
        [_eventsTableView setMj_header:[MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullRefreshAction:)]];
    }
    return _eventsTableView;
}

-(UIView*)tableViewFooterView
{
    if (!_tableViewFooterView) {
        _tableViewFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 20)];
        [_tableViewFooterView addSubview:self.totalLabel];
    }
    return _tableViewFooterView;
}

-(UILabel *)totalLabel
{
    if (!_totalLabel) {
        _totalLabel = [self headerLabelWithFrame:CGRectMake(10, 0, 100, _tableViewFooterView.height) Title:@""];
        [_totalLabel setTextColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.7]];
    }
    return _totalLabel;
}

-(AXEventsFilterView *)eventsFilterView
{
    if (!_eventsFilterView) {
        _eventsFilterView = [[AXEventsFilterView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 40)];
        _eventsFilterView.delegate = self;
    }
    return _eventsFilterView;
}

-(AXEventsPageNumberView *)pageNumberView
{
    if (!_pageNumberView) {
        _pageNumberView = [[AXEventsPageNumberView alloc]initWithFrame:CGRectMake(0, self.view.height - 40, SCREEN_WIDTH, 40) onView:self.view];
        _pageNumberView.delegate = self;
    }
    return _pageNumberView;
}
@end
