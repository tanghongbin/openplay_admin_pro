//
//  KTAppSingleton.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "KTAppSingleton.h"

@implementation KTAppSingleton
+ (instancetype)sharedAppSingleton{
    static KTAppSingleton *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[KTAppSingleton alloc] init];
        _sharedInstance.currentMatchFormations = [NSArray new];
    });
    return _sharedInstance;
}
@end
