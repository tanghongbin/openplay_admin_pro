//
//  NSString+Ext.h
//  uptownfunk
//
//  Created by yellow on 16/4/19.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Ext)

+ (BOOL)isEmpty:(NSString *)text;

@end
