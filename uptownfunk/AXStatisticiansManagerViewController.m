//
//  AXStatisticiansManagerViewController.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/17.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXStatisticiansManagerViewController.h"
#import "AXStatisticianTableViewCell.h"
#import "AXStatisticianSectionHeaderView.h"
#import "AXTeam.h"
#import "OPFMatchStatisticians.h"
#import "AXStatistician.h"
#import "AXStatisticianSearchView.h"

@interface AXStatisticiansManagerViewController ()<UITableViewDelegate,UITableViewDataSource,AXStatisticianTableViewCellDelegate,AXStatisticianSectionHeaderViewDelegate,AXStatisticianSearchViewDelegate>
@property (strong, nonatomic) IBOutlet UIButton *masterButton;
@property (weak, nonatomic) IBOutlet UILabel *masterLabel;
@property (strong, nonatomic) IBOutlet UIButton *guestButton;
@property (weak, nonatomic) IBOutlet UILabel *guestLabel;

@property (strong, nonatomic) IBOutlet UITableView *contentTableView;
@property (nonatomic, assign)BOOL isGuest;

@property (nonatomic, strong)AXStatisticianSectionHeaderView *statisticianHeaderView;
@property (nonatomic, strong)AXStatisticianSectionHeaderView *statisticianTestHeaderView;

@property (nonatomic, strong)NSMutableArray<OPFMatchStatisticians*> *allArray;
@property (nonatomic, strong)NSMutableArray<OPFMatchStatisticians*> *mainHomeArray;
@property (nonatomic, strong)NSMutableArray<OPFMatchStatisticians*> *testHomeArray;
@property (nonatomic, strong)NSMutableArray<OPFMatchStatisticians*> *mainGuestArray;
@property (nonatomic, strong)NSMutableArray<OPFMatchStatisticians*> *testGuestArray;

@property (strong, nonatomic) UIBarButtonItem *saveBarButton;
@property (nonatomic, strong)AXStatisticianSearchView *searchView;
@end

@implementation AXStatisticiansManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor defaultTextColor]];
    _saveBarButton = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(saveBarButtonClicked:)];
    [_saveBarButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0],NSForegroundColorAttributeName:[UIColor lightThemeColor]} forState:UIControlStateNormal];
    [_saveBarButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0],NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateDisabled];
    self.parentViewController.navigationItem.rightBarButtonItem = _saveBarButton;
    [_contentTableView setBackgroundColor:[UIColor clearColor]];
    [_contentTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_contentTableView registerClass:[AXStatisticianTableViewCell class] forCellReuseIdentifier:@"AXStatisticianTableViewCell"];
    [_contentTableView setContentInsetTop:20];
    
    [self viewChangeWithGuest:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.parentViewController.navigationItem setRightBarButtonItem:_saveBarButton animated:NO];
}

- (IBAction)masterButtonClicked:(UIButton *)sender {
    if (_masterButton.isSelected) {
        return;
    }
    [_guestButton setSelected:NO];
    [_masterButton setSelected:YES];
    [self viewChangeWithGuest:NO];
    
}

- (IBAction)guestButtonClicked:(UIButton *)sender {
    if (_guestButton.isSelected) {
        return;
    }
    [_masterButton setSelected:NO];
    [_guestButton setSelected:YES];
    [self viewChangeWithGuest:YES];
    
}

- (void)viewChangeWithGuest:(BOOL)isGuest
{
    self.isGuest = isGuest;
}

-(void)setIsGuest:(BOOL)isGuest
{
    _isGuest = isGuest;
    if(isGuest) {
        self.masterLabel.textColor = [UIColor lightGrayColor];
        [self.masterLabel setBackgroundColor:UIColorWithRGB(0x7f7f7f)];
        self.guestLabel.textColor = [UIColor whiteColor];
        [self.guestLabel setBackgroundColor:UIColorWithRGB(0x3a3a3a)];
        
    } else {
        self.guestLabel.textColor = [UIColor lightGrayColor];
        [self.guestLabel setBackgroundColor:UIColorWithRGB(0x7f7f7f)];
        self.masterLabel.textColor = [UIColor whiteColor];
        [self.masterLabel setBackgroundColor:UIColorWithRGB(0x3a3a3a)];
        
    }
    
    [_contentTableView reloadData];
}

-(void)setMatch:(AXMatch *)match
{
    _match = match;
    self.masterLabel.text = [NSString stringWithFormat:@"主队\n%@",_match.teamA.name];
    self.guestLabel.text = [NSString stringWithFormat:@"客队\n%@",_match.teamB.name];
    
    self.mainHomeArray = [NSMutableArray new];
    self.testHomeArray = [NSMutableArray new];
    self.mainGuestArray = [NSMutableArray new];
    self.testGuestArray = [NSMutableArray new];
    
    self.allArray = [NSMutableArray arrayWithArray:_match.matchStatisticians];
    
}

-(void)setAllArray:(NSMutableArray<OPFMatchStatisticians *> *)allArray
{
    _allArray = allArray;
    [_allArray enumerateObjectsUsingBlock:^(OPFMatchStatisticians*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        OPFMatchStatisticians *object = obj;
        if ([object.teamId isEqualToString:_match.teamA.teamIdentifier]) {
            if ([object.role isEqualToString:@"statistician"]) {
                [_mainHomeArray addObject:object];
            }else{
                [_testHomeArray addObject:object
                 ];
            }
        }else{
            if ([object.role isEqualToString:@"statistician"]) {
                [_mainGuestArray addObject:object];
            }else{
                [_testGuestArray addObject:object];
            }
        }
    }];
    
    [_contentTableView reloadData];

}

- (void)saveBarButtonClicked:(id)sender {
        [self.view showLoadingView:@"保存中..."];
        
        NSMutableArray *sArray = [NSMutableArray new];
        
        [_allArray enumerateObjectsUsingBlock:^(OPFMatchStatisticians * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [sArray addObject:[obj dictionaryRepresentation]];
        }];
        
        NSDictionary *dataDic = @{@"match_statisticians":sArray,
                                  @"team_ids":@[_match.teamA.teamIdentifier,_match.teamB.teamIdentifier],
                                  @"roles":@[@"statistician",@"statistician_to_be",@"statistician_test"]};
        
        [[OPLDataManager manager]saveStatisticiansWithMatchId:self.match.listIdentifier data:dataDic response:^(id data, NSString *errorMessage) {
            [self.view hiddenLoadingView];
            if (errorMessage) {
                [self.view showToast:errorMessage];
            }else{
                [self.view showToast:@"保存成功"];
                [[NSNotificationCenter defaultCenter] postNotificationName:kUpdatedEventNotificaitonName object:nil];
            }
        }];
}

#pragma mark - UITableViewDelegate & UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        NSInteger count = _isGuest?_mainGuestArray.count:_mainHomeArray.count;
        if (count==0) {
            [_statisticianHeaderView.addButton setHidden:NO];
        }else{
            [_statisticianHeaderView.addButton setHidden:YES];
        }
        return count;
    }else{
        return _isGuest?_testGuestArray.count:_testHomeArray.count;;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return self.statisticianHeaderView;
    }
    return self.statisticianTestHeaderView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXStatisticianTableViewCell *c = [tableView dequeueReusableCellWithIdentifier:@"AXStatisticianTableViewCell" forIndexPath:indexPath];
    c.delegate = self;
    if (indexPath.section == 0) {
        OPFMatchStatisticians *sta = _isGuest?_mainGuestArray[indexPath.row]:_mainHomeArray[indexPath.row];
        [c setMatchStatistician:sta];
    }else{
        OPFMatchStatisticians *sta = _isGuest?_testGuestArray[indexPath.row]:_testHomeArray[indexPath.row];
        [c setMatchStatistician:sta];
    }
    
    return c;
}

#pragma mark - AXStatisticianTableViewCellDelegate
-(void)statisticianTableViewCell:(AXStatisticianTableViewCell *)statisticianTableViewCell didDelete:(OPFMatchStatisticians *)sta
{
    [_allArray removeObject:sta];
    [_mainHomeArray containsObject:sta]?[_mainHomeArray removeObject:sta]:nil;
    [_mainGuestArray containsObject:sta]?[_mainGuestArray removeObject:sta]:nil;
    [_testHomeArray containsObject:sta]?[_testHomeArray removeObject:sta]:nil;
    [_testGuestArray containsObject:sta]?[_testGuestArray removeObject:sta]:nil;
    
    [_contentTableView reloadData];
}

#pragma mark - AXStatisticianSectionHeaderViewDelegate
-(void)addStatistician:(AXStatisticianSectionHeaderView *)statisticianSectionHeaderView
{
    if ([statisticianSectionHeaderView isEqual:_statisticianHeaderView]) {
        [self.searchView setIsMain:YES];
    }else
    {
        [self.searchView setIsMain:NO];
    }
    [self.searchView setIsGuest:_isGuest];
    [self.searchView show];
}

#pragma mark - searchView delegate

-(void)statisticianSearchView:(AXStatisticianSearchView *)statisticianSearchView didSelectedStatistician:(AXStatistician *)sta
{
    if (!statisticianSearchView.isGuest) {
        if (statisticianSearchView.isMain) {
            OPFMatchStatisticians *opfSta = [OPFMatchStatisticians modelObjectWithDictionary:@{@"team_id":_match.teamA.teamIdentifier,@"role":@"statistician"}];
            [opfSta setStatistician:[AXStatistician modelObjectWithDictionary:@{@"id":sta.statisticianIdentifier,@"name":sta.name}]];
            [_allArray addObject:opfSta];
            [_mainHomeArray addObject:opfSta];
        }else{
            OPFMatchStatisticians *opfSta = [OPFMatchStatisticians modelObjectWithDictionary:@{@"team_id":_match.teamA.teamIdentifier,@"role":@"statistician_test"}];
            [opfSta setStatistician:[AXStatistician modelObjectWithDictionary:@{@"id":sta.statisticianIdentifier,@"name":sta.name}]];
            [_allArray addObject:opfSta];
            [_testHomeArray addObject:opfSta];
        }
    }else{
        if (statisticianSearchView.isMain) {
            OPFMatchStatisticians *opfSta = [OPFMatchStatisticians modelObjectWithDictionary:@{@"team_id":_match.teamB.teamIdentifier,@"role":@"statistician"}];
            [opfSta setStatistician:[AXStatistician modelObjectWithDictionary:@{@"id":sta.statisticianIdentifier,@"name":sta.name}]];
            [_allArray addObject:opfSta];
            [_mainGuestArray addObject:opfSta];
        }else{
            OPFMatchStatisticians *opfSta = [OPFMatchStatisticians modelObjectWithDictionary:@{@"team_id":_match.teamB.teamIdentifier,@"role":@"statistician_test"}];
            [opfSta setStatistician:[AXStatistician modelObjectWithDictionary:@{@"id":sta.statisticianIdentifier,@"name":sta.name}]];
            [_allArray addObject:opfSta];
            [_testGuestArray addObject:opfSta];
        }
    }
    
    [_contentTableView reloadData];
}

#pragma mark - Getter
-(AXStatisticianSectionHeaderView *)statisticianHeaderView
{
    if (!_statisticianHeaderView) {
        _statisticianHeaderView = [[AXStatisticianSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
        [_statisticianHeaderView.titleLabel setText:@"主力统计员"];
        _statisticianHeaderView.delegate = self;
    }
    return _statisticianHeaderView;
}

-(AXStatisticianSectionHeaderView *)statisticianTestHeaderView
{
    if (!_statisticianTestHeaderView) {
        _statisticianTestHeaderView = [[AXStatisticianSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
        [_statisticianTestHeaderView.titleLabel setText:@"测试统计员"];
        _statisticianTestHeaderView.delegate = self;
    }
    return _statisticianTestHeaderView;
}
-(AXStatisticianSearchView *)searchView
{
    if (!_searchView) {
        _searchView = [[AXStatisticianSearchView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height) onView:self.view];
        _searchView.deleagte = self;
    }
    return _searchView;
}
@end
