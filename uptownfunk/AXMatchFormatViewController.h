//
//  AXMatchFormatViewController.h
//  uptownfunk
//
//  Created by yellow on 16/4/19.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXMatch.h"

@interface AXMatchFormatViewController : UIViewController

@property (nonatomic, strong)AXMatch *match;
@end
