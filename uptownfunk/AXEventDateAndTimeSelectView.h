//
//  AXEventDateAndTimeSelectView.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/10.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^completed)(NSDate* selcetedDate);

@interface AXEventDateAndTimeSelectView : UIView
@property (nonatomic, copy)completed completed;
- (instancetype)initWithEventTime:(NSDate*)currentDate eventItemType:(AXEventItemType)eventItemType onView:(UIView*)onView;
- (void)show;
- (void)hide;
@end
