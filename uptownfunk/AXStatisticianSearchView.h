//
//  AXStatisticianSearchView.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AXStatisticianSearchView;
@class AXStatistician;
@protocol AXStatisticianSearchViewDelegate <NSObject>

@required
-(void)statisticianSearchView:(AXStatisticianSearchView*)statisticianSearchView didSelectedStatistician:(AXStatistician*)sta;

@end

@interface AXStatisticianSearchView : UIView
@property (nonatomic, assign)BOOL isMain;
@property (nonatomic, assign)BOOL isGuest;

@property (nonatomic, strong)id<AXStatisticianSearchViewDelegate>deleagte;

- (instancetype)initWithFrame:(CGRect)frame onView:(UIView*)onView;
- (void)show;
- (void)cancel;
@end
