//
//  KTAppSingleton.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KTAppSingleton : NSObject
+(instancetype)sharedAppSingleton;

@property (nonatomic, strong)NSArray *currentMatchFormations;
@end
