//
//  AXEvnetsItemSelectViewController.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/9.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXMatch.h"
#import "AXEvent.h"
#import "AXPlayer.h"
#import "AXTeam.h"
#import "AXStatistician.h"
#import "AXStatistician.h"
#import "OPFMatchStatisticians.h"
#import "AXFormation.h"

@interface AXEventItemSelectViewController : UIViewController
@property (nonatomic, strong)AXEvent *event;
@property (nonatomic, assign)AXEventItemType eventItemType;
@property (nonatomic, strong)id dataSource;
@property (nonatomic, strong)NSArray *formations;
@property (nonatomic, strong)NSArray *matchStatisticians;
@end
