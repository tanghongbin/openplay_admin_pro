//
//  TBReachabilityManager.h
//  csair-inspector
//
//  Created by Jarvis on 14-7-3.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface TBReachabilityManager : NSObject
+(instancetype)shareDefaultManager;
-(void)setup;
@end
