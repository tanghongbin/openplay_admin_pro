//
//  NSString+MD5Encode.h
//  bianbian-merchant
//
//  Created by Jarvis on 14-8-27.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <commoncrypto/CommonDigest.h>

typedef NS_ENUM(NSUInteger, NSStringCaseType) {
    NSStringCaseTypeLower = 0,
    NSStringCaseTypeUpper,
};

@interface NSString (MD5Encode)

-(NSString*)MD5Encode:(NSStringCaseType)caseType;
@end
