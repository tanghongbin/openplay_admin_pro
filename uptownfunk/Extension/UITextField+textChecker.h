//
//  UITextField+textChecker.h
//  bianbian-merchant
//
//  Created by Jarvis on 14-8-18.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, UITextFieldTextType) {
    UITextFieldTextTypeDefault,
    UITextFieldTextTypeAccount,
    UITextFieldTextTypePassword,
};

@interface UITextField (textChecker)
<
UIAlertViewDelegate
>

-(BOOL)checkTextFormat:(UITextFieldTextType)_textType;

@end
