//
//  NSString+MD5Encode.m
//  bianbian-merchant
//
//  Created by Jarvis on 14-8-27.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import "NSString+MD5Encode.h"

@implementation NSString (MD5Encode)

-(NSString *)MD5Encode:(NSStringCaseType)caseType
{

    const char *cStr = [self UTF8String];
    
    unsigned char result[16];
    
    CC_MD5( cStr, (CC_LONG)strlen(cStr), result );
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", result[i]];
    }
    
    if (caseType == NSStringCaseTypeLower) {
        return [output lowercaseString];
    }
    return [output uppercaseString];
    
}
@end
