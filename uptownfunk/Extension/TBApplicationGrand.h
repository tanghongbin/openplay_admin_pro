//
//  CSCustomGrand.pch
//  csair-inspector
//
//  Created by Jarvis on 14-6-29.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

/*
 *
 */
#pragma mark - Base-Host
#define BASE_HOST @"http://www.baidu.com"
#define TEST_HOST @"http://121.40.196.229:8082"
//#define TEST_HOST @"http://192.168.1.115:8080/shit"

#pragma mark - Tools Grand

#define DEFAULT_THEME_COLOR UIColorFromRGB(0x7651A3)
#define DEFAULT_BACKGROUND_COLOR UIColorFromRGB(0xEFEFF4)
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define SCREEN_WIDTH [[UIScreen mainScreen] currentMode].size.width/2
#define SCREEN_HEIGHT [[UIScreen mainScreen] currentMode].size.height/2

//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#ifdef DEBUG
#define DBLog(format, ...) NSLog(format, ## __VA_ARGS__)
#else
#define DBLog(format, ...)
#endif
