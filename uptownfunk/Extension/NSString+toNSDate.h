//
//  NSString+toNSDate.h
//  bianbian-merchant
//
//  Created by Jarvis on 14-8-25.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (toNSDate)
+(NSDate*)toNSDate:(NSString*)dateString;
+(long)datestringFromString:(NSString *)dateString;
@end
