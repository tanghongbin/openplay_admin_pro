//
//  NSObject_TBConstValue.h
//  bianbian-merchant
//
//  Created by Jarvis on 14-8-29.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import <Foundation/Foundation.h>
static NSString * const kUpdateRegionNotificationIdentifier = @"kUpdateRegionNotificationIdentifier";
static NSString * const kRefreshOrderNotificationIdentifier = @"kRefreshOrderNotificationIdentifier";
static NSString * const kRefreshOrderBackgroundNotificationIdentifier = @"kRefreshOrderBackgroundNotificationIdentifier";
static NSString * const kShelfOffCommodityTableViewRefreshNotificationIdentifier = @"kShelfOffCommodityTableViewRefreshNotification";
static NSString * const kShelfOnCommodityTableViewRefreshNotificationIdentifier = @"kShelfOnCommodityTableViewRefreshNotification";
static NSString * const kChangedRegionNotificationIdentifier = @"kChangedRegionNotificationIdentifier";
static NSString * const kSearchCancelNotificationIdentifier = @"kSearchCancelNotificationIdentifier";
static NSString * const kSearchDisplayUserRecordViewNotificationIdentifier = @"kSearchDisplayUserRecordViewNotificationIdentifier";
static NSString * const kLoadCustomerAddrNotificationIdentifier = @"kLoadCustomerAddrNotificationIdentifier";

static NSString * const kAutoLoginSwitchStatus = @"kAutoLoginSwitchStatus";
static NSString * const kSearchRecordStoreKey = @"kSearchRecordStoreKey";
static NSString * const kLocalShoppingCartListArray = @"kLocalShoppingCartListArray";
static NSString * const kClearingShoppingCartListArrayWhenLogout = @"kClearingShoppingCartListArrayWhenLogout";

