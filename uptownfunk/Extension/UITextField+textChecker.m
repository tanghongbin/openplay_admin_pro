//
//  UITextField+textChecker.m
//  bianbian-merchant
//
//  Created by Jarvis on 14-8-18.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import "UITextField+textChecker.h"

@implementation UITextField (textChecker)

-(BOOL)checkTextFormat:(UITextFieldTextType)_textType
{
    switch (_textType) {
        case UITextFieldTextTypeDefault:

            break;
        case UITextFieldTextTypeAccount:
        {
            
            if([self.text stringByReplacingOccurrencesOfString:@"" withString:@" "].length <= 0)
            {
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil
                                                                   message:@"请输入手机号"
                                                                  delegate:self
                                                         cancelButtonTitle:@"知道了"
                                                         otherButtonTitles:nil];
                [alertView show];
                return false;

            }
            //手机号以13， 15，18开头，八个 \d 数字字符
            NSString *phoneRegex = @"^1(3|5|7|8|4)\\d{9}";
            NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
            //    NSLog(@"phoneTest is %@",phoneTest);
            if (![phoneTest evaluateWithObject:self.text]) {
                
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil
                                                                   message:@"手机号格式错误"
                                                                  delegate:self
                                                         cancelButtonTitle:@"知道了"
                                                         otherButtonTitles:nil];
                [alertView show];
                return false;
            }else{
                return true;
            }
        }
            break;
        case UITextFieldTextTypePassword:
            {
                NSString *Regex = @"\\w{3,16}";
                NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex];
                if (![emailTest evaluateWithObject:self.text]) {
                    
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil
                                                                       message:@"密码必须大于6位少于16位"
                                                                      delegate:self
                                                             cancelButtonTitle:@"知道了"
                                                             otherButtonTitles:nil];
                    [alertView show];
                    return false;
                }else{
                    return true;
                }
                
            }
            break;
        default:
            break;
    }

    return true;
}

#pragma  mark - UIAlerViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self becomeFirstResponder];
}
@end
