//
//  NSDate+toNSString.h
//  csair-inspector
//
//  Created by Jarvis on 14-6-30.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (toNSString)
- (NSString*)toNSStringByFormatter:(NSString*)_formatter;
+ (NSString *)stringFromDate:(NSString*)mSecond;
@end
