//
//  NSString+toNSDate.m
//  bianbian-merchant
//
//  Created by Jarvis on 14-8-25.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import "NSString+toNSDate.h"

@implementation NSString (toNSDate)

+(NSDate*)toNSDate:(NSString*)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    return date;
}


+(long)datestringFromString:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *date =  [dateFormatter dateFromString:dateString];
    NSTimeInterval diff = [date timeIntervalSince1970]*1000;
    
    
    
    return (long)diff;
    
    
}
@end
