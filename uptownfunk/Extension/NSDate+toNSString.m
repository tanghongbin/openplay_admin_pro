//
//  NSDate+toNSString.m
//  csair-inspector
//
//  Created by Jarvis on 14-6-30.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import "NSDate+toNSString.h"

@implementation NSDate (toNSString)

- (NSString*)toNSStringByFormatter:(NSString*)_formatter
{
    NSString *_dateString;
    NSDateFormatter *_dateFormatter = [NSDateFormatter new];
    [_dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
    [_dateFormatter setDateFormat:_formatter];
    _dateString = [_dateFormatter stringFromDate:self];
    return _dateString;
}

+ (NSString *)stringFromDate:(NSString*)mSecond{
    
    if (mSecond == (NSString*)[NSNull null]) {
        return nil;
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970: [mSecond longLongValue]/1000];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [dateFormatter setTimeZone:zone];
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    [dateFormatter setDateFormat:@"HH:mm"];
    
    NSString *destDateString = [dateFormatter stringFromDate:date];
    DBLog(@"dateString==%@",destDateString);
    
    return destDateString;
}
@end
