//
//  TBReachabilityManager.m
//  csair-inspector
//
//  Created by Jarvis on 14-7-3.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import "TBReachabilityManager.h"

@implementation TBReachabilityManager
{
    Reachability *_hostReachability;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

+(instancetype)shareDefaultManager
{
    static TBReachabilityManager *_tbDefaultManager = nil;
    static dispatch_once_t once_t;
    dispatch_once(&once_t, ^(){
         _tbDefaultManager = [[self alloc]init];
    });
    
    return _tbDefaultManager;
}

-(void)setup
{
    
    /*
     Observe the kNetworkReachabilityChangedNotification. When that notification is posted, the method reachabilityChanged will be called.
     */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    //Change the host name here to change the server you want to monitor.
    
    DBLog(@"host = %@",[[NSURL URLWithString:BASE_HOST] host]);
    _hostReachability = [Reachability reachabilityWithHostName:[[NSURL URLWithString:BASE_HOST] host]];
	[_hostReachability startNotifier];
}

/*!
 * Called by Reachability whenever status changes.
 */
- (void) reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    NSString* statusString = @"";
    BOOL isShowAlert = NO;
    
    switch (netStatus)
    {
        case NotReachable:
        {
            statusString = NSLocalizedString(@"当前网络不可用，请检查您的网络设置", @"");
            isShowAlert = YES;
            break;
        }

        case ReachableViaWWAN:
        {
            statusString = NSLocalizedString(@"Reachable WWAN", @"");
            break;
        }

        case ReachableViaWiFi:
        {
            statusString= NSLocalizedString(@"Reachable WiFi", @"");
            break;
        }
    }
    
    if (isShowAlert) {
//        isShowAlert = NO;
//        UIAlertView *_reachAlertView = [[UIAlertView alloc]initWithTitle:nil message:statusString delegate:nil cancelButtonTitle:@"好" otherButtonTitles:nil];
//        [_reachAlertView show];
    }
}
@end
