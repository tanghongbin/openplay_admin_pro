//
//  TBNetworkingClient.m
//  bianbian-merchant
//
//  Created by Jarvis on 14-8-14.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import "TBNetworkingClient.h"

static NSString * const kTBNetworkingAPIBaseURL = BASE_HOST;
static NSString * const kTBNetworkingAPIBaseURL_TEST = TEST_HOST;

static int const kHttpResponseStatusCode200 = 200;
static int const kHttpResponseStatusCode204 = 204;

static NSString * const kContentType = @"Content-type";
static NSString * const kContentTypeJSON = @"application/json";

//static void showHUD(BOOL show){
////    if (show) {
////        dispatch_async(dispatch_get_main_queue(), ^(){
////            MBHUDView *_hudView = [[MBHUDView alloc]init];
////            _hudView.hudType = MBAlertViewHUDTypeActivityIndicator;
////            _hudView.itemColor = [UIColor whiteColor];
////            _hudView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.9];
////            _hudView.bodyText = @"wait😊";
////            [_hudView addToDisplayQueue];
////        });
////    }
//}

//static void showHUDWithMsg(BOOL show , NSString *msg , float delay , MBAlertViewHUDType type){
////    if (show) {
////        dispatch_async(dispatch_get_main_queue(), ^(){
////            [MBHUDView hudWithBody:msg type:type hidesAfter:delay show:show];
////        });
////
////    }
//}

//static void dismissHUD(){
//        dispatch_async(dispatch_get_main_queue(), ^(){
////            [MBHUDView dismissCurrentHUD];
//        });
//}

#pragma mark - Class Begin
@implementation TBNetworkingClient
{
    NSURL *_baseURL;
    NSURLSessionConfiguration *_sessionConfiguration;
    NSURLSession *_session;
    NSURLSessionDataTask *_task;
}

#pragma mark - Singleton Accessor

- (instancetype)initWithURL:(NSURL*)url
{
    self = [super init];
    if (self) {
        _baseURL = url;
        
        _sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _sessionConfiguration.timeoutIntervalForRequest = 10.0;
        _session = [NSURLSession sessionWithConfiguration:_sessionConfiguration];
        
    }
    return self;
}

+ (instancetype)sharedNetworkingClient {
    static TBNetworkingClient *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[TBNetworkingClient alloc] initWithURL:[NSURL URLWithString:kTBNetworkingAPIBaseURL_TEST]];
    });
    
    return _sharedInstance;
}

#pragma mark - GET/POST/PUT
-(NSURLSessionDataTask *)GET:(NSString *)URLString
                  parameters:(id)parameters
                     success:(void (^)(NSData * data , NSURLResponse * response))success
                        fail:(void(^)())fail
                     showHUD:(BOOL)show
{
//    showHUD(show);
    
    NSMutableURLRequest * _request = [[NSMutableURLRequest alloc]initWithURL:_baseURL
                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                       timeoutInterval:60.0];
    [_request addValue:kContentTypeJSON forHTTPHeaderField:kContentType];
    
    _request.URL = [_baseURL URLByAppendingPathComponent:[URLString copy]];
    _request.HTTPMethod = @"GET";
    _task = [_session dataTaskWithRequest:_request
                        completionHandler:^(NSData * data , NSURLResponse * response , NSError *error){
//                                dismissHUD();
                            if (error) {
//                                showHUDWithMsg(show,
//                                               @"超时" ,
//                                               1.2 , MBAlertViewHUDTypeExclamationMark);
//                                if (fail) {
//                                    fail();
//                                }
                            } else {
                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
                                switch (httpResponse.statusCode) {
                                    case kHttpResponseStatusCode200:
                                    {
                                        if ([[httpResponse.allHeaderFields valueForKey:@"tips"] length] > 0) {
//                                            showHUDWithMsg(show,
//                                                           [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] ,
//                                                           1.2 , MBAlertViewHUDTypeExclamationMark);
                                        }else{
                                            if (success) {
                                                success(data,response);
                                            }
                                        }
                                    }
                                        break;
                                    case kHttpResponseStatusCode204:
                                    {
//                                        showHUDWithMsg(show, @"操作成功" , 0.9 , MBAlertViewHUDTypeCheckmark);
//                                        if (success) {
//                                            success(data,response);
//                                        }
                                    }
                                        break;
                                    default:
                                    {
                                        NSString *msg = [NSString stringWithFormat:@"%ld",(long)[httpResponse statusCode]];
//                                        showHUDWithMsg(show, msg , 0.9 , MBAlertViewHUDTypeCheckmark);
                                    }
                                        break;
                                }
                            }
                        }];
    [_task resume];
    return _task;
}

-(NSURLSessionDataTask *)POST:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void (^)(NSData * data , NSURLResponse * response))success
                         fail:(void(^)())fail
                      showHUD:(BOOL)show
{
//    showHUD(show);
    NSMutableURLRequest * _request = [[NSMutableURLRequest alloc]initWithURL:_baseURL
                                                                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                             timeoutInterval:60.0];
    [_request addValue:kContentTypeJSON forHTTPHeaderField:kContentType];
    _request.URL = [_baseURL URLByAppendingPathComponent:[URLString copy]];
    _request.HTTPMethod = @"POST";
    if (parameters) {
        if (![parameters isKindOfClass:[NSString class]]) {
            _request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
        }else{
            _request.HTTPBody = [parameters data];
        }
    }
    
    _task = [_session dataTaskWithRequest:_request
                        completionHandler:^(NSData * data , NSURLResponse * response , NSError *error){
//                                dismissHUD();

                            if (error) {
//                                showHUDWithMsg(show,
//                                               @"超时" ,
//                                               1.2 , MBAlertViewHUDTypeExclamationMark);
                                if (fail) {
                                    fail();
                                }

                            } else {
                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
                                switch (httpResponse.statusCode) {
                                    case kHttpResponseStatusCode200:
                                    {
                                        if ([[httpResponse.allHeaderFields valueForKey:@"tips"] length] > 0) {
//                                            showHUDWithMsg(show,
//                                                           [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] ,
////                                                           1.2 , MBAlertViewHUDTypeExclamationMark);

                                            if (success) {
                                                success(data,response);
                                            }
                                        }else{
                                            if (success) {
                                                success(data,response);
                                            }
                                        }
                                    }
                                        break;
                                    case kHttpResponseStatusCode204:
                                    {
//                                        showHUDWithMsg(show, @"操作成功" , 0.9 , MBAlertViewHUDTypeCheckmark);
                                        
                                        if (success) {
                                            success(data,response);
                                        }
                                    }
                                        break;
                                    default:
                                    {
                                        NSString *msg = [NSString stringWithFormat:@"%ld",(long)[httpResponse statusCode]];
//                                        showHUDWithMsg(show, msg , 0.9 , MBAlertViewHUDTypeCheckmark);

                                    }
                                        break;
                                }
                            }
                        }];

    [_task resume];
    return _task;
}

-(NSURLSessionDataTask *)PUT:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void (^)(NSData * data , NSURLResponse * response))success
                         fail:(void(^)())fail
                      showHUD:(BOOL)show
{
//    showHUD(show);
    NSMutableURLRequest * _request = [[NSMutableURLRequest alloc]initWithURL:_baseURL
                                                                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                             timeoutInterval:60.0];
    [_request addValue:kContentTypeJSON forHTTPHeaderField:kContentType];
    _request.URL = [_baseURL URLByAppendingPathComponent:[URLString copy]];
    _request.HTTPMethod = @"PUT";
    if (parameters) {
        if (![parameters isKindOfClass:[NSString class]]) {
            _request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
        }else{
            _request.HTTPBody = [parameters data];
        }
    }
    
    _task = [_session dataTaskWithRequest:_request
                        completionHandler:^(NSData * data , NSURLResponse * response , NSError *error){
//                                dismissHUD();

                            if (error) {
//                                showHUDWithMsg(show,
//                                               @"超时" ,
//                                               1.2 , MBAlertViewHUDTypeExclamationMark);
                                if (fail) {
                                    fail();
                                }

                            } else {
                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
                                switch (httpResponse.statusCode) {
                                    case kHttpResponseStatusCode200:
                                    {
                                        if ([[httpResponse.allHeaderFields valueForKey:@"tips"] length] > 0) {
//                                            showHUDWithMsg(show,
//                                                           [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] ,
//                                                           1.2 , MBAlertViewHUDTypeExclamationMark);
                                        }else{
                                            if (success) {
                                                success(data,response);
                                            }
                                        }
                                    }
                                        break;
                                    case kHttpResponseStatusCode204:
                                    {
                                      //  showHUDWithMsg(show, @"操作成功" , 0.9 , MBAlertViewHUDTypeCheckmark);
                                        if (success) {
                                            success(data,response);
                                        }
                                    }
                                        break;
                                    default:
                                    {
                                        NSString *msg = [NSString stringWithFormat:@"%ld",(long)[httpResponse statusCode]];
//                                        showHUDWithMsg(show, msg , 0.9 , MBAlertViewHUDTypeCheckmark);
                                    }
                                        break;
                                }
                            }
                        }];
    
    [_task resume];
    return _task;
}

-(NSURLSessionDataTask *)DELETE:(NSString *)URLString
                  parameters:(id)parameters
                     success:(void (^)(NSData * data , NSURLResponse * response))success
                        fail:(void(^)())fail
                     showHUD:(BOOL)show
{
//    showHUD(show);
    NSMutableURLRequest * _request = [[NSMutableURLRequest alloc]initWithURL:_baseURL
                                                                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                             timeoutInterval:60.0];
    [_request addValue:kContentTypeJSON forHTTPHeaderField:kContentType];
    _request.URL = [_baseURL URLByAppendingPathComponent:[URLString copy]];
    _request.HTTPMethod = @"DELETE";
    if (parameters) {
        if (![parameters isKindOfClass:[NSString class]]) {
            _request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
        }else{
            _request.HTTPBody = [parameters data];
        }
    }
    
    _task = [_session dataTaskWithRequest:_request
                        completionHandler:^(NSData * data , NSURLResponse * response , NSError *error){
//                                dismissHUD();
                            
                            if (error) {
//                                showHUDWithMsg(show,
//                                               @"超时" ,
//                                               1.2 , MBAlertViewHUDTypeExclamationMark);
                                if (fail) {
                                    fail();
                                }

                            } else {
                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
                                switch (httpResponse.statusCode) {
                                    case kHttpResponseStatusCode200:
                                    {
                                        if ([[httpResponse.allHeaderFields valueForKey:@"tips"] length] > 0) {
//                                            showHUDWithMsg(show,
//                                                           [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] ,
//                                                               1.2 , MBAlertViewHUDTypeExclamationMark);

                                        }else{
                                            if (success) {
                                                success(data,response);
                                            }
                                        }
                                    }
                                        break;
                                    case kHttpResponseStatusCode204:
                                    {
//                                        showHUDWithMsg(show, @"操作成功" , 0.9 , MBAlertViewHUDTypeCheckmark);
                                        if (success) {
                                            success(data,response);
                                        }
                                    }
                                        break;
                                    default:
                                    {
                                        NSString *msg = [NSString stringWithFormat:@"%ld",(long)[httpResponse statusCode]];
//                                        showHUDWithMsg(show, msg , 0.9 , MBAlertViewHUDTypeCheckmark);
                                    }
                                        break;
                                }
                            }
                        }];
    
    [_task resume];
    return _task;
}

@end
