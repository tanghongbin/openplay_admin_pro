//
//  TBNetworkingClient.h
//  bianbian-merchant
//
//  Created by Jarvis on 14-8-14.
//  Copyright (c) 2014年 com.thongbin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TBNetworkingClient : NSObject

+(instancetype)sharedNetworkingClient;


- (NSURLSessionDataTask *)GET:(NSString *)URLString
                    parameters:(id)parameters
                      success:(void (^)(NSData * data , NSURLResponse * response))success
                         fail:(void(^)())fail
                       showHUD:(BOOL)show;

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
                       success:(void (^)(NSData * data , NSURLResponse * response))success
                          fail:(void(^)())fail
                       showHUD:(BOOL)show;

- (NSURLSessionDataTask *)PUT:(NSString *)URLString
                    parameters:(id)parameters
                       success:(void (^)(NSData * data , NSURLResponse * response))success
                          fail:(void(^)())fail
                       showHUD:(BOOL)show;

- (NSURLSessionDataTask *)DELETE:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void (^)(NSData * data , NSURLResponse * response))success
                         fail:(void(^)())fail
                      showHUD:(BOOL)show;

@end
