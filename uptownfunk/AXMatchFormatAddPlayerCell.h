//
//  AXMatchFormatAddPlayerCell.h
//  uptownfunk
//
//  Created by yellow on 16/4/22.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXMatchFormatAddPlayerCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@end
