//
//  AXStatisticianSectionHeaderView.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AXStatisticianSectionHeaderView;
@protocol AXStatisticianSectionHeaderViewDelegate <NSObject>

@required
-(void)addStatistician:(AXStatisticianSectionHeaderView*)statisticianSectionHeaderView;

@end
@interface AXStatisticianSectionHeaderView : UITableViewHeaderFooterView
@property (nonatomic, strong)UILabel *titleLabel;
@property (nonatomic, strong)UIButton *addButton;

@property (nonatomic, assign)id<AXStatisticianSectionHeaderViewDelegate>delegate;
@end
