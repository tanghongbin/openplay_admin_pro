//
//  AXEventDateAndTimeSelectView.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/10.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXEventDateAndTimeSelectView.h"

@interface AXEventDateAndTimeSelectView ()<UIPickerViewDelegate,UIPickerViewDataSource>
@property (nonatomic, strong)UIView *onView;
@property (nonatomic, strong)UIView *backgroundView;
@property (nonatomic, strong)UIDatePicker *datePicker;
@property (nonatomic, assign)AXEventItemType eventItemType;
@property (nonatomic, strong)NSDate *currentDate;

@property (nonatomic, strong)UIPickerView *timePickView;

@property (nonatomic, strong)UIButton *todayButton;
@property (nonatomic, strong)UIButton *completedButton;
@property (nonatomic, strong)UIButton *backgroundButton;

@property (nonatomic, strong)NSArray *hours;
@property (nonatomic, strong)NSArray *mins;
@property (nonatomic, strong)NSArray *seconds;



@end
@implementation AXEventDateAndTimeSelectView
- (instancetype)initWithEventTime:(NSDate *)currentDate eventItemType:(AXEventItemType)eventItemType onView:(UIView *)onView
{
    self = [super initWithFrame:CGRectMake(0, 0, onView.width, onView.height)];
    if (self) {
        self.currentDate = currentDate;
        self.eventItemType = eventItemType;
        self.onView = onView;
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.0]];
        [self addSubview:self.backgroundView];
        [self addSubview:self.backgroundButton];
        [self insertSubview:self.backgroundButton belowSubview:self.backgroundView];
        
        NSMutableArray *_tHours = [NSMutableArray new];
        for (int i = 0; i < 24; i++) {
            if (i<10) {
                [_tHours addObject:[NSString stringWithFormat:@"0%i",i]];
            }else
            {
                [_tHours addObject:[NSString stringWithFormat:@"%i",i]];
            }
        }
        _hours = [NSArray arrayWithArray:_tHours];
        
        NSMutableArray *_tMins = [NSMutableArray new];
        NSMutableArray *_tSeconds = [NSMutableArray new];
        for (int i = 0; i < 60; i++) {
            if (i < 10) {
                [_tMins addObject:[NSString stringWithFormat:@"0%i",i]];
                [_tSeconds addObject:[NSString stringWithFormat:@"0%i",i]];
            }else
            {
                [_tMins addObject:[NSString stringWithFormat:@"%i",i]];
                [_tSeconds addObject:[NSString stringWithFormat:@"%i",i]];
            }
        }
        _mins = [NSArray arrayWithArray:_tMins];
        _seconds = [NSArray arrayWithArray:_tSeconds];
        
        [self setTimeSelectView];
    }
    return self;
}

-(void)setTimeSelectView
{
    NSString *startTime =  [NSString formatYearMonthDayHourMinuteSecond:_currentDate.timeIntervalSince1970];
    NSArray *ta = [[startTime componentsSeparatedByString:@" "][1] componentsSeparatedByString:@":"];
    [_timePickView selectRow:[ta[0] integerValue] inComponent:0 animated:YES];
    [_timePickView selectRow:[ta[1] integerValue] inComponent:1 animated:YES];
    [_timePickView selectRow:[ta[2] integerValue] inComponent:2 animated:YES];
}

-(void)show
{
    [_onView addSubview:self];
    [UIView animateWithDuration:0.2 animations:^{
       [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.65]];
        [self.backgroundView setTransform:CGAffineTransformMakeTranslation(0, -240)];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.15 animations:^{
            
        }];
        
        
    }];
}

-(void)hide
{
    [UIView animateWithDuration:0.15 animations:^{
        [self.backgroundView setTransform:CGAffineTransformIdentity];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            [self setBackgroundColor:[[UIColor blueColor] colorWithAlphaComponent:0.0]];
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
}

-(void)completeAction
{
    if (_eventItemType == AXEventItemTypeDate) {
        if (_completed) {
            _completed(_datePicker.date);
        }
    }else{
        
        NSString *dateString = [NSString formatYearMonthDayHourMinuteSecond:_currentDate.timeIntervalSince1970];
        
        NSInteger hourIdx = [_timePickView selectedRowInComponent:0];
        
        if (hourIdx<8) {
            hourIdx = 24 + (hourIdx - 8);
        }else{
            hourIdx = hourIdx - 8;
        }
        
        NSInteger minIdx = [_timePickView selectedRowInComponent:1];
        NSInteger secondIdx = [_timePickView selectedRowInComponent:2];
        
        NSString *newDateString = [NSString stringWithFormat:@"%@ %@:%@:%@",[dateString componentsSeparatedByString:@" "][0],_hours[hourIdx],_mins[minIdx],_seconds[secondIdx]];
        
        NSDate *date = [NSDate dateStringToDate:newDateString];
        
        if (_completed) {
            _completed(date);
        }
    }
}

#pragma mark - UIPickerViewDelegate & UIPickerViewDataSource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 60;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0:
        {
            return _hours.count;
        }
            break;
        case 1:
        {
            return _mins.count;
        }
            break;
        case 2:
        {
            return _seconds.count;
        }
            break;
            
        default:
            return 0;
            break;
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 0:
        {
            return _hours[row];
        }
            break;
        case 1:
        {
            return _mins[row];
        }
            break;
        case 2:
        {
            return _seconds[row];
        }
            break;
            
        default:
            return nil;
            break;
    }
}

#pragma mark - Getter
-(UIView *)backgroundView
{
    if (!_backgroundView) {
        _backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, _onView.height, _onView.width, 240)];
        [_backgroundView setBackgroundColor:[UIColor defaultBackGroundColor]];
        [_backgroundView addSubview:_eventItemType == AXEventItemTypeDate?self.datePicker:self.timePickView];
        [_backgroundView addSubview:self.completedButton];
    }
    return _backgroundView;
}

-(UIDatePicker *)datePicker
{
    if (!_datePicker) {
        _datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 40, self.backgroundView.width, 200)];
        [_datePicker setDatePickerMode:UIDatePickerModeDate];
        [_datePicker setDate:_currentDate];
    }
    return _datePicker;
}

-(UIPickerView *)timePickView
{
    if (!_timePickView) {
        _timePickView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 40, self.backgroundView.width, 200)];
        _timePickView.delegate = self;
        _timePickView.dataSource = self;
        _timePickView.showsSelectionIndicator = YES;
    }
    return _timePickView;
}

-(UIButton *)completedButton
{
    if (!_completedButton) {
        _completedButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_completedButton setFrame:CGRectMake(0, 0, _backgroundView.width, 40)];
        [_completedButton setTitle:@"完成" forState:UIControlStateNormal];
        [_completedButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
        [_completedButton setBackgroundImage:[UIImage imageNamed:@"opf_btn_delete_bg_normal"] forState:UIControlStateNormal];
        [_completedButton setBackgroundImage:[UIImage imageNamed:@"opf_btn_delete_bg_higlight"] forState:UIControlStateHighlighted];
        [_completedButton setTintColor:[UIColor lightThemeColor]];
        [_completedButton bk_addEventHandler:^(id sender) {
            [self hide];
            [self completeAction];
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _completedButton;
}


-(UIButton *)backgroundButton
{
    if (!_backgroundButton) {
        _backgroundButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backgroundButton setTitle:@"取消戳我🙄️" forState:UIControlStateNormal];
        [_backgroundButton setFrame:CGRectMake(0, 0, _onView.width, _onView.height)];
        [_backgroundButton bk_addEventHandler:^(id sender) {
            [self hide];
            if (_completed) {
                _completed(nil);
            }
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _backgroundButton;
}

@end
