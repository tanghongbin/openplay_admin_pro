//
//  AXStatisticianTableViewCell.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPFMatchStatisticians.h"
#import "AXStatistician.h"

@class AXStatisticianTableViewCell;
@protocol AXStatisticianTableViewCellDelegate <NSObject>

@required
-(void)statisticianTableViewCell:(AXStatisticianTableViewCell*)statisticianTableViewCell didDelete:(OPFMatchStatisticians*)sta;

@end
@interface AXStatisticianTableViewCell : UITableViewCell
@property (nonatomic, strong)OPFMatchStatisticians *matchStatistician;

@property (nonatomic, assign)id<AXStatisticianTableViewCellDelegate>delegate;
@end
