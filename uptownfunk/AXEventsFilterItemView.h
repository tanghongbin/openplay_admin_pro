//
//  AXEventsFilterItemView.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/29.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXEventsFilterItemView : UIView
@property (nonatomic, assign)BOOL isOpened;
@property (nonatomic, copy)NSString *itemTitle;
-(instancetype)initWithFrame:(CGRect)frame Selected:(void (^)(AXEventsFilterItemView *item))selected;
@end
