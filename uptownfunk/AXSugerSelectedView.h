//
//  AXSugerSelectedView.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/27.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, ItemType) {
    ItemTypeStatistician,
    ItemTypeFormation,
    ItemTypeEvents,
};
@protocol AXSugerSelectedViewDelegate <NSObject>

@required
-(void)sugerSelected:(ItemType)itemType;

@end
@interface AXSugerSelectedView : UIView
@property (nonatomic ,assign)id<AXSugerSelectedViewDelegate>delegate;
- (instancetype)initWithContainerView:(UIView*)cView Delegate:(id<AXSugerSelectedViewDelegate>)d;
@end


@protocol AXSugerItemViewDelegate <NSObject>

@required
-(void)itemSelected:(ItemType)itemType;
-(void)cancel;

@end
@interface AXSugerItemsView : UIView
@property (nonatomic, assign)id<AXSugerItemViewDelegate>delegate;
-(void)cancel;
- (instancetype)initWithContainerView:(UIView*)cView CurrentItem:(NSString*)currentItem;
@end