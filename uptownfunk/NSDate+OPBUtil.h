//
//  NSDate+OPBUtil.h
//  OpenPlay
//
//  Created by Kelvin Tong on 15/12/18.
//  Copyright © 2015年 laihj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (OPBUtil)
+(NSString *)intervalFromLastDateStr: (NSString *) dateString1 toTheDateStr:(NSString *) dateString2;
+(NSString *)intervalFromLastDate: (NSDate *) date1 toTheDate:(NSDate*) date2;
+(NSTimeInterval)timeIntervalFromDate: (NSDate *) before toDate:(NSDate*) after;
+ (NSDate *) dateFromLocalString:(NSString *) dateString;

+(NSDate *)utcTimeToLocaleDate:(NSTimeInterval)utcTimeMilliSeconds; //utc毫秒数转化为本地日期+

+(NSTimeInterval)utcDateToTimeMilliSeconds:(NSDate *)date; //日期转换为UTC值

+(NSTimeInterval)utcTimeMilliSecondsNow; //返回当前时间的UTC值

+(NSTimeInterval)dateStringToUtcTime:(NSString *)dateString; //转化时间字符串为utc时间值。

+(NSString *)dateToDateString:(NSDate *)date; //日期转化为字符串

+(NSDate *)dateStringToDate:(NSString *)dateString; //字符串转化为日期

+(NSString *)utcTimeToDataString:(NSNumber *)time; //utc毫秒数转化为本地日期字符串
+(NSString *)timeStringFromDate:(NSDate *)date; //转换日期为日期时间字符串“YYY-MM-DD HH:mm:ss"
+(NSString *)timeStringFromDateWithoutSecond:(NSDate *)date;
@end
