//
//  AXStatisticianSectionHeaderView.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXStatisticianSectionHeaderView.h"

@interface AXStatisticianSectionHeaderView()

@end

@implementation AXStatisticianSectionHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundView:nil];
        [self.contentView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.86]];
        [self addSubview:self.titleLabel];
        [self addSubview:self.addButton];
    }
    return self;
}
#pragma mark - Getter

-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 100, 30)];
        [_titleLabel setTextColor:[UIColorWithRGB(0x40C0FC) colorWithAlphaComponent:0.85]];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    }
    return _titleLabel;
}

-(UIButton *)addButton
{
    if (!_addButton) {
        _addButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addButton setFrame:CGRectMake(_titleLabel.x+_titleLabel.width + 10, 3, 60, 24)];
        [_addButton setTitle:@"添加" forState:UIControlStateNormal];
        [_addButton.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [_addButton setBackgroundImage:[UIImage imageFromColor:UIColorWithRGB_Alpha(0x40C0FC, 1.0)] forState:UIControlStateNormal];
        [_addButton setBackgroundImage:[UIImage imageFromColor:UIColorWithRGB_Alpha(0x40C0FC, 0.65)] forState:UIControlStateHighlighted];
        [_addButton.layer setCornerRadius:2];
        [_addButton.layer setMasksToBounds:YES];
        [_addButton bk_addEventHandler:^(id sender) {
            if ([_delegate respondsToSelector:@selector(addStatistician:)]) {
                [_delegate addStatistician:self];
            }
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _addButton;
}
@end
