//
//  AXStatisticiansManagerViewController.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/17.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXMatch.h"

@interface AXStatisticiansManagerViewController : UIViewController
@property (nonatomic, strong)AXMatch *match;
@end
