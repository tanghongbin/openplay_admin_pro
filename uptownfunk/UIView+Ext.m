//
//  UIView+Ext.m
//  uptownfunk
//
//  Created by yellow on 16/4/19.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "UIView+Ext.h"
#import <MBProgressHUD.h>

@implementation UIView (Ext)

- (void)showToast:(NSString *)text
{
    [self showToast:text delay:1.5];
}
- (void)showToast:(NSString *)text delay:(NSInteger)delay
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self];
    hud.mode = MBProgressHUDModeText;
    [self addSubview:hud];
    hud.labelText = text;
    [hud show:YES];
    [hud hide:YES afterDelay:delay];
}

- (void)showLoadingView:(NSString *)text
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self];
    hud.mode = MBProgressHUDModeIndeterminate;
    [self addSubview:hud];
    hud.labelText = text;
    [hud show:YES];
}

- (void)showLoadingView
{
    NSDictionary *textDic = @{@0:@"😊等会errr~",
                              @1:@"🙄️急什么~",
                              @2:@"😄哈哈哈哈哈~",
                              @3:@"🙄️哈哈"};
    
    int ran_int = arc4random() % 3;
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self];
    hud.mode = MBProgressHUDModeIndeterminate;
    [self addSubview:hud];
    hud.labelText = textDic[[NSNumber numberWithInt:ran_int]];
    [hud show:YES];
}

- (void)hiddenLoadingView
{
    [MBProgressHUD hideAllHUDsForView:self animated:NO];
}


@end
