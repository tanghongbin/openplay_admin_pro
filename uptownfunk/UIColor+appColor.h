//
//  UIColor+appColor.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (appColor)
+(UIColor *)defaultBackGroundColor;
+(UIColor*)darkThemeColor;
+(UIColor*)lightThemeColor;
+(UIColor*)unableColor;
+(UIColor *)defaultTextColor;
@end
