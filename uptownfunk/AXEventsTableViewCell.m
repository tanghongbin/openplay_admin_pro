//
//  AXEventsTableViewCell.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/26.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXEventsTableViewCell.h"
#import "AXTeam.h"
#import "AXPlayer.h"
#import "NSDate+OPBUtil.h"
#import "NSString+Extra.h"

@interface AXEventsTableViewCell()
@property (nonatomic, strong)UIView *seperateView;
@property (nonatomic, strong)UIView *containView;

@end
@implementation AXEventsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setFrame:CGRectMake(0, 0, iPad?768:SCREEN_WIDTH, 40)];
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleDefault;
        UIView *selectedView = [[UIView alloc]init];
        [selectedView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.65]];
        self.selectedBackgroundView = selectedView;
        [self.contentView addSubview:self.containView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setEvent:(AXEvent *)event
{
    _event = event;
    [_teamLabel setText:_event.team.name];
    [_playerLabel setText:(_event.player&&_event.player.playerIdentifier)?[NSString stringWithFormat:@"%.0f｜%@",_event.shirtNumber,_event.player.name]:@"无"];
    [_eventLabel setText:[AXStaticDataHelper eventCodeForStringWithKey:[NSString stringWithFormat:@"%.0f",_event.code]]];
    
    NSArray *strArray = [_event.clientStartedAt componentsSeparatedByString:@"."];
    if ([strArray count] > 1) {
        NSString *startTime = [NSString formatYearMonthDayHourMinuteSecond:[NSDate dateStringToDate:strArray[0]].timeIntervalSince1970];
        [_timeLabel setText:startTime];
    }
}

#pragma mark - Getter
-(UIView *)containView
{
    if (!_containView) {
        _containView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.width - 0, self.height - 0)];
        [_containView setBackgroundColor:[UIColor defaultTextColor]];
        [_containView addSubview:self.seperateView];
        [_containView addSubview:self.teamLabel];
        [_containView addSubview:self.playerLabel];
        [_containView addSubview:self.eventLabel];
        [_containView addSubview:self.timeLabel];
    }
    return _containView;
}

-(UIView *)seperateView
{
    if (!_seperateView) {
        _seperateView = [[UIView alloc]initWithFrame:CGRectMake(0, _containView.height - 1, _containView.width, 1)];
        [_seperateView setBackgroundColor:UIColorWithRGB(0x4a4a4a)];
    }
    return _seperateView;
}

-(UILabel *)teamLabel
{
    if (!_teamLabel) {
        _teamLabel = [self headerLabelWithFrame:CGRectMake(10, 0, iPad?140:60, _containView.height) Title:@"球队"];
    }
    return _teamLabel;
}

-(UILabel *)playerLabel
{
    if (!_playerLabel) {
        _playerLabel = [self headerLabelWithFrame:CGRectMake(_teamLabel.x+_teamLabel.width + 10, 0, iPad?100:65, _containView.height) Title:@"球员"];
    }
    return _playerLabel;
}

-(UILabel *)eventLabel
{
    if (!_eventLabel) {
        _eventLabel = [self headerLabelWithFrame:CGRectMake(_playerLabel.x+_playerLabel.width + 10, 0, 80, _containView.height) Title:@"事件"];
    }
    return _eventLabel;
}

-(UILabel *)timeLabel
{
    if (!_timeLabel) {
        _timeLabel = [self headerLabelWithFrame:CGRectMake(_eventLabel.x + _eventLabel.width + 5, 0, 130, _containView.height) Title:@"时间"];
    }
    return _timeLabel;
}

-(UILabel*)headerLabelWithFrame:(CGRect)frame Title:(NSString*)title
{
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:frame];
    [headerLabel setTextColor:[UIColor defaultBackGroundColor]];
    [headerLabel setTextAlignment:NSTextAlignmentLeft];
    [headerLabel setFont:[UIFont systemFontOfSize:12]];
    [headerLabel setText:title];
    return headerLabel;
}
@end
