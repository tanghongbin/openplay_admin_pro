//
//  UIImage+Extra.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/26.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extra)
+ (UIImage *)imageFromColor:(UIColor *)color;
@end
