//
//  AXMasterTableViewController.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXMasterTableViewController.h"
#import "AXMatchFormatViewController.h"
#import "AXEventsViewController.h"
#import "AXParentViewController.h"
#import "AXMasterTableViewCell.h"
#import "OPFMatches_Day.h"
#import "NSString+Extra.h"
#import "AXMenuView.h"
#import "AXAddTestMatchViewController.h"

@interface AXMasterTableViewController ()<AXMenuViewDelegate>
@property (nonatomic, strong)NSMutableArray *matches;
@property (nonatomic, strong)AXMenuView *menuView;
@end

@implementation AXMasterTableViewController

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdatedEventNotificaitonName object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setBackgroundColor:[UIColor defaultTextColor]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UIBarButtonItem *logoutBar = [[UIBarButtonItem alloc]initWithTitle:@"退出登录" style:UIBarButtonItemStyleDone target:self action:@selector(logoutBarClicked:)];
    self.navigationItem.leftBarButtonItem = logoutBar;
    
    UIBarButtonItem *moreBar = [[UIBarButtonItem alloc]initWithTitle:@"😛Menu" style:UIBarButtonItemStyleDone target:self action:@selector(moreBarClicked:)];
    self.navigationItem.rightBarButtonItem = moreBar;
    
    [self.tableView registerClass:[AXMasterTableViewCell class] forCellReuseIdentifier:NSStringFromClass([AXMasterTableViewCell class])];
    
     self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    [self.tableView.mj_header beginRefreshing];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadNewData) name:kUpdatedEventNotificaitonName object:nil];
}

-(void)loadNewData
{
    NSString *dateStr = [NSString formatYearMonthDay:[NSDate date].timeIntervalSince1970];
//    NSString *dateStr = @"2016-07-01";
    [[OPLDataManager manager] fetchMatches:dateStr response:^(id matches, NSString *errorMessage) {
        
        [[self.tableView mj_header] endRefreshing];
        OPFMatches_Day *matchList = [[OPFMatches_Day alloc] initWithDictionary:matches];
        self.matches = [NSMutableArray arrayWithArray:matchList.list];
        [self.matches sortUsingComparator:^NSComparisonResult(AXMatch*  _Nonnull obj1, AXMatch*  _Nonnull obj2) {
            return [obj1.startAt compare:obj2.startAt];
        }];
        [self.tableView reloadData];
        
        
        if ([_matches count] > 0) {
            if(iPad)
            {
                [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
                [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            }
        }else{
            [self.view showToast:@"今天又没比赛，休息下♨️"];
        }
    }];
}

- (void)logoutBarClicked:(UIBarButtonItem *)bar
{
    UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"确定退出登录嘛？🙄️" message:nil delegate:nil cancelButtonTitle:@"嘛~憋这样😁" otherButtonTitles:@"退了退了😠", nil];
    [alertview bk_setHandler:^{
        [self dismissViewControllerAnimated:YES completion:^{
            [AXUser userRemove];
        }];
    } forButtonAtIndex:1];
    [alertview show];
}

- (void)moreBarClicked:(UIBarButtonItem *)bar
{
    [self.menuView trigger];
}

-(void)menuView:(AXMenuView*)menuView selectedIndex:(NSInteger)buttonIndex
{
    AXAddTestMatchViewController *vc = [[AXAddTestMatchViewController alloc]init];
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:vc animated:NO completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _matches.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 160;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AXMasterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AXMasterTableViewCell class]) forIndexPath:indexPath];
    [cell setFrame:CGRectMake(0, 0, self.tableView.width, 160)];
    [cell setMatch:_matches[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UISplitViewController *sVC = [self splitViewController];
    AXParentViewController *dc = nil;
    if (iPad) {
        UINavigationController *navVC = [sVC viewControllers][1];
        dc = (AXParentViewController*)navVC.topViewController;
        dc.match = _matches[indexPath.row];
    }else
    {
        UINavigationController *navVC = [sVC viewControllers][0];
        AXParentViewController *pVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AXParentViewController"];
        pVC.match = _matches[indexPath.row];
        [navVC pushViewController:pVC animated:YES];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self becomeFirstResponder];
}

#pragma mark - Getter
-(NSMutableArray *)matches
{
    if (!_matches) {
        _matches = [NSMutableArray new];
    }
    return _matches;
}

-(AXMenuView *)menuView
{
    if (!_menuView) {
        _menuView = [[AXMenuView alloc]initOnView:self.view];
        _menuView.delegate = self;
    }
    return _menuView;
}

@end
