//
//  AXEventsTableViewCell.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/26.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXEvent.h"

@interface AXEventsTableViewCell : UITableViewCell
@property (nonatomic, strong)UILabel *teamLabel;
@property (nonatomic, strong)UILabel *playerLabel;
@property (nonatomic, strong)UILabel *eventLabel;
@property (nonatomic, strong)UILabel *timeLabel;
@property (nonatomic, strong)AXEvent *event;
@end
