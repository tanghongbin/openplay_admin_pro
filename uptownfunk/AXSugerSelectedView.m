//
//  AXSugerSelectedView.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/27.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXSugerSelectedView.h"

@interface AXSugerSelectedView()<AXSugerItemViewDelegate>
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *indicatorImageView;
@property (nonatomic, strong) UIButton *selectedButton;
@property (nonatomic, assign) CGRect originRect;
@property (nonatomic, assign) BOOL isOpened;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) AXSugerItemsView *itemView;
@property (nonatomic, assign) ItemType curretnItemType;
@end
@implementation AXSugerSelectedView

- (instancetype)initWithContainerView:(UIView *)cView Delegate:(id<AXSugerSelectedViewDelegate>)d
{
    self = [super initWithFrame:CGRectMake(0, 0, 110, 40)];
    if (self) {
        self.originRect = CGRectMake(0, 0, 110, 40);
        self.containerView = cView;
        self.delegate = d;
        self.curretnItemType = ItemTypeFormation;
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    [self addSubview:self.titleLabel];
    [self addSubview:self.indicatorImageView];
}

#pragma mark - Getter
-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.width/2 - 40, 0, 80, self.height)];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setFont:[UIFont systemFontOfSize:20.0]];
        [_titleLabel setTextColor:[UIColor defaultBackGroundColor]];
        [_titleLabel setText:@"阵容设置"];
        [_titleLabel setUserInteractionEnabled:YES];
        [_titleLabel addSubview:self.selectedButton];
    }
    return _titleLabel;
}

-(UIImageView *)indicatorImageView
{
    if (!_indicatorImageView) {
        _indicatorImageView = [[UIImageView alloc]initWithFrame:CGRectMake(_titleLabel.x + _titleLabel.width + 5, 15, 10, 10)];
        [_indicatorImageView setImage:[UIImage imageNamed:@"Common_Icon_Arrow_down"]];
    }
    return _indicatorImageView;
}

-(UIButton *)selectedButton
{
    if (!_selectedButton) {
        _selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selectedButton.layer setCornerRadius:5.0];
        [_selectedButton.layer setMasksToBounds:YES];
        [_selectedButton setFrame:CGRectMake(-5, 0, 110, _titleLabel.height - 5)];
        [_selectedButton setBackgroundImage:[UIImage imageFromColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.2]] forState:UIControlStateSelected];
        [_selectedButton setBackgroundImage:[UIImage imageFromColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.2]] forState:UIControlStateHighlighted];
        [_selectedButton setBackgroundImage:[UIImage imageFromColor:[UIColor clearColor]] forState:UIControlStateNormal];
        [_selectedButton bk_addEventHandler:^(UIButton* sender) {
            [self.containerView endEditing:YES];
            if (_isOpened) {
                [self.itemView cancel];
                return ;
            }else{
                _isOpened = YES;
                self.itemView = [[AXSugerItemsView alloc]initWithContainerView:self.containerView CurrentItem:_titleLabel.text];
                _itemView.delegate = self;
                [self.containerView addSubview:_itemView];
            }
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectedButton;
}

-(void)itemSelected:(ItemType)itemType
{
    if (_curretnItemType == itemType) {
        return;
    }else{
        self.curretnItemType = itemType;
        [_titleLabel setText:[AXStaticDataHelper operationStringWithKey:itemType]];
        _isOpened = NO;
        if ([_delegate respondsToSelector:@selector(sugerSelected:)]) {
            [_delegate sugerSelected:itemType];
        }
    }
}
-(void)cancel
{
    _isOpened = NO;
}

@end

#pragma mark - AXSugerItemView
@interface AXSugerItemsView()
@property (nonatomic, strong)UIButton *statisticiansButton;
@property (nonatomic, strong)UIButton *formationButton;
@property (nonatomic, strong)UIButton *eventsButton;
@property (nonatomic, strong)UIView *containerView;
@property (nonatomic, strong)UIView *sView;
@property (nonatomic, copy)NSString *currentItem;
@end
@implementation AXSugerItemsView

- (instancetype)initWithContainerView:(UIView*)cView CurrentItem:(NSString*)currentItem
{
    self.sView = cView;
    self.currentItem = currentItem;
    CGRect rect = CGRectMake(0, 0, _sView.width, _sView.height);
    self = [super initWithFrame:rect];
    if (self) {
        [self setBackgroundColor:[[UIColor defaultTextColor] colorWithAlphaComponent:0.0]];
        [self addSubview:self.containerView];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.3 animations:^{
                [self setBackgroundColor:[[UIColor defaultTextColor] colorWithAlphaComponent:0.85]];
                [self.containerView setTransform:CGAffineTransformMakeTranslation(0, 60)];
            }];
        });
    }
    
    [self bk_whenTapped:^{
        [self cancel];
    }];
    return self;
}

-(void)cancel
{
    [UIView animateWithDuration:0.3 animations:^{
        [self.containerView setTransform:CGAffineTransformIdentity];
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.0]];
    } completion:^(BOOL finished) {
        if ([_delegate respondsToSelector:@selector(cancel)]) {
            [_delegate cancel];
        }
        [self removeFromSuperview];
    }];
}


#pragma mark - AXSugerItemView Getter
-(UIButton *)statisticiansButton
{
    if (!_statisticiansButton) {
        _statisticiansButton = [self itemButton];
        [_statisticiansButton setTitle:[AXStaticDataHelper operationStringWithKey:ItemTypeStatistician] forState:UIControlStateNormal];
        if ([_currentItem isEqualToString:[_statisticiansButton titleForState:UIControlStateNormal]]) {
            [_statisticiansButton setSelected:YES];
        }
    }
    return _statisticiansButton;
    
}


-(UIButton *)formationButton
{
    if (!_formationButton) {
        _formationButton = [self itemButton];
        [_formationButton setTitle:[AXStaticDataHelper operationStringWithKey:ItemTypeFormation] forState:UIControlStateNormal];
        if ([_currentItem isEqualToString:[_formationButton titleForState:UIControlStateNormal]]) {
            [_formationButton setSelected:YES];
        }
    }
    return _formationButton;

}

-(UIButton *)eventsButton
{
    if (!_eventsButton) {
        _eventsButton = [self itemButton];
        [_eventsButton setTitle:[AXStaticDataHelper operationStringWithKey:ItemTypeEvents] forState:UIControlStateNormal];
        if ([_currentItem isEqualToString:[_eventsButton titleForState:UIControlStateNormal]]) {
            [_eventsButton setSelected:YES];
        }
    }
    return _eventsButton;
    
}

-(UIButton *)itemButton
{
    UIButton *itemButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [itemButton.layer setCornerRadius:4.0];
    [itemButton.layer setMasksToBounds:YES];
    [itemButton setFrame:CGRectMake(0, 0, 100, 40)];
    [itemButton setBackgroundImage:[UIImage imageFromColor:[[UIColor lightThemeColor] colorWithAlphaComponent:1.0]] forState:UIControlStateSelected];
    [itemButton setBackgroundImage:[UIImage imageFromColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.2]] forState:UIControlStateHighlighted];
    [itemButton setBackgroundImage:[UIImage imageFromColor:[UIColor clearColor]] forState:UIControlStateNormal];
    [itemButton bk_addEventHandler:^(UIButton* sender) {
        [self cancel];
        if ([_delegate respondsToSelector:@selector(itemSelected:)]) {
            if ([[sender titleForState:UIControlStateNormal] isEqualToString:[AXStaticDataHelper operationStringWithKey:ItemTypeStatistician]]) {
                [_delegate itemSelected:ItemTypeStatistician];
            }
            else if ([[sender titleForState:UIControlStateNormal] isEqualToString:[AXStaticDataHelper operationStringWithKey:ItemTypeFormation]]) {
                [_delegate itemSelected:ItemTypeFormation];
            }else if ([[sender titleForState:UIControlStateNormal] isEqualToString:[AXStaticDataHelper operationStringWithKey:ItemTypeEvents]])
            {
                [_delegate itemSelected:ItemTypeEvents];
            }
        }
    } forControlEvents:UIControlEventTouchUpInside];
    return itemButton; 
}

-(UIView *)containerView
{
    if (!_containerView) {
        _containerView = [[UIView alloc]initWithFrame:CGRectMake(0, -60, _sView.width, 60)];
        [_containerView setBackgroundColor:[UIColor darkTextColor]];
        [self.statisticiansButton setCenter:CGPointMake(_containerView.width/6, _containerView.height/2)];
        [self.formationButton setCenter:CGPointMake(_containerView.width/6 * 3, _containerView.height/2)];
        [self.eventsButton setCenter:CGPointMake(_containerView.width/6 * 5, _containerView.height/2)];
        [_containerView addSubview:self.statisticiansButton];
        [_containerView addSubview:self.formationButton];
        [_containerView addSubview:self.eventsButton];
    }
    return _containerView;
}

@end
