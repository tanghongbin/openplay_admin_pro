//
//  NSString+Extra.m
//  Roosher
//
//  Created by Levin on 10-9-27.
//  Copyright 2010 Roosher inc. All rights reserved.
//

#import "NSString+Extra.h"


@implementation NSString (Extra)
//只有数字
- (BOOL)isOnlyNumber
{
    NSString *regex = @"^[0-9]*$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:self];
}
-(BOOL)notEmpty
{
    if (!self || [[self stringByTrimmingBoth] length] == 0) {
        return NO;
    }
    return YES;
}

+ (NSString*)stringWithNewUUID {
    // Create a new UUID
    CFUUIDRef uuidObj = CFUUIDCreate(nil);
    CFStringRef uuidString = CFUUIDCreateString(nil, uuidObj);
    NSString *result = (__bridge_transfer NSString *)CFStringCreateCopy(NULL, uuidString);
    CFRelease(uuidObj);
    CFRelease(uuidString);
    return result;
}

- (NSString *)URLEncodedString {
    NSString *result = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                           (__bridge CFStringRef)self,
                                                                           NULL,
																		   CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                           kCFStringEncodingUTF8);
	return result;
}

- (NSString*)URLDecodedString {
	NSString *result = (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault, (__bridge CFStringRef)self, CFSTR(""), kCFStringEncodingUTF8);
	return result;	
}

- (NSString*)stringByTrimmingBoth {
    NSString *trimmed = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return trimmed;
}

- (NSString*)stringByTrimmingLeadingWhitespace {
    NSInteger i = 0;
    
    while ([[NSCharacterSet whitespaceCharacterSet] characterIsMember:[self characterAtIndex:i]]) {
        i++;
    }
    return [self substringFromIndex:i];
}

+(NSString *)formatYearMonthDayHourMinuteSecond:(NSTimeInterval)interval
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    return [formatter stringFromDate:date];
}

+ (NSString*)formatYearMonthDayHourMinute:(NSTimeInterval)interval
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat: @"MM-dd HH:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    return [formatter stringFromDate:date];
}

+ (NSString*)formatYearMonth:(NSTimeInterval)interval
{
    if (interval == 0) {
        return @"";
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat: @"yyyy.MM"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    return [formatter stringFromDate:date];
}

+ (NSString*)formatYearMonthCN:(NSTimeInterval)interval
{
    if (interval == 0) {
        return @"";
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat: @"yyyy年M月"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    return [formatter stringFromDate:date];
}

+ (NSString*)formatYearMonthDay:(NSTimeInterval)interval
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat: @"yyyy-MM-dd"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    return [formatter stringFromDate:date];
}

+ (NSString*)formatDay:(NSTimeInterval)interval
{
    if (interval == 0) {
        return @"";
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat: @"dd"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    return [formatter stringFromDate:date];
}

+ (NSString*)formatHourMinute:(NSTimeInterval)interval
{
    if (interval == 0) {
        return @"";
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat: @"mm:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    return [formatter stringFromDate:date];
}

+ (NSString*)formatMinuteSecond:(NSTimeInterval)interval
{
    if (interval == 0) {
        return @"";
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat: @"mm:ss.SS"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    return [formatter stringFromDate:date];
}

+ (NSString*)formatTimeAgo:(NSTimeInterval)interval
{
    NSTimeInterval current = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval detal = current - interval;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if (detal <= 0) {
        return @"刚刚";
    }
    else if (detal <= 3600) {
        [formatter setDateFormat: @"HH:mm"];
    }
    else if (detal < 3600*24) {
        return [NSString stringWithFormat:@"%d小时前", (NSInteger)detal / 3600];
    }
    else {
        [formatter setDateFormat: @"MM-dd"];
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    return [formatter stringFromDate:date];
}

+ (NSString*)formatYearMonthDayHourMinuteShort:(NSTimeInterval)interval
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat: @"yyyy-MM-dd HH:mm"];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDate* dateNow = [NSDate date];
    
    NSString* timeStr = [formatter stringFromDate:date];
    NSString* timeNowStr = [formatter stringFromDate:dateNow];
    
    NSString* year1 = [timeStr substringToIndex:4];
    NSString* year2 = [timeNowStr substringToIndex:4];

    NSString* day1 = [timeStr substringToIndex:10];
    NSString* day2 = [timeNowStr substringToIndex:10];

    NSString* str = year1;
    if ([year1 isEqualToString:year2]) {
        // 同一年
        if ([day1 isEqualToString:day2]) {
            // 同一天
            str = [timeStr substringFromIndex:11];
        }
        else {
            str = [timeStr substringFromIndex:5];
            str = [str substringToIndex:str.length - 6];
        }
    }
    else {
        str = [timeStr substringToIndex:timeStr.length - 6];
    }
    
    return str;
}

- (CGFloat)measureTextHeight:(UIFont *)desFont desWidth:(CGFloat)desWidth
{
    if (self.length < 1) {
        return 10.0f;
    }
    
    CGFloat result = 0.0f;

    CGSize textSize = CGSizeMake(desWidth, 100000.0f);
    CGSize size = [self sizeWithFont:desFont
                   constrainedToSize:textSize
                       lineBreakMode:NSLineBreakByWordWrapping];
    result = size.height;	// at least one row

    return result;
}
- (CGFloat)measureTextHeight:(UIFont *)desFont desWidth:(CGFloat)desWidth maxHeight:(CGFloat)height
{
    if (self.length < 1) {
        return 10.0f;
    }
    
    CGFloat result = 0.0f;
    
    CGSize textSize = CGSizeMake(desWidth, height);
    CGSize size = [self sizeWithFont:desFont
                   constrainedToSize:textSize
                       lineBreakMode:NSLineBreakByWordWrapping];
    result = size.height;	// at least one row
    return result;
}
- (NSString*)trimText
{
    if (self.length > 0) {
        NSCharacterSet* set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString* text = [self stringByTrimmingCharactersInSet:set];
        return text;
    }
    else {
        return self;
    }
}


- (NSString *)stringRemoveZhongTags
{
	@autoreleasepool {
//        NSString *pattern = @"(?<!=\")\\b((http|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%%&amp;:/~\\+#]*[\\w\\-\\@?^=%%&amp;/~\\+#])?)";
//        NSString *pattern = @"<(\S*?)[^>]*>.*?</\1>|<.*? />";
        // 去掉img
//        NSString *pattern = @"\\[img\\].*?\\[/img\\]";
        NSString *pattern = @"\\[img\\].*?\\[/img\\]";
        NSRegularExpression *regex1 = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                                options:0
                                                                                  error:nil];
        NSString *modifiedString = [regex1 stringByReplacingMatchesInString:self
                                                                   options:0
                                                                     range:NSMakeRange(0, [self length])
                                                              withTemplate:@""];
//        pattern = @"\\[color=#.*?\\[/color\\]";
//        NSRegularExpression *regex2 = [NSRegularExpression regularExpressionWithPattern:pattern
//                                                                                options:0
//                                                                                  error:nil];
//        modifiedString = [regex2 stringByReplacingMatchesInString:modifiedString
//                                                          options:0
//                                                            range:NSMakeRange(0, [modifiedString length])
//                                                     withTemplate:@""];
        
        pattern = @"\\[.*?\\]";
        NSRegularExpression *regex3 = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                                options:0
                                                                                  error:nil];
        modifiedString = [regex3 stringByReplacingMatchesInString:modifiedString
                                                          options:0
                                                            range:NSMakeRange(0, [modifiedString length])
                                                     withTemplate:@""];
        return modifiedString;
    }
}

- (NSString *)stringRemoveHTMLTags
{
	@autoreleasepool {
        NSString *pattern = @"<.*?>";
        NSRegularExpression *regex1 = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                                options:0
                                                                                  error:nil];
        NSString *modifiedString = [regex1 stringByReplacingMatchesInString:self
                                                                    options:0
                                                                      range:NSMakeRange(0, [self length])
                                                               withTemplate:@""];
        return modifiedString;
    }
}


@end
