//
//  AXEvnetsItemSelectViewController.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/9.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXEventItemSelectViewController.h"
#import "AXEventsFilterItemCell.h"

@interface AXEventItemSelectViewController()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)UITableView *contentTableView;
@end

@implementation AXEventItemSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor defaultTextColor]];
    [self.view addSubview:self.contentTableView]; 
}

#pragma mark - UITableViewDelegate & UITaleViewDataSource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataSource count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXEventsFilterItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AXEventsFilterItemCell" forIndexPath:indexPath];
    [cell.teamLabel setFont:[UIFont systemFontOfSize:16.0]];
    switch (_eventItemType) {
        case AXEventItemTypeEvent:
        {
            [cell.teamLabel setText:_dataSource[indexPath.row]];
        }
            break;
        case AXEventItemTypeTeam:
        {
            AXTeam *team = [_dataSource objectAtIndex:indexPath.row];
            [cell.teamLabel setText:team.name];
        }
            break;
        case AXEventItemTypePlayer:
        {
            AXFormation *formation = [_dataSource objectAtIndex:indexPath.row];
            [cell.teamLabel setText:[NSString stringWithFormat:@"%@｜%0.0f号",formation.name,formation.shirtNumber]];
        }
            break;
        case AXEventItemTypeStatistician:
        {
            OPFMatchStatisticians *mSta = [_dataSource objectAtIndex:indexPath.row];
            [cell.teamLabel setText:mSta.statistician.name];
        }
            break;
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (_eventItemType) {
        case AXEventItemTypeEvent:
        {
            [_event setCode:[[[[AXStaticDataHelper eventCodeStringDic] allKeysForObject:_dataSource[indexPath.row]] lastObject] doubleValue]];
            
            if (_event.player) {
                if ([AXStaticDataHelper matchEvent:[NSString stringWithFormat:@"%0.0f",_event.code]]) {
                    [_event setPlayer:nil];
                }
            }else{
                if (![AXStaticDataHelper matchEvent:[NSString stringWithFormat:@"%0.0f",_event.code]]) {
                    NSMutableArray<AXFormation*> *fArray = [NSMutableArray new];
                    for (AXFormation *formation in _formations) {
                        if ([_event.team.teamIdentifier isEqualToString:formation.teamId]) {
                            [fArray addObject:formation];
                        }
                    }
                    
                    if ([fArray count]>0) {
                        NSDictionary *dic = [[fArray objectAtIndex:0] dictionaryRepresentation];
                        [_event setPlayer:[AXPlayer modelObjectWithDictionary:dic]];
                        [_event setShirtNumber:[fArray objectAtIndex:0].shirtNumber];
                    }
                }
            }
            
        }
            break;
        case AXEventItemTypeTeam:
        {
            AXTeam *team = [_dataSource objectAtIndex:indexPath.row];
            [_event setTeam:team];
            
            NSMutableArray<AXFormation*> *fArray = [NSMutableArray new];
            for (AXFormation *formation in _formations) {
                if ([_event.team.teamIdentifier isEqualToString:formation.teamId]) {
                    [fArray addObject:formation];
                }
            }
            
            if ([fArray count]>0) {
                NSDictionary *dic = [[fArray objectAtIndex:0] dictionaryRepresentation];
                [_event setPlayer:[AXPlayer modelObjectWithDictionary:dic]];
                [_event setShirtNumber:[fArray objectAtIndex:0].shirtNumber];
            }else{
                [_event setPlayer:nil];
            }
            
            NSMutableArray<OPFMatchStatisticians*> *sArray = [NSMutableArray new];
            for (OPFMatchStatisticians *matchStatisticians in _matchStatisticians) {
                if ([_event.team.teamIdentifier isEqualToString:matchStatisticians.teamId]) {
                    [sArray addObject:matchStatisticians];
                }
            }
            
            if ([sArray count]>0) {
                [_event setStatistician:[sArray objectAtIndex:0].statistician];
            }else{
                [_event setStatistician:nil];
            }
            
        }
            break;
        case AXEventItemTypePlayer:
        {
            AXFormation *formation = [_dataSource objectAtIndex:indexPath.row];
            NSDictionary *dic = [formation dictionaryRepresentation];
            [_event setPlayer:[AXPlayer modelObjectWithDictionary:dic]];
            [_event setShirtNumber:formation.shirtNumber];
        }
            break;
        case AXEventItemTypeStatistician:
        {
            OPFMatchStatisticians *mSta = [_dataSource objectAtIndex:indexPath.row];
            [_event setStatistician:mSta.statistician];
        }
            break;
        default:
            break;
    }
    
    [self.navigationController popViewControllerAnimated:YES];

}

#pragma Mark - Getter

-(UITableView *)contentTableView
{
    if (!_contentTableView) {
        _contentTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height - 64) style:UITableViewStylePlain];
        [_contentTableView setDelegate:self];
        [_contentTableView setDataSource:self];
        [_contentTableView setBackgroundColor:[UIColor clearColor]];
        [_contentTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_contentTableView registerClass:[AXEventsFilterItemCell class] forCellReuseIdentifier:@"AXEventsFilterItemCell"];
    }
    
    return _contentTableView;
}

@end
