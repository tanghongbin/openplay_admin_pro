//
//  AXMasterTableViewCell.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXMasterTableViewCell.h"
#import "NSDate+OPBUtil.h"
#import "NSString+Extra.h"

@interface AXMasterTableViewCell()
@property (nonatomic, strong)UIView *seperateView;
@property (nonatomic, strong)UIView *containView;
@property (nonatomic, strong)UIView *seperateViewB;

@property (nonatomic, strong)UIImageView *teamALogoImageView;
@property (nonatomic, strong)UIImageView *teamBLogoImageView;
@property (nonatomic, strong)UILabel *teamANameLabel;
@property (nonatomic, strong)UILabel *teamBNameLabel;
@property (nonatomic, strong)UILabel *matchTimeLabel;
@property (nonatomic, strong)UIImageView *matchStyleImageView;
@property (nonatomic, strong)UILabel *matchStyleLabel;
@property (nonatomic, strong)UILabel *matchStatusLabel;
@property (nonatomic, strong)UILabel *matchPointLabel;
@property (nonatomic, strong)UIImageView *statisticsAIconImageView;
@property (nonatomic, strong)UIImageView *statisticsBIconImageView;
@property (nonatomic, strong)UIImageView *matchStatisticianAAvatarImageView;
@property (nonatomic, strong)UIImageView *matchStatisticianBAvatarImageView;
@property (nonatomic, strong)UILabel *matchStatisticianALabel;
@property (nonatomic, strong)UILabel *matchStatisticianBLabel;
@property (nonatomic, strong)UIImageView *venueIconImageView;
@property (nonatomic, strong)UILabel *venueLable;
@property (nonatomic, strong)UILabel *opIdLable;

@end

@implementation AXMasterTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.contentView addSubview:self.containView];
    [self.containView addSubview:self.matchStyleImageView];
    [self.containView addSubview:self.matchStyleLabel];
    [self.containView addSubview: self.matchTimeLabel];
    [self.containView addSubview:self.teamALogoImageView];
    [self.containView addSubview:self.teamBLogoImageView];
    [self.containView addSubview:self.teamANameLabel];
    [self.containView addSubview:self.teamBNameLabel];
    [self.containView addSubview:self.matchStatusLabel];
    [self.containView addSubview:self.matchPointLabel];
    [self.containView addSubview:self.seperateViewB];
    [self.containView addSubview:self.venueIconImageView];
    [self.containView addSubview:self.venueLable];
    [self.containView addSubview:self.opIdLable];
    self.match = _match;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        [self.containView.layer setBorderColor:[UIColor lightThemeColor].CGColor];
        [self.containView.layer setBorderWidth:3.5];
    }else{
        [self.containView.layer setBorderColor:[UIColor defaultBackGroundColor].CGColor];
        [self.containView.layer setBorderWidth:0.0];
    }
}

#pragma mark - Setter
-(void)setMatch:(AXMatch *)match
{
    _match = match;
    [_matchStyleLabel setText:[AXStaticDataHelper matchStyleStringWithKey:_match.matchStyle]];
    NSString *startTime = [NSString formatYearMonthDayHourMinuteSecond:[NSDate dateStringToDate:_match.startAt].timeIntervalSince1970];
    [_matchTimeLabel setText:[NSString stringWithFormat:@"开赛 %@",startTime]];
    [_teamALogoImageView sd_setImageWithURL:[NSURL URLWithString:_match.teamA.logoUri] placeholderImage:[UIImage imageNamed:@"Common_Team_Logo_80"]];
    [_teamBLogoImageView sd_setImageWithURL:[NSURL URLWithString:_match.teamB.logoUri] placeholderImage:[UIImage imageNamed:@"Common_Team_Logo_80"]];
    [_teamANameLabel setText:[_match.teamA.shortName length]==0?_match.teamA.name:_match.teamA.shortName];
    [_teamBNameLabel setText:[_match.teamB.shortName length]==0?_match.teamB.name:_match.teamB.shortName];
    [_matchStatusLabel setText:[AXStaticDataHelper matchStatusStringWithKey:_match.status]];
    if ([_match.status isEqualToString:@"Playing"]||[_match.status isEqualToString:@"Played"]) {
        [_matchPointLabel setHidden:NO];
        [_matchPointLabel setText:[NSString stringWithFormat:@"%0.f - %0.f",_match.fsA,_match.fsB]];
    }else{
        [_matchPointLabel setHidden:YES];
    }
    [_venueLable setText:_match.venue.stadium.name];
    [_opIdLable setText:_match.opId];
    
}
#pragma mark - Getter
-(UIView *)containView
{
    if (!_containView) {
        _containView = [[UIView alloc]initWithFrame:CGRectMake(5, 5, self.width - 10, self.height - 10)];
        [_containView setBackgroundColor:[UIColor defaultBackGroundColor]];
        [_containView addSubview:self.seperateView];
        [_containView setClipsToBounds:YES];
        [_containView.layer setCornerRadius:2.0];
    }
    return _containView;
}

-(UIView *)seperateView
{
    if (!_seperateView) {
        _seperateView = [[UIView alloc]initWithFrame:CGRectMake(0, _containView.height - 4, _containView.width, 4)];
        [_seperateView setBackgroundColor:UIColorWithRGB(0xf0f0f0)];
    }
     return _seperateView;
}

-(UIImageView *)matchStyleImageView
{
    if (!_matchStyleImageView) {
        _matchStyleImageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 8, 15, 15)];
        [_matchStyleImageView setContentMode:UIViewContentModeScaleAspectFit];
        [_matchStyleImageView setImage:[UIImage imageNamed:@"Common_Default_League_Logo_30"]];
    }
    return _matchStyleImageView;
}

-(UILabel *)matchStyleLabel
{
    if (!_matchStyleLabel) {
        _matchStyleLabel = [[UILabel alloc]initWithFrame:CGRectMake(_matchStyleImageView.width + _matchStyleImageView.x + 5, 8, 50, 15)];
        [_matchStyleLabel setTextAlignment:NSTextAlignmentLeft];
        [_matchStyleLabel setFont:[UIFont systemFontOfSize:12.0]];
        [_matchStyleLabel setTextColor:UIColorWithRGB_Alpha(0x999999, 1.0)];
    }
    return _matchStyleLabel;
}

-(UILabel *)matchTimeLabel
{
    if (!_matchTimeLabel) {
        _matchTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(_containView.width - 200 - 5, _matchStyleLabel.y, 200, 15)];
        [_matchTimeLabel setTextAlignment:NSTextAlignmentRight];
        [_matchTimeLabel setFont:[UIFont systemFontOfSize:12.0]];
        [_matchTimeLabel setTextColor:UIColorWithRGB_Alpha(0x999999, 1.0)];
    }
    return _matchTimeLabel;
}

-(UIImageView *)teamALogoImageView
{
    if (!_teamALogoImageView) {
        _teamALogoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(35, _matchStyleLabel.y+_matchStyleLabel.height + 10, 50, 50)];
        [_teamALogoImageView setContentMode:UIViewContentModeScaleAspectFit];
        [_teamALogoImageView setImage:[UIImage imageNamed:@"Common_Team_Logo_80"]];
    }
    return _teamALogoImageView;
}

-(UIImageView *)teamBLogoImageView
{
    if (!_teamBLogoImageView) {
        _teamBLogoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(_containView.width - 35 - 50, _matchStyleLabel.y+_matchStyleLabel.height + 10, 50, 50)];
        [_teamBLogoImageView setContentMode:UIViewContentModeScaleAspectFit];
        [_teamBLogoImageView setImage:[UIImage imageNamed:@"Common_Team_Logo_80"]];
    }
    return _teamBLogoImageView;
}

-(UILabel *)teamANameLabel
{
    if (!_teamANameLabel) {
        _teamANameLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, _teamALogoImageView.y + _teamALogoImageView.height + 5, 110, 20)];
        [_teamANameLabel setTextAlignment:NSTextAlignmentCenter];
        [_teamANameLabel setFont:[UIFont systemFontOfSize:16.0]];
        [_teamANameLabel setTextColor:[UIColor defaultTextColor]];
    }
    return _teamANameLabel;
}

-(UILabel *)teamBNameLabel
{
    if (!_teamBNameLabel) {
        _teamBNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(_containView.width - 5 - 110, _teamALogoImageView.y + _teamALogoImageView.height + 5, 110, 20)];
        [_teamBNameLabel setTextAlignment:NSTextAlignmentCenter];
        [_teamBNameLabel setFont:[UIFont systemFontOfSize:16.0]];
        [_teamBNameLabel setTextColor:[UIColor defaultTextColor]];
    }
    return _teamBNameLabel;
}

-(UILabel *)matchStatusLabel
{
    if (!_matchStatusLabel) {
        _matchStatusLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, _teamALogoImageView.y + 5, 50, 20)];
        [_matchStatusLabel setCenterX:_containView.width/2];
        [_matchStatusLabel setTextAlignment:NSTextAlignmentCenter];
        [_matchStatusLabel setFont:[UIFont systemFontOfSize:14.0]];
        [_matchStatusLabel setTextColor:[UIColor defaultTextColor]];
        [_matchStatusLabel setText:@"进行中"];
    }
    return _matchStatusLabel;
}

-(UILabel *)matchPointLabel
{
    if (!_matchPointLabel) {
        _matchPointLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, _matchStatusLabel.y + _matchStatusLabel.height + 5, 80, 25)];
        [_matchPointLabel setCenterX:_containView.width/2];
        [_matchPointLabel setTextAlignment:NSTextAlignmentCenter];
        [_matchPointLabel setFont:[UIFont boldSystemFontOfSize:22.0]];
        [_matchPointLabel setTextColor:[UIColor defaultTextColor]];
        [_matchPointLabel setText:@"10 - 10"];
    }
    return _matchPointLabel;
}

-(UIView *)seperateViewB
{
    if (!_seperateViewB) {
        _seperateViewB = [[UIView alloc]initWithFrame:CGRectMake(5, _teamANameLabel.y + _teamANameLabel.height + 10, _containView.width - 10, 0.5)];
        [_seperateViewB setBackgroundColor:UIColorWithRGB(0xf0f0f0)];
    }
    return _seperateViewB;
}

-(UIImageView *)venueIconImageView
{
    if (!_venueIconImageView) {
        _venueIconImageView = [[UIImageView  alloc]initWithFrame:CGRectMake(15, _seperateViewB.y + _seperateViewB.height + 5, 13, 13)];
        [_venueIconImageView setContentMode:UIViewContentModeScaleAspectFit];
        [_venueIconImageView setImage:[UIImage imageNamed:@"Common_Icon_Location_1"]];
    }
    return _venueIconImageView;
}

-(UILabel *)venueLable
{
    if (!_venueLable) {
        _venueLable = [[UILabel alloc]initWithFrame:CGRectMake(_venueIconImageView.x+_venueIconImageView.width + 5, _venueIconImageView.y, self.width/2 - 5 - _venueIconImageView.x + _venueIconImageView.width - 5, 13)];
        [_venueLable setTextAlignment:NSTextAlignmentLeft];
        [_venueLable setFont:[UIFont systemFontOfSize:14.0]];
        [_venueLable setTextColor:UIColorWithRGB_Alpha(0x999999, 1.0)];
    }
    return _venueLable;
}

-(UILabel *)opIdLable
{
    if (!_opIdLable) {
        _opIdLable = [[UILabel alloc]initWithFrame:CGRectMake(_venueLable.x+_venueLable.width + 5, _venueLable.y, self.width/2 - 40, 13)];
        [_opIdLable setTextAlignment:NSTextAlignmentRight];
        [_opIdLable setFont:[UIFont boldSystemFontOfSize:15.0]];
        [_opIdLable setTextColor:[UIColor defaultTextColor]];
    }
    return _opIdLable;
}

@end
