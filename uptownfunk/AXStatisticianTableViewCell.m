//
//  AXStatisticianTableViewCell.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXStatisticianTableViewCell.h"

@interface AXStatisticianTableViewCell()
@property (nonatomic, strong)UIView *seperateView;
@property (nonatomic, strong)UIView *containView;

@property (nonatomic, strong)UILabel *nameLabel;
@property (nonatomic, strong)UIButton *deleteButton;

@end
@implementation AXStatisticianTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setFrame:CGRectMake(0, 0, iPad?SCREEN_HEIGHT - 320:SCREEN_WIDTH, 40)];
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
//        UIView *selectedView = [[UIView alloc]init];
//        [selectedView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.65]];
        self.selectedBackgroundView = nil;
        [self.contentView addSubview:self.containView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setMatchStatistician:(OPFMatchStatisticians *)matchStatistician
{
    _matchStatistician = matchStatistician;
    [_nameLabel setText:_matchStatistician.statistician.name];
}

#pragma mark - Getter
-(UIView *)containView
{
    if (!_containView) {
        _containView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.width - 0, self.height - 0)];
        [_containView setBackgroundColor:[UIColor defaultTextColor]];
        [_containView addSubview:self.seperateView];
        [_containView addSubview:self.nameLabel];
        [_containView addSubview:self.deleteButton];
    }
    return _containView;
}

-(UIView *)seperateView
{
    if (!_seperateView) {
        _seperateView = [[UIView alloc]initWithFrame:CGRectMake(0, _containView.height - 1, _containView.width, 1)];
        [_seperateView setBackgroundColor:UIColorWithRGB(0x4a4a4a)];
    }
    return _seperateView;
}

-(UILabel *)nameLabel
{
    if (!_nameLabel) {
        _nameLabel = [self headerLabelWithFrame:CGRectMake(10, 0, 200, _containView.height) Title:@"统计员"];
    }
    return _nameLabel;
}

-(UIButton *)deleteButton
{
    if (!_deleteButton) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton setFrame:CGRectMake(self.width - 60 - 10, 8, 60, 24)];
        [_deleteButton setTitle:@"删除" forState:UIControlStateNormal];
        [_deleteButton.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [_deleteButton setBackgroundImage:[UIImage imageFromColor:UIColorWithRGB_Alpha(0xcc0001, 1.0)] forState:UIControlStateNormal];
        [_deleteButton setBackgroundImage:[UIImage imageFromColor:UIColorWithRGB_Alpha(0xcc0001, 0.65)] forState:UIControlStateHighlighted];
        [_deleteButton.layer setCornerRadius:2];
        [_deleteButton.layer setMasksToBounds:YES];
        [_deleteButton bk_addEventHandler:^(id sender) {
            if ([_delegate respondsToSelector:@selector(statisticianTableViewCell:didDelete:)]) {
                [_delegate statisticianTableViewCell:self didDelete:_matchStatistician];
            }
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteButton;
}


-(UILabel*)headerLabelWithFrame:(CGRect)frame Title:(NSString*)title
{
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:frame];
    [headerLabel setTextColor:[UIColor defaultBackGroundColor]];
    [headerLabel setTextAlignment:NSTextAlignmentLeft];
    [headerLabel setFont:[UIFont systemFontOfSize:12]];
    [headerLabel setText:title];
    return headerLabel;
}

@end
