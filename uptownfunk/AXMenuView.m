//
//  AXMenuView.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/11.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXMenuView.h"
#import "JWBlurView.h"
@interface AXMenuView()
@property (nonatomic, strong)UIView *contentView;
@property (nonatomic, strong)UIImageView *backgroundImageView;
@property (nonatomic, strong)UIButton *addTestMatchButton;
@property (nonatomic, strong)UIButton *backgroundButton;

@property (nonatomic, strong)UIWindow *window;
@property (nonatomic, strong)UIView *onView;

@property (nonatomic, assign)BOOL isOpened;
@end
@implementation AXMenuView

- (instancetype)initOnView:(UIView*)onView
{
    self = [super initWithFrame:CGRectMake(0, 64, onView.width, onView.height)];
    if (self) {
        self.window = [UIApplication sharedApplication].keyWindow;
        [self addSubview:self.backgroundButton];
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.0]];
        [self addSubview:self.contentView];
        [self setClipsToBounds:YES];
    }
    return self;
}

-(void)trigger
{
    if (_isOpened) {
        [UIView animateWithDuration:0.3 animations:^{
            [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.0]];
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
        [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.7 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [_contentView setY:-150];
        } completion:^(BOOL finished) {
            
        }];
        _isOpened = NO;
    }else
    {
        [_window addSubview:self];
        [UIView animateWithDuration:0.3 animations:^{
            [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.86]];
        }];
        
        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.7 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [_contentView setY:0];
        } completion:^(BOOL finished) {
            
        }];
        _isOpened = YES;
    }
}

#pragma makr - Getter

-(UIImageView *)backgroundImageView
{
    if (!_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"opf_menu_bg"]];
        [_backgroundImageView setFrame:CGRectMake(0, 0, _contentView.width, _contentView.height)];
    }
    return _backgroundImageView;
}

-(UIView *)contentView
{
    if(!_contentView)
    {
        _contentView = [[UIView alloc]initWithFrame:CGRectMake(self.width - 110, -300, 100, 150)];
        [_contentView setClipsToBounds:YES];
        [_contentView addSubview:self.backgroundImageView];
        [_contentView addSubview:self.addTestMatchButton];
        [_contentView addSubview:[self seperatorView]];
    }
    return _contentView;
}

-(UIButton *)addTestMatchButton{
    if (!_addTestMatchButton) {
        _addTestMatchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _addTestMatchButton.tag = AXMenuButtonTypeAddTestMatch;
        [_addTestMatchButton setFrame:CGRectMake(0, 5, _contentView.width, 40)];
        [_addTestMatchButton setTitle:@"🤑新建测试赛" forState:UIControlStateNormal];
        [_addTestMatchButton.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [_addTestMatchButton setBackgroundImage:[UIImage imageFromColor:[[UIColor blackColor] colorWithAlphaComponent:0.86]] forState:UIControlStateHighlighted];
        [_addTestMatchButton bk_addEventHandler:^(UIButton* sender) {
            [self trigger];
            
            if ([_delegate respondsToSelector:@selector(menuView:selectedIndex:)]) {
                [_delegate menuView:self selectedIndex:sender.tag];
            }
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _addTestMatchButton;
}

-(UIButton *)backgroundButton
{
    if (!_backgroundButton) {
        _backgroundButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backgroundButton setFrame:CGRectMake(0, 0, self.width, self.height)];
        [_backgroundButton setBackgroundColor:[UIColor clearColor]];
        [_backgroundButton setTitle:@"🙄️取戳" forState:UIControlStateNormal];
        [_backgroundButton bk_addEventHandler:^(id sender) {
            [self trigger];
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _backgroundButton;
}


-(UIView*)seperatorView
{
    UIView *sView = [[UIView alloc]initWithFrame:CGRectMake(10, _addTestMatchButton.y+_addTestMatchButton.height, _addTestMatchButton.width - 20, 0.7)];
    [sView setBackgroundColor:[UIColor blackColor]];
    return sView;
}

@end
