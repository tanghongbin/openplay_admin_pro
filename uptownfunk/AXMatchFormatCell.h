//
//  AXMatchFormatCell.h
//  uptownfunk
//
//  Created by yellow on 16/4/20.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AXMatchFormatCell;
@protocol AXMatchFormatCellDelegate <NSObject>

- (BOOL)matchFormatCellShouldBeginEditing:(AXMatchFormatCell *)cell;
- (void)matchFormatCellValueChange:(AXMatchFormatCell *)cell;
- (void)matchFormatCellDone:(AXMatchFormatCell *)cell;

@end

@interface AXMatchFormatCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UITextField *numberTextField;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet id<AXMatchFormatCellDelegate> delegate;

@end
