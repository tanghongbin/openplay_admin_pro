//
//  AXDataHelper.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/19.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXStaticDataHelper.h"

@implementation AXStaticDataHelper
+(NSString *)roleStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"super_administrator" : @"超级管理员",
                          @"general_administrator" : @"管理员",
                          @"administrator" : @"管理员",
                          @"chairman" : @"组委会主席",
                          @"committee" : @"组委会成员",
                          @"player" : @"球员",
                          @"team_manager" : @"领队",
                          @"statistician" : @"统计员",
                          @"statistician_to_be" : @"见习统计员",
                          @"organizer" : @"组织者"
                          };
    return dic[key];
}

+(NSString *)statisticianPositionStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"statistician" : @"主力",
                          @"statistician_to_be" : @"候补",
                          @"statistician_test" : @"测试"};
    return dic[key];
}

+(NSString *)statisticianRoleStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"statistician" : @"统计员",
                          @"statistician_to_be" : @"见习统计员",
                          @"statistician_test" : @"测试统计员",
                          @"key_account_manager" : @"关键统计员",
                          @"city_manager" : @"城市经理"};
    return dic[key];
}

+(NSString *)statusStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"1" : @"已激活",
                          @"0" : @"未激活"};
    return dic[key];
}

+(NSString *)approvalStatusStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"0" : @"未审核",
                          @"8" : @"未审核",
                          @"1" : @"已通过",
                          @"4" : @"已驳回"};
    return dic[key];
}

+(NSString *)featuresStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"0" : @"未上线",
                          @"1" : @"已上线"};
    return dic[key];
}

+(NSString *)genderStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"1" : @"男",
                          @"0" : @"女"};
    return dic[key];
}

+(NSString *)periodStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"Playing" : @"进行中",
                          @"Played" : @"已结束",
                          @"Fixture" : @"准备中"};
    return dic[key];
}

+(NSString *)matchStyleStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"free" : @"自由比赛",
                          @"test" : @"测试赛",
                          @"series" : @"系列赛"};
    return dic[key];
}

+(NSString *)matchStatusStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"Playing" : @"进行中",
                          @"Played" : @"已结束",
                          @"Fixture" : @"未开始",
                          @"Postponed" : @"已延迟",
                          @"Abandoned" : @"已放弃",
                          @"Cancelled" : @"已取消"};
    return dic[key];
}

+(NSString *)matchTypeStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"5" : @"五人制",
                          @"7" : @"七人制",
                          @"11" : @"十一人制"};
    return dic[key];
}

+(NSInteger )matchTypeWithString:(NSString *)key
{
    NSDictionary *dic = @{@"五人制" : @5,
                          @"七人制" : @7,
                          @"十一人制" : @11};
    return [dic[key] integerValue];
}

+(NSString *)statsModeStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"0" : @"基本模式",
                          @"1" : @"进阶模式",
                          @"2" : @"专业模式"};
    return dic[key];
}

+(NSString *)stoppedWatchStringWithKey:(NSString *)key
{
    NSDictionary *dic = @{@"0" : @"不停表",
                          @"1" : @"停表"};
    return dic[key];
}

+(NSInteger)stoppedWatchValueWithString:(NSString *)key
{
    NSDictionary *dic = @{@"不停表" : @0,
                          @"停表" : @1};
    return [dic[key] integerValue];
}

+(NSString *)eventCodeForStringWithKey:(NSString *)key
{
    
    return [self eventCodeStringDic][key]?[self eventCodeStringDic][key]:key;
}

+(NSDictionary *)eventCodeStringDic
{
    return  @{
              @"11" : @"射正",
              @"12" : @"射偏",
              @"13" : @"被封堵",
              @"14" : @"射中门框",
              @"21" : @"传球成功",
              @"22" : @"传球失败",
              @"33" : @"抢断成功",
              @"34" : @"抢断失败",
              @"41" : @"拦截",
              @"42" : @"解围",
              @"43" : @"封堵",
              @"51" : @"扑救",
              @"71" : @"进球",
              @"72" : @"乌龙球",
              @"73" : @"助攻",
              @"82" : @"黄牌",
              @"83" : @"红牌",
              @"91" : @"控球",
              @"100" : @"比赛开始",
              @"101" : @"上半场开始",
              @"102" : @"上半场结束",
              @"103" : @"下半场开始",
              @"104" : @"下半场结束",
              @"105" : @"加时赛上半场开始",
              @"106" : @"加时赛上半场结束",
              @"107" : @"加时赛下半场开始",
              @"108" : @"加时赛下半场结束",
              @"109" : @"点球大战开始",
              @"110" : @"点球大战结束",
              @"111" : @"比赛结束",
              @"114" : @"射正 (点球)",
              @"120" : @"暂停",
              @"121" : @"继续",
              @"124" : @"射偏 (点球)",
              @"144" : @"射中门框 (点球)",
              @"200" : @"球员上场",
              @"201" : @"球员下场",
              @"311" : @"点球大战射正",
              @"312" : @"点球大战射偏",
              @"314" : @"点球大战射中门框",
              @"351" : @"点球大战扑救",
              @"371" : @"点球大战进球",
              @"514" : @"扑救 (点球)",
              @"714" : @"进球 (点球)"
              };
}

+(NSArray<NSString*>*)eventStringArray
{
    return  @[
              @"射正",
              @"射偏",
              @"被封堵",
              @"射中门框",
              @"传球成功",
              @"传球失败",
              @"抢断成功",
              @"抢断失败",
              @"拦截",
              @"解围",
              @"封堵",
              @"扑救",
              @"进球",
              @"乌龙球",
              @"助攻",
              @"黄牌",
              @"红牌",
              @"控球",
              @"比赛开始",
              @"上半场开始",
              @"上半场结束",
              @"下半场开始",
              @"下半场结束",
              @"加时赛上半场开始",
              @"加时赛上半场结束",
              @"加时赛下半场开始",
              @"加时赛下半场结束",
              @"点球大战开始",
              @"点球大战结束",
              @"比赛结束",
              @"射正 (点球)",
              @"暂停",
              @"继续",
              @"射偏 (点球)",
              @"射中门框 (点球)",
              @"球员上场",
              @"球员下场",
              @"点球大战射正",
              @"点球大战射偏",
              @"点球大战射中门框",
              @"点球大战扑救",
              @"点球大战进球",
              @"扑救 (点球)",
              @"进球 (点球)"
              ];
    
}

+(NSArray *)eventQueriesCodeArray
{
    return  @[
              @{@"code:00" : @"所有事件"},
              @{@"code:71+OR+code:714+OR+code:72+OR+code:73+OR+code:82+OR+code:83" : @"关键数据"},
              @{@"code:11+OR+code:114+OR+code:12+OR+code:124+OR+code:13+OR+code:14+OR+code:144+OR+code:71+OR+code:714+OR+code:51+OR+code:514" : @"射门"},
              @{@"code:100+OR+code:101+OR+code:102+OR+code:103+OR+code:104+OR+code:105+OR+code:106+OR+code:107+OR+code:108+OR+code:109+OR+code:110+OR+code:111+OR+code:120+OR+code:121" : @"比赛阶段"},
              @{@"code:200+OR+code:201+OR+code:100+OR+code:101+OR+code:102+OR+code:103+OR+code:104+OR+code:105+OR+code:106+OR+code:107+OR+code:108+OR+code:109+OR+code:110+OR+code:111" : @"球员上下场"},
              @{@"code:11" : @"射正"},
              @{@"code:12" : @"射偏"},
              @{@"code:13" : @"被封堵"},
              @{@"code:14" : @"射中门框"},
              @{@"code:21" : @"传球成功"},
              @{@"code:22" : @"传球失败"},
              @{@"code:33" : @"抢断成功"},
              @{@"code:34" : @"抢断失败"},
              @{@"code:41" : @"拦截"},
              @{@"code:42" : @"解围"},
              @{@"code:43" : @"封堵"},
              @{@"code:51" : @"扑救"},
              @{@"code:71" : @"进球"},
              @{@"code:72" : @"乌龙球"},
              @{@"code:73" : @"助攻"},
              @{@"code:82" : @"黄牌"},
              @{@"code:83" : @"红牌"},
              @{@"code:91" : @"控球"},
              @{@"code:100" : @"比赛开始"},
              @{@"code:101" : @"上半场开始"},
              @{@"code:102" : @"上半场结束"},
              @{@"code:103" : @"下半场开始"},
              @{@"code:104" : @"下半场结束"},
              @{@"code:105" : @"加时赛上半场开始"},
              @{@"code:106" : @"加时赛上半场结束"},
              @{@"code:107" : @"加时赛下半场开始"},
              @{@"code:108" : @"加时赛下半场结束"},
              @{@"code:109" : @"点球大战开始"},
              @{@"code:110" : @"点球大战结束"},
              @{@"code:111" : @"比赛结束"},
              @{@"code:114" : @"射正 (点球)"},
              @{@"code:120" : @"暂停"},
              @{@"code:121" : @"继续"},
              @{@"code:124" : @"射偏 (点球)"},
              @{@"code:144" : @"射中门框 (点球)"},
              @{@"code:200" : @"球员上场"},
              @{@"code:201" : @"球员下场"},
              @{@"code:311" : @"点球大战射正"},
              @{@"code:312" : @"点球大战射偏"},
              @{@"code:314" : @"点球大战射中门框"},
              @{@"code:351" : @"点球大战扑救"},
              @{@"code:371" : @"点球大战进球"},
              @{@"code:514" : @"扑救 (点球)"},
              @{@"code:714" : @"进球 (点球)"}
              ];
}

+(NSString *)operationStringWithKey:(NSInteger)k
{
    NSString *key = [NSString stringWithFormat:@"%ld",(long)k];
    NSDictionary *dic = @{@"0" : @"统计员们",
                          @"1" : @"阵容设置",
                          @"2" : @"比赛事件"};
    return dic[key]?dic[key]:key;
}

+(NSArray<NSString*> *)eventItemTitleArray
{
    return @[@"事件",
             @"球队",
             @"球员",
             @"统计员",
             @"日期",
             @"时间"];
}

+(NSString*)matchEvent:(NSString*)key
{
    return @{
             @"100" : @"比赛开始",
             @"101" : @"上半场开始",
             @"102" : @"上半场结束",
             @"103" : @"下半场开始",
             @"104" : @"下半场结束",
             @"105" : @"加时赛上半场开始",
             @"106" : @"加时赛上半场结束",
             @"107" : @"加时赛下半场开始",
             @"108" : @"加时赛下半场结束",
             @"109" : @"点球大战开始",
             @"110" : @"点球大战结束",
             @"111" : @"比赛结束",
             @"120" : @"暂停",
             @"121" : @"继续",}[key];
}
@end
