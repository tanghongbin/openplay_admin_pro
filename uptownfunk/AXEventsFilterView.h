//
//  AXEventsFilterView.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/27.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXQueries.h"

@class AXEventsFilterView;
@protocol AXEventsFilterViewDelegate <NSObject>

@required
-(void)filterView:(AXEventsFilterView*)filterView didSelectedWithFilterItemArray:(NSArray*)fArray;
@end

@interface AXEventsFilterView : UIView
@property (nonatomic, strong)AXQueries *queries;
@property (nonatomic, assign)id<AXEventsFilterViewDelegate>delegate;
@end
