//
//  AXEventsFilterView.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/27.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXEventsFilterView.h"
#import "AXEventsFilterItemView.h"
#import "AXEventsFilterItemCell.h"
#import "AXByTeams.h"
#import "AXByPlayers.h"
#import "AXByStatisticians.h"
#import "JWBlurView.h"
#import "AXStaticDataHelper.h"
#import <objc/runtime.h>

@interface AXEventsFilterView()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)AXEventsFilterItemView *eventsFilterView;
@property (nonatomic, strong)AXEventsFilterItemView *teamFilterView;
@property (nonatomic, strong)AXEventsFilterItemView *playerFilterView;
@property (nonatomic, strong)AXEventsFilterItemView *statisticianFilterView;
@property (nonatomic, strong)AXEventsFilterItemView *currentOpenedFilterView;
@property (nonatomic, strong)NSMutableArray<AXEventsFilterItemView*> *filterViewArrray;
@property (nonatomic, strong)UITableView *filterItemTableView;
@property (nonatomic, strong)NSMutableArray *currentfilterItemArray;
@property (nonatomic, strong)NSMutableArray *selectedfilterItemArray;
@end
@implementation AXEventsFilterView
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.0]];
        [self addSubview:self.eventsFilterView];
        [self addSubview:self.teamFilterView];
        [self addSubview:self.playerFilterView];
        [self addSubview:self.statisticianFilterView];
        [self addSubview:self.filterItemTableView];
        [self seperatorView];
        
    }
    return self;
}

-(void)setQueries:(AXQueries *)queries
{
    _queries = queries;
}

#pragma mark - method
-(void)selectedAction:(AXEventsFilterItemView*)sender
{
    self.currentOpenedFilterView = sender;
    [_filterViewArrray enumerateObjectsUsingBlock:^(AXEventsFilterItemView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![obj isEqual:sender]) {
            [obj setIsOpened:NO];
        }
    }];
    
    if ([sender isEqual:_eventsFilterView]) {
        self.currentfilterItemArray = [[AXStaticDataHelper eventQueriesCodeArray] mutableCopy];
    }else if ([sender isEqual:_teamFilterView])
    {
        self.currentfilterItemArray = [_queries.byTeams mutableCopy];
        [self.currentfilterItemArray insertObject:[AXByTeams modelObjectWithDictionary:@{@"label":@"所有球队"}] atIndex:0];
        
    }else if ([sender isEqual:_playerFilterView])
    {
        self.currentfilterItemArray = [_queries.byPlayers mutableCopy];
        [self.currentfilterItemArray insertObject:[AXByPlayers modelObjectWithDictionary:@{@"name":@"所有球员"}] atIndex:0];
    }else if ([sender isEqual:_statisticianFilterView])
    {
        self.currentfilterItemArray = [_queries.byStatisticians mutableCopy];
        [self.currentfilterItemArray insertObject:[AXByStatisticians modelObjectWithDictionary:@{@"name":@"所有统计员"}] atIndex:0];
        [self.currentfilterItemArray insertObject:[AXByStatisticians modelObjectWithDictionary:@{@"name":@"主力统计员"}] atIndex:1];
    }
    
    [self.filterItemTableView reloadData];
    if (sender.isOpened) {
        [self setHeight:self.superview.height];
        [UIView animateWithDuration:0.25 animations:^{
            [_filterItemTableView setHeight:500];
        }];
        [UIView animateWithDuration:0.2 animations:^{
            [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.85]];
        }];
    }else{
        [UIView animateWithDuration:0.25 animations:^{
            [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.0]];
        }];
        
        [UIView animateWithDuration:0.2 animations:^{
            [_filterItemTableView setHeight:0.0];
        } completion:^(BOOL finished) {
            [self setHeight:40];
        }];
    }
}

#pragma mark - UITableViewDelegate && UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_currentfilterItemArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXEventsFilterItemCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AXEventsFilterItemCell class]) forIndexPath:indexPath];
    if ([_currentOpenedFilterView isEqual:_eventsFilterView]) {
        NSDictionary *dic = _currentfilterItemArray[indexPath.row];
        dic?[cell.teamLabel setText:[dic allValues][0]]:nil;
    }else if ([_currentOpenedFilterView isEqual:_teamFilterView])
    {
        AXByTeams *byTeam = _currentfilterItemArray[indexPath.row];
        [cell.teamLabel setText:byTeam.label];
    }else if ([_currentOpenedFilterView isEqual:_playerFilterView])
    {
        AXByPlayers *byPlayer = _currentfilterItemArray[indexPath.row];
        if (indexPath.row==0) {
            [cell.teamLabel setText:byPlayer.name];
        }else{
            [cell.teamLabel setText:[NSString stringWithFormat:@"%@(%.0f)｜%@",byPlayer.name,byPlayer.shirtNumber,byPlayer.teamLabel]];
        }
        
    }else if ([_currentOpenedFilterView isEqual:_statisticianFilterView])
    {
        AXByStatisticians *byAXByStatisticians = _currentfilterItemArray[indexPath.row];
        [cell.teamLabel setText:byAXByStatisticians.name];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([_currentOpenedFilterView isEqual:_eventsFilterView]) {
        NSDictionary<NSString*,NSString*> *dic = _currentfilterItemArray[indexPath.row];
        dic?[_currentOpenedFilterView setItemTitle:[dic allValues][0]]:nil;
        if (![[dic allValues][0] isEqualToString:@"所有事件"]) {
            objc_setAssociatedObject(_currentOpenedFilterView, @"event", [dic allKeys][0], OBJC_ASSOCIATION_COPY_NONATOMIC);
        }else{
            objc_removeAssociatedObjects(_currentOpenedFilterView);
        }
    }else if ([_currentOpenedFilterView isEqual:_teamFilterView])
    {
        AXByTeams *byTeam = _currentfilterItemArray[indexPath.row];
        [_currentOpenedFilterView setItemTitle:byTeam.label];
        
        if (![byTeam.label isEqualToString:@"所有球队"]) {
            objc_setAssociatedObject(_currentOpenedFilterView, @"team",[NSString stringWithFormat:@"team_id:%@",byTeam.value], OBJC_ASSOCIATION_COPY_NONATOMIC);
        }else{
            objc_removeAssociatedObjects(_currentOpenedFilterView);
        }
        
    }else if ([_currentOpenedFilterView isEqual:_playerFilterView])
    {
        AXByPlayers *byPlayer = _currentfilterItemArray[indexPath.row];
        [_currentOpenedFilterView setItemTitle:byPlayer.name];
        
        if (![byPlayer.name isEqualToString:@"所有球员"]) {
            objc_setAssociatedObject(_currentOpenedFilterView, @"player", [NSString stringWithFormat:@"player_id:%@",byPlayer.byPlayersIdentifier], OBJC_ASSOCIATION_COPY_NONATOMIC);
        }else{
            objc_removeAssociatedObjects(_currentOpenedFilterView);
        }
        
    }else if ([_currentOpenedFilterView isEqual:_statisticianFilterView])
    {
        AXByStatisticians *byStatisticians = _currentfilterItemArray[indexPath.row];
        [_currentOpenedFilterView setItemTitle:byStatisticians.name];
        
        if (![byStatisticians.name isEqualToString:@"所有统计员"]) {
            objc_setAssociatedObject(_currentOpenedFilterView, @"statistician", [NSString stringWithFormat:@"statistician_id:%@",byStatisticians.byStatisticiansIdentifier], OBJC_ASSOCIATION_COPY_NONATOMIC);
        }else if ([byStatisticians.name isEqualToString:@"主力统计员"])
        {
            objc_setAssociatedObject(_currentOpenedFilterView, @"statistician", @"role:statistician", OBJC_ASSOCIATION_COPY_NONATOMIC);
        }
        else{
            objc_removeAssociatedObjects(_currentOpenedFilterView);
        }
        
    }
    [_currentOpenedFilterView setIsOpened:NO];
    [self selectedAction:_currentOpenedFilterView];
    
    if ([_delegate respondsToSelector:@selector(filterView:didSelectedWithFilterItemArray:)]) {
        NSMutableArray *filterItemArray = [NSMutableArray new];
        if (objc_getAssociatedObject(_eventsFilterView, @"event")) {
            [filterItemArray addObject:objc_getAssociatedObject(_eventsFilterView, @"event")];
        }
        if (objc_getAssociatedObject(_teamFilterView, @"team")) {
            [filterItemArray addObject:objc_getAssociatedObject(_teamFilterView, @"team")];
        }
        if (objc_getAssociatedObject(_playerFilterView, @"player")) {
            [filterItemArray addObject:objc_getAssociatedObject(_playerFilterView, @"player")];
        }
        if (objc_getAssociatedObject(_statisticianFilterView, @"statistician")) {
            [filterItemArray addObject:objc_getAssociatedObject(_statisticianFilterView, @"statistician")];
        }
        
        [_delegate filterView:self didSelectedWithFilterItemArray:filterItemArray];
    }
}

#pragma mark - SubViews
-(void)seperatorView
{
    UIView *sv_1 = [self seperateView_V];
    [sv_1 setCenterX:self.width/4 * 1];
    UIView *sv_2 = [self seperateView_V];
    [sv_2 setCenterX:self.width/4 * 2];
    UIView *sv_3 = [self seperateView_V];
    [sv_3 setCenterX:self.width/4 * 3];
    
    [self addSubview:sv_1];
    [self addSubview:sv_2];
    [self addSubview:sv_3];
}


#pragma mark - Getter
-(AXEventsFilterItemView *)eventsFilterView
{
    if (!_eventsFilterView) {
        _eventsFilterView = [[AXEventsFilterItemView alloc]initWithFrame:CGRectMake(0, 0, self.width/4, 40) Selected:^(AXEventsFilterItemView *item) {
            [self selectedAction:item];
        }];
        [_eventsFilterView setCenter:CGPointMake(self.width/8, self.height/2)];
        [_eventsFilterView setItemTitle:@"所有事件"];
        [self.filterViewArrray addObject:_eventsFilterView];
    }
    return _eventsFilterView;
}

-(AXEventsFilterItemView *)teamFilterView
{
    if (!_teamFilterView) {
        _teamFilterView = [[AXEventsFilterItemView alloc]initWithFrame:CGRectMake(0, 0, self.width/4, 40) Selected:^(AXEventsFilterItemView *item) {
            [self selectedAction:item];
        }];
        [_teamFilterView setCenter:CGPointMake(self.width/8 * 3, self.height/2)];
        [_teamFilterView setItemTitle:@"所有球队"];
        [self.filterViewArrray addObject:_teamFilterView];
    }
    return _teamFilterView;
}

-(AXEventsFilterItemView *)playerFilterView
{
    if (!_playerFilterView) {
        _playerFilterView = [[AXEventsFilterItemView alloc]initWithFrame:CGRectMake(0, 0, self.width/4, 40) Selected:^(AXEventsFilterItemView *item) {
            [self selectedAction:item];
        }];
        [_playerFilterView setCenter:CGPointMake(self.width/8 * 5, self.height/2)];
        [_playerFilterView setItemTitle:@"所有球员"];
        [self.filterViewArrray addObject:_playerFilterView];
    }
    return _playerFilterView;
}

-(AXEventsFilterItemView *)statisticianFilterView
{
    if (!_statisticianFilterView) {
        _statisticianFilterView = [[AXEventsFilterItemView alloc]initWithFrame:CGRectMake(0, 0, self.width/4, 40) Selected:^(AXEventsFilterItemView *item) {
            [self selectedAction:item];
        }];
        [_statisticianFilterView setCenter:CGPointMake(self.width/8 * 7, self.height/2)];
        [_statisticianFilterView setItemTitle:@"所有统计员"];
        [self.filterViewArrray addObject:_statisticianFilterView];
    }
    return _statisticianFilterView;
}

-(UIView *)seperateView_V
{
    UIView *sv = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 30)];
    [sv setCenterY:self.height/2];
    [sv setBackgroundColor:UIColorWithRGB(0x4a4a4a)];
    return sv;
}

-(NSMutableArray *)filterViewArrray
{
    if (!_filterViewArrray) {
        _filterViewArrray = [[NSMutableArray alloc]initWithCapacity:4];
    }
    return _filterViewArrray;
}

-(UITableView *)filterItemTableView
{
    if (!_filterItemTableView) {
        _filterItemTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, self.width, 0) style:UITableViewStylePlain];
        [_filterItemTableView setBackgroundColor:[UIColor clearColor]];
        [_filterItemTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _filterItemTableView.delegate = self;
        _filterItemTableView.dataSource = self;
        [_filterItemTableView registerClass:[AXEventsFilterItemCell class] forCellReuseIdentifier:@"AXEventsFilterItemCell"];
    }
    return _filterItemTableView;
}

-(NSMutableArray *)currentfilterItemArray
{
    if (!_currentfilterItemArray) {
        _currentfilterItemArray = [[NSMutableArray alloc] init];
    }
    return _currentfilterItemArray;
}

-(NSMutableArray *)selectedfilterItemArray
{
    if (!_selectedfilterItemArray) {
        _selectedfilterItemArray = [[NSMutableArray alloc] init];
    }
    return _selectedfilterItemArray;
}

-(AXEventsFilterItemView *)currentOpenedFilterView
{
    if (!_currentOpenedFilterView) {
        _currentOpenedFilterView = [AXEventsFilterItemView new];
    }
    return _currentOpenedFilterView;
}

@end
