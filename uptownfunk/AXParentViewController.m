//
//  AXParentViewController.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/28.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXParentViewController.h"
#import "AXMatchFormatViewController.h"
#import "AXEventsViewController.h"
#import "AXStatisticiansManagerViewController.h"
#import "AXSugerSelectedView.h"

@interface AXParentViewController()<AXSugerSelectedViewDelegate>
@property (nonatomic, strong)AXMatchFormatViewController *matchFormationViewController;
@property (nonatomic, strong)AXEventsViewController *eventsViewController;
@property (nonatomic, strong)AXStatisticiansManagerViewController *statisticiansManagerViewController;
@property (nonatomic, strong)UIViewController *currentVC;
@end
@implementation AXParentViewController
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitleView:[[AXSugerSelectedView alloc] initWithContainerView:self.view Delegate:self]];
    
    _matchFormationViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AXMatchFormatViewController"];
    _eventsViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AXEventsViewController"];
    _statisticiansManagerViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AXStatisticiansManagerViewController"];
    
    [self addChildViewController:_matchFormationViewController];
    [self addChildViewController:_eventsViewController];
    [self addChildViewController:_statisticiansManagerViewController];
    
    [self fitFrameForChildViewController:_matchFormationViewController];
    [self.view addSubview:_matchFormationViewController.view];
    _currentVC = _matchFormationViewController;
    self.match = _match;
    
}

-(void)setMatch:(AXMatch *)match
{
    _match = match;
    if ([_currentVC isEqual:_matchFormationViewController]) {
        _matchFormationViewController.match = _match;
    }else if ([_currentVC isEqual:_eventsViewController])
    {
        _eventsViewController.match = _match;
    }else
    {
        _statisticiansManagerViewController.match = _match;
    }
    
}

- (void)fitFrameForChildViewController:(UIViewController *)chileViewController{
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    chileViewController.view.frame = frame;
}

#pragma mark - AXSugerSelectedViewDelegate
-(void)sugerSelected:(ItemType)itemType
{
    switch (itemType) {
        case ItemTypeStatistician:
        {
            [self fitFrameForChildViewController:_statisticiansManagerViewController];
            [self transitionFromViewController:_currentVC toViewController:_statisticiansManagerViewController duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
                if (finished) {
                    _statisticiansManagerViewController.match = _match;
                    [_statisticiansManagerViewController didMoveToParentViewController:self];
                    _currentVC = _statisticiansManagerViewController;
                }
            }];
        }
            break;

        case ItemTypeEvents:
        {
            [self fitFrameForChildViewController:_eventsViewController];
            [self transitionFromViewController:_currentVC toViewController:_eventsViewController duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
                if (finished) {
                    _eventsViewController.match = _match;
                    [_eventsViewController didMoveToParentViewController:self];
                    _currentVC = _eventsViewController;
                }
            }];
        }
            break;
        case ItemTypeFormation:
        {
            [self fitFrameForChildViewController:_matchFormationViewController];
            [self transitionFromViewController:_currentVC toViewController:_matchFormationViewController duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
                if (finished) {
                    _matchFormationViewController.match = _match;
                    [_matchFormationViewController didMoveToParentViewController:self];
                    _currentVC = _matchFormationViewController;
                }
            }];
        }
            break;
        default:
            break;
    }
}

@end
