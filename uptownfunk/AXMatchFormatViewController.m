//
//  AXMatchFormatViewController.m
//  uptownfunk
//
//  Created by yellow on 16/4/19.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXMatchFormatViewController.h"
#import "AXMatchFormatCell.h"
#import "UIView+Ext.h"
#import "NSString+Ext.h"
#import "OPLDataManager.h"
#import "AXTeam.h"
#import "AXMatchFormatAddPlayerCell.h"

@interface AXMatchFormatViewController () <UIAlertViewDelegate,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,AXMatchFormatCellDelegate>
@property (strong, nonatomic) UIBarButtonItem *saveBarButton;

@property (strong, nonatomic) IBOutlet UIButton *masterButton;
@property (weak, nonatomic) IBOutlet UILabel *masterLabel;
@property (strong, nonatomic) IBOutlet UIButton *guestButton;
@property (weak, nonatomic) IBOutlet UILabel *guestLabel;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UICollectionView *masterCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *guestCollectionView;

@property (strong, nonatomic) UICollectionView *activeCollectionView;
@property (strong, nonatomic) NSMutableArray *activeDataSource;
@property (weak, nonatomic) AXMatchFormatCell *editCell;
@property (assign, nonatomic) CGFloat     overlayOffset;
@property (assign, nonatomic) BOOL      isSliding;
@property (assign, nonatomic) BOOL      isGuest;

@property (nonatomic, strong)   NSMutableArray *dataSource;
@property (nonatomic, strong)   NSMutableArray *masterDataSource;
@property (nonatomic, strong)   NSMutableArray *guestDataSource;
@property (nonatomic, strong)   NSMutableArray *changeDataSource;

@property (nonatomic, assign) CGFloat originCollectionViewHeight;

@property (nonatomic, copy) NSDictionary *matchesData;
@end

@implementation AXMatchFormatViewController

#pragma mark - Setter
-(void)setMatch:(AXMatch *)match
{
    _match = match;
    self.masterLabel.text = [NSString stringWithFormat:@"主队\n%@",_match.teamA.name];
    self.guestLabel.text = [NSString stringWithFormat:@"客队\n%@",_match.teamB.name];
    [self requestData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _saveBarButton = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(saveBarButtonClicked:)];
    [_saveBarButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0],NSForegroundColorAttributeName:[UIColor lightThemeColor]} forState:UIControlStateNormal];
    [_saveBarButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0],NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateDisabled];
    [_saveBarButton setEnabled:NO];
    self.parentViewController.navigationItem.rightBarButtonItem = _saveBarButton;
    [self.contentView setBackgroundColor:UIColorWithRGB(0x3a3a3a)];
    [self.masterCollectionView setBackgroundColor:UIColorWithRGB(0x3a3a3a)];
    [self.guestCollectionView setBackgroundColor:UIColorWithRGB(0x3a3a3a)];

    [self.masterCollectionView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)]];
    [self.guestCollectionView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)]];
    
    [self viewChangeWithGuest:NO];
    
    self.originCollectionViewHeight = SCREEN_HEIGHT - 108;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.parentViewController.navigationItem setRightBarButtonItem:_saveBarButton animated:NO];
}
- (void)panGesture:(UITapGestureRecognizer *)recognizer{
    [self endEditView:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self registerNotification];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self unregisterNotification];
}

- (void)requestData
{
    [self.view showLoadingView];
    __weak typeof(self) obj = self;
    [[OPLDataManager manager] fetchFormations:self.match.listIdentifier response:^(id matches, NSString *errorMessage) {
        [obj.view hiddenLoadingView];
        NSArray *list = matches;
        [KTAppSingleton sharedAppSingleton].currentMatchFormations = [NSArray arrayWithArray:matches];
        obj.dataSource = [[NSMutableArray alloc]initWithCapacity:list.count];
        obj.masterDataSource = [[NSMutableArray alloc]initWithCapacity:list.count];
        obj.guestDataSource = [[NSMutableArray alloc]initWithCapacity:list.count];
        for (NSDictionary *item in list) {
            NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:item];
            [obj.dataSource addObject:data];
            
            
            
            NSDictionary *team = data[@"team"];
            if ([team[@"id"] isEqualToString:self.match.teamA.teamIdentifier]) {
                [obj.masterDataSource addObject:data];
            } else {
                [obj.guestDataSource addObject:data];
            }
        }
        
        [obj.masterCollectionView reloadData];
        [obj.guestCollectionView reloadData];
    }];
}

#pragma mark -- Notification
- (void)registerNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)saveBarButtonClicked:(id)sender {
    [self endEditView:YES];
    if ([self.changeDataSource count] > 0) {
        [self.view showLoadingView:@"保存中..."];
        self.saveBarButton.enabled = NO;
        [[OPLDataManager manager]saveFormations:self.match.listIdentifier datas:self.changeDataSource response:^(id matches, NSString *errorMessage) {
            [self.view hiddenLoadingView];
            [self.changeDataSource removeAllObjects];
            if (errorMessage) {
                [self.view showToast:errorMessage];
            }else{
                [self.view showToast:@"保存成功"];
            }
        }];
    }
}

- (IBAction)masterButtonClicked:(UIButton *)sender {
    if (_masterButton.isSelected) {
        return;
    }
    [_guestButton setSelected:NO];
    [_masterButton setSelected:YES];
    [self viewChangeWithGuest:NO];
}

- (IBAction)guestButtonClicked:(UIButton *)sender {
    if (_guestButton.isSelected) {
        return;
    }
    [_masterButton setSelected:NO];
    [_guestButton setSelected:YES];
    [self viewChangeWithGuest:YES];
}

- (UICollectionView *)activeCollectionView
{
    if (self.isGuest) {
        return _activeCollectionView =  self.guestCollectionView;
    }
    return _activeCollectionView =  self.masterCollectionView;
}

- (NSMutableArray *)activeDataSource
{
    if (self.isGuest) {
        return self.guestDataSource;
    }
    return self.masterDataSource;
}


- (void)viewChangeWithGuest:(BOOL)isGuest
{
    [self endEditView:NO];
    
    if(isGuest) {
        self.masterLabel.textColor = [UIColor lightGrayColor];
        [self.masterLabel setBackgroundColor:UIColorWithRGB(0x7f7f7f)];
        self.guestLabel.textColor = [UIColor whiteColor];
        [self.guestLabel setBackgroundColor:UIColorWithRGB(0x3a3a3a)];
    } else {
        self.guestLabel.textColor = [UIColor lightGrayColor];
        [self.guestLabel setBackgroundColor:UIColorWithRGB(0x7f7f7f)];
        self.masterLabel.textColor = [UIColor whiteColor];
        [self.masterLabel setBackgroundColor:UIColorWithRGB(0x3a3a3a)];
    }
    
    self.isGuest = isGuest;
    
    [self.activeCollectionView reloadData];
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationTransition: isGuest?UIViewAnimationTransitionFlipFromLeft:UIViewAnimationTransitionFlipFromRight forView:self.contentView cache:YES];
    [self.contentView bringSubviewToFront:self.activeCollectionView];
    [UIView commitAnimations];
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.activeDataSource.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (indexPath.row == self.activeDataSource.count) {
       AXMatchFormatAddPlayerCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AddPlayerCell" forIndexPath:indexPath];
//        cell.numberTextField.text = @"+";
//        cell.numberTextField.placeholder = @"+";
//        cell.nameLabel.text = @"";
        return cell;
    }
    AXMatchFormatCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FormatCell" forIndexPath:indexPath];
    NSMutableDictionary *data = self.activeDataSource[indexPath.row];
    
    NSNumber *number = data[@"shirt_number"];
    NSString *text = @"";
    if ([number isKindOfClass:[NSNumber class]]) {
        text = [NSString stringWithFormat:@"%@",number];
    }
    
    cell.nameLabel.text = data[@"name"];
    cell.numberTextField.placeholder = text;
    cell.numberTextField.text = text;
    return cell;
}

#pragma mark - AXMatchFormatCellDelegate
- (BOOL)matchFormatCellShouldBeginEditing:(AXMatchFormatCell *)cell
{
    NSIndexPath *indexPath = [self.activeCollectionView indexPathForCell:cell];
    if (indexPath.row == self.activeDataSource.count) {
//        [self performSelector:@selector(addPlayer)];
        return NO;
    }
    
    self.editCell = cell;
    return YES;
}

- (void)matchFormatCellValueChange:(AXMatchFormatCell *)cell
{
    NSIndexPath *indexPath = [self.activeCollectionView indexPathForCell:cell];
    
    NSMutableDictionary *data = nil;
    if (self.activeCollectionView == self.masterCollectionView) {
        data = self.masterDataSource[indexPath.row];
    } else {
        data = self.guestDataSource[indexPath.row];
    }
    
    if ([NSString isEmpty:cell.numberTextField.text]) {
        data[@"shirt_number"] = [NSNull null];
    } else {
        data[@"shirt_number"] = [NSNumber numberWithInteger:cell.numberTextField.text.integerValue];
    }

    if (!self.changeDataSource) {
        self.changeDataSource = [NSMutableArray new];
    }
    
    NSInteger index = [self.changeDataSource indexOfObject:data];
    
    if (index == NSNotFound) {
        [self.changeDataSource addObject:data];
        [self.saveBarButton setEnabled:YES];
    }
    
    self.saveBarButton.enabled = YES;
}

- (void)matchFormatCellDone:(AXMatchFormatCell *)cell
{
    [self saveBarButtonClicked:nil];
}
#pragma mark -- Keyboard notification
- (void)endEditView:(BOOL)animation {
    [self.view endEditing:YES];
    
    if (animation) {
        [UIView animateWithDuration:.2 animations:^{
            
            CGPoint contentOffset = self.activeCollectionView.contentOffset;
            contentOffset.y -= self.overlayOffset;
            contentOffset.y = contentOffset.y > 0?:0;
            self.activeCollectionView.contentOffset = contentOffset;
        } completion:^(BOOL finished) {
            
        }];
    } else {
        CGPoint contentOffset = self.activeCollectionView.contentOffset;
        contentOffset.y -= self.overlayOffset;
        contentOffset.y = contentOffset.y > 0?:0;
        self.activeCollectionView.contentOffset = contentOffset;
    }
    
    self.overlayOffset = 0;
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [value CGRectValue];
    
    keyboardRect.origin.y = SCREEN_HEIGHT - keyboardRect.size.height;
    
    CGRect frame = [self.editCell convertRect:self.editCell.numberTextField.frame toView:self.view];
    NSLog(@"%@,%@",self.editCell.nameLabel.text,NSStringFromCGRect(frame));
    
    CGFloat offset = 0;
    if(CGRectIntersectsRect(keyboardRect, frame)) {
        if (keyboardRect.origin.y > frame.origin.y) {
            offset = frame.size.height - (keyboardRect.origin.y - frame.origin.y) + 90;
        } else {
            offset = frame.size.height + (frame.origin.y - keyboardRect.origin.y) + 90;
        }
    }
    CGPoint contentOffset =  self.activeCollectionView.contentOffset;
    contentOffset.y += offset;
    self.overlayOffset += offset;
    
    self.isSliding = YES;
    [UIView animateWithDuration:.2 animations:^{
        self.activeCollectionView.contentOffset = contentOffset;
    } completion:^(BOOL finished) {
        self.isSliding = NO;
    }];
    
    if (_originCollectionViewHeight == _activeCollectionView.height) {
        [self.activeCollectionView setHeight:_originCollectionViewHeight - keyboardRect.size.height];
    }

}

- (void)keyboardWillHide:(NSNotification *)notification
{
    if (self.activeCollectionView.contentOffsetY < 0) {
        [UIView animateWithDuration:.2 animations:^{
            self.activeCollectionView.contentOffsetY = 0;
        }];
    }

    if (_originCollectionViewHeight != _activeCollectionView.height) {
        [self.activeCollectionView setHeight:_originCollectionViewHeight];
    }

}

- (NSDictionary *)createNewPlayer
{
    NSInteger now = [[NSDate date] timeIntervalSince1970];
    
    NSString *text = [NSString stringWithFormat:@"%04i%05li",now % 10000,random() % 100000];
    NSString *phone = [NSString stringWithFormat:@"14%@",text];
    phone = [phone substringToIndex:11];
    return  @{
              @"name": @"AP",
              @"country_code": @"+86",
              @"phone": phone,
              @"gender": @1,
              @"email": [NSNull null],
              @"avatar_uri": [NSNull null],
              @"birth": [NSNull null],
              @"height": [NSNull null],
              @"weight": [NSNull null],
              @"id_card": [NSNull null],
              @"passport": [NSNull null],
              @"nationality": @"中国",
              @"account_roles": @[],
              @"roles": @[],
              @"area": @{
                      @"country": @"中国",
                      @"province": @"广东",
                      @"city": @"广州",
                      @"district": @"白云"
                      }
              };
    
}

- (void)commitFreeMatchNewPlayer:(NSInteger)number completion:(void (^)(BOOL success))completion
{
    [self.view showLoadingView:@"保存中..."];
    NSDictionary *data = @{@"player":[self createNewPlayer],@"shirt_number":@(number),@"roles":@[@"player"]};
    NSDictionary *team = self.isGuest?[self.match.teamB dictionaryRepresentation]:[self.match.teamA dictionaryRepresentation];


    [[OPLDataManager manager] addMatchPlayer:data teamId:team[@"id"] response:^(id matches, NSString *errorMessage) {
        [self.view hiddenLoadingView];
        completion([NSString isEmpty:errorMessage]);
        if (![NSString isEmpty:errorMessage]) {
            [self.view showToast:errorMessage];
            return ;
        }
    }];
}

#pragma mark -- UIAlertViewDelegate
- (void)willPresentAlertView:(UIAlertView *)alertView
{
    UITextField *textField =  [alertView textFieldAtIndex:0];
    textField.delegate = self;
    textField.placeholder = [self.match.matchStyle isEqualToString:@"test"]?@"12,34,56":@"请输入球员号";
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UITextField *textField =  [alertView textFieldAtIndex:0];
    [textField resignFirstResponder];
    if (buttonIndex == alertView.cancelButtonIndex) {
        return;
    }
    
    NSString *number = [[alertView textFieldAtIndex:0] text];
    if ([self.match.matchStyle isEqualToString:@"test"]) {
        NSMutableArray *players = [NSMutableArray arrayWithArray:self.matchesData[@"team_a_players"]];
        if (self.isGuest) {
            players = [NSMutableArray arrayWithArray:self.matchesData[@"team_b_players"]];
        }
        
        NSArray *list = [number componentsSeparatedByString:@","];
        
        for (NSString *newPlayer in list) {
            for (NSNumber *player in players) {
                if (player.integerValue == [newPlayer integerValue]) {
                    [self.view showToast:@"存在重复球员号"];
                    return;
                }
            }
            [players addObject:@(newPlayer.integerValue)];
        }
        
        NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:self.matchesData];
        if (self.isGuest) {
            data[@"team_b_players"] = players;
        } else {
            data[@"team_a_players"] = players;
        }
        
        [self.view showLoadingView:@"保存中..."];
        __weak typeof(self) obj =  self;
        [[OPLDataManager manager] saveTestMatchInfoWithMatchId:self.match.listIdentifier data:data response:^(id matches, NSString *errorMessage) {
            [self.view hiddenLoadingView];
            if (![NSString isEmpty:errorMessage]) {
                [self.view showToast:errorMessage];
                return ;
            }
            [obj requestData];
        }];
        
    } else {
        __weak typeof(self) obj =  self;
        [self commitFreeMatchNewPlayer:number.integerValue completion:^(BOOL success) {
            if (success) {
                [obj requestData];
            }
        }];
    }
}

- (void)fetchTestMatchInfo
{
    [self.view showLoadingView:@"加载中..."];
    __weak typeof(self) obj = self;
    [[OPLDataManager manager] fetchTestMatchInfoWithMatchId:self.match.listIdentifier response:^(id matches, NSString *errorMessage) {
        [obj.view hiddenLoadingView];
        if (![NSString isEmpty:errorMessage]) {
            [obj.view showToast:errorMessage];
            return ;
        }
        
        obj.matchesData = matches;
        [obj showAlertViewForInputNumber];
    }];
}

- (void)showAlertViewForInputNumber
{
    NSString *title = @"添加球员";
    if ([self.match.matchStyle isEqualToString:@"test"]) {
        title = @"添加测试赛球员";

    }
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"添加", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView show];
}

- (IBAction)addBtnClicked:(id)sender {
    if ([self.match.matchStyle isEqualToString:@"test"]) {
        [self fetchTestMatchInfo];
    } else {
        [self showAlertViewForInputNumber];
    }
    
}
@end
