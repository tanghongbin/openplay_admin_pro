//
//  AXEventsFilterItemCell.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/4.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXEventsFilterItemCell.h"
@interface AXEventsFilterItemCell()
@property (nonatomic, strong)UIView *seperateView;
@property (nonatomic, strong)UIView *containView;

@end
@implementation AXEventsFilterItemCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setFrame:CGRectMake(0, 0, iPad?768:SCREEN_WIDTH, 40)];
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.containView];
        UIView *selectedView = [[UIView alloc]init];
        [selectedView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.65]];
        self.selectedBackgroundView = selectedView;

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - Getter
-(UIView *)containView
{
    if (!_containView) {
        _containView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.width - 0, self.height - 0)];
        [_containView setBackgroundColor:[UIColor defaultTextColor]];
        [_containView addSubview:self.seperateView];
        [_containView addSubview:self.teamLabel];
    }
    return _containView;
}

-(UIView *)seperateView
{
    if (!_seperateView) {
        _seperateView = [[UIView alloc]initWithFrame:CGRectMake(0, _containView.height - 1, _containView.width, 1)];
        [_seperateView setBackgroundColor:UIColorWithRGB(0x4a4a4a)];
    }
    return _seperateView;
}

-(UILabel *)teamLabel
{
    if (!_teamLabel) {
        _teamLabel = [self headerLabelWithFrame:CGRectMake(10, 0, _containView.width - 20, _containView.height) Title:@"球队"];
    }
    return _teamLabel;
}

-(UILabel*)headerLabelWithFrame:(CGRect)frame Title:(NSString*)title
{
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:frame];
    [headerLabel setTextColor:[UIColor defaultBackGroundColor]];
    [headerLabel setTextAlignment:NSTextAlignmentLeft];
    [headerLabel setFont:[UIFont systemFontOfSize:12]];
    [headerLabel setText:title];
    return headerLabel;
}

@end
