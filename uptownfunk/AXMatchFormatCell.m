//
//  AXMatchFormatCell.m
//  uptownfunk
//
//  Created by yellow on 16/4/20.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXMatchFormatCell.h"
#import "NSString+Ext.h"

@interface AXMatchFormatCell () <UITextFieldDelegate>


@end

@implementation AXMatchFormatCell

- (void)awakeFromNib
{
    self.numberTextField.layer.cornerRadius = 40;
    self.numberTextField.layer.masksToBounds = YES;
    
    self.backgroundView = nil;
    self.backgroundColor = [UIColor clearColor];
}

- (BOOL)isPlayerNumber:(NSString *)str
{
    NSString *regex = @"^[1-9]?[0-9]";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:str];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([self.delegate respondsToSelector:@selector(matchFormatCellShouldBeginEditing:)]) {
        return [self.delegate matchFormatCellShouldBeginEditing:self];
    }
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSString *text = textField.text;
    NSString *placeholder = textField.placeholder;
    
    if (![text isEqualToString:placeholder]) {
        textField.placeholder = text;
        if ([self.delegate respondsToSelector:@selector(matchFormatCellValueChange:)]) {
            [self.delegate matchFormatCellValueChange:self];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSString *text = textField.text;
    NSString *placeholder = textField.placeholder;
    
    [textField resignFirstResponder];
    
    [self textFieldDidEndEditing:textField];
    if (![text isEqualToString:placeholder]) {
        textField.placeholder = text;
        if ([self.delegate respondsToSelector:@selector(matchFormatCellDone:)]) {
            [self.delegate matchFormatCellDone:self];
        }
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([NSString isEmpty:string]) {
        return YES;
    }
    
    NSString *text = [NSString stringWithFormat:@"%@%@",textField.text,string];
    if ([self isPlayerNumber:text]) {
        return YES;
    }
    return NO;
}


@end
