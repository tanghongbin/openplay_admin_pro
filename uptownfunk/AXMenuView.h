//
//  AXMenuView.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/11.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, AXMenuButtonType) {
    AXMenuButtonTypeAddTestMatch
};

@class AXMenuView;
@protocol AXMenuViewDelegate <NSObject>

@required
-(void)menuView:(AXMenuView*)menuView selectedIndex:(NSInteger)buttonIndex;

@end

@interface AXMenuView : UIView
@property (nonatomic, assign)id<AXMenuViewDelegate>delegate;

- (instancetype)initOnView:(UIView*)onView;
-(void)trigger;
@end
