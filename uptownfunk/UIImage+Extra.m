//
//  UIImage+Extra.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/26.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "UIImage+Extra.h"

@implementation UIImage (Extra)
+ (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
@end
