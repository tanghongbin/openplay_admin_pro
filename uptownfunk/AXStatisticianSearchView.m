//
//  AXStatisticianSearchView.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXStatisticianSearchView.h"
#import "AXEventsFilterItemCell.h"
#import "OPLDataManager.h"
#import "AXStatistician.h"

@interface AXStatisticianSearchView()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)UISearchBar *searchBar;
@property (nonatomic, strong)UITableView *resultTableView;

@property (nonatomic, strong)UIButton *backgroundButton;
@property (nonatomic, strong)UIView *contentView;
@property (nonatomic, strong)UIView *onView;
@property (nonatomic, strong)NSMutableArray<AXStatistician*> *resultArray;
@end
@implementation AXStatisticianSearchView

- (instancetype)initWithFrame:(CGRect)frame onView:(UIView*)onView
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.0]];
        self.onView = onView;
        [self addSubview:self.backgroundButton];
        [self addSubview:self.contentView];
    }
    return self;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.resultArray = [NSMutableArray new];
    [[OPLDataManager manager] searchStatisticiansWithKey:searchText response:^(id data, NSString *errorMessage) {
        if (errorMessage) {
            AlertViewShowWith(errorMessage);
        }else{
            NSArray *staArray = [data objectForKey:@"list"];
            [staArray enumerateObjectsUsingBlock:^(NSDictionary*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [self.resultArray addObject:[AXStatistician modelObjectWithDictionary:obj]];
            }];
            [_resultTableView reloadData];
        }
    }];
}

-(void)show
{
    [_onView addSubview:self];
    [_onView bringSubviewToFront:self];
    [UIView animateWithDuration:0.3 animations:^{
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.85]];
        [_contentView setTransform:CGAffineTransformMakeTranslation(0, -_contentView.height)];
    } completion:^(BOOL finished) {
        
    }];
}

-(void)cancel
{
    [UIView animateWithDuration:0.3 animations:^{
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.0]];
        [_contentView setTransform:CGAffineTransformIdentity];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - Getter
-(UISearchBar *)searchBar
{
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, self.width, 40)];
        [_searchBar setSearchBarStyle:UISearchBarStyleMinimal];
        [_searchBar setPlaceholder:@"搜索添加统计员"];
        [_searchBar setDelegate:self];
    }
    return _searchBar;
}

-(UITableView *)resultTableView
{
    if (!_resultTableView) {
        _resultTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, _onView.width, _onView.height - 80) style:UITableViewStylePlain];
        [_resultTableView setBackgroundColor:[UIColor clearColor]];
        [_resultTableView setDelegate:self];
        [_resultTableView setDataSource:self];
        [_resultTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_resultTableView registerClass:[AXEventsFilterItemCell class] forCellReuseIdentifier:@"AXEventsFilterItemCell"];
    }
    return _resultTableView;
}

-(UIView *)contentView
{
    if (!_contentView) {
        _contentView = [[UIView alloc]initWithFrame:CGRectMake(0, _onView.height, self.width, _onView.height - 40)];
        [_contentView setBackgroundColor:[UIColor defaultTextColor]];
        [_contentView addSubview:self.searchBar];
        [_contentView addSubview:self.resultTableView];
    }
    return _contentView;
}

-(UIButton *)backgroundButton
{
    if (!_backgroundButton) {
        _backgroundButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backgroundButton setFrame:CGRectMake(0, 0, self.width, 40)];
        [_backgroundButton setBackgroundColor:[UIColor clearColor]];
        [_backgroundButton setTitle:@"🙄️取戳" forState:UIControlStateNormal];
        [_backgroundButton bk_addEventHandler:^(id sender) {
            [self cancel];
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _backgroundButton;
}

#pragma mark - UITableViewDelegate & UITableViewDataSrouce
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _resultArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXEventsFilterItemCell *c = [tableView dequeueReusableCellWithIdentifier:@"AXEventsFilterItemCell" forIndexPath:indexPath];
    [c.teamLabel setText:_resultArray[indexPath.row].name];
    return c;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self cancel];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([_deleagte respondsToSelector:@selector(statisticianSearchView:didSelectedStatistician:)]) {
            [_deleagte statisticianSearchView:self didSelectedStatistician:_resultArray[indexPath.row]];
        }
    });
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self endEditing:YES];
}

@end
