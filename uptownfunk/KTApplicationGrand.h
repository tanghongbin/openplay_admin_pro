//
//  KTApplicationGrand.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#ifndef KTApplicationGrand_h
#define KTApplicationGrand_h

#if DEBUG
#define API_URL @"https://cub-staging.openplay.com/admin/v1"
#elif STAG
#define API_URL @"https://cub-staging.openplay.com/admin/v1"
#elif PROD
#define API_URL @"https://cub.openplay.com/admin/v1"
#endif

#pragma mark - .h file
#import "KTAppSingleton.h"
#import "UIColor+appColor.h"
#import "UIImage+Extra.h"
#import <FrameAccessor/FrameAccessor.h>
#import "OPLDataManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AXStaticDataHelper.h"
#import <MJRefresh/MJRefresh.h>
#import <BlocksKit.h>
#import <BlocksKit+UIKit.h>
#import "UIView+Ext.h"
#import <AFNetworking/AFNetworking.h>
#import "Masonry.h"
#import "NSDate+OPBUtil.h"
#import "NSString+Extra.h"

#pragma mark - grand
#define UIColorWithRGB_Alpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]
#define UIColorWithRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#ifdef DEBUG
#define DLog(format, ...) NSLog(format, ## __VA_ARGS__)
#define DLogObject(object) NSLog(@"%@ == %@", NSStringFromClass([object class]), object)
#elif STAG
#define DLog(format, ...) NSLog(format, ## __VA_ARGS__)
#define DLogObject(object) NSLog(@"%@ == %@", NSStringFromClass([object class]), object)
#else
#define DLog(format, ...)
#define DLogObject(object)
#endif

#define DLogSize(size) DLog(@"\n width == %.2f\n height == %.2f", size.width,size.height)

#define DLogFrame(frame) DLog(@"\n x -- %.2f\n y -- %.2f\n width -- %.2f\n height -- %.2f",frame.origin.x,frame.origin.y,frame.size.width,frame.size.height)

#define DLogEdgeInsets(edgeInsets) DLog(@"\n top -- %.2f\n bottom -- %.2f\n left -- %.2f\n right -- %.2f",edgeInsets.top,edgeInsets.bottom,edgeInsets.left,edgeInsets.right)

#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.width > [[UIScreen mainScreen] bounds].size.height ? [UIScreen mainScreen].bounds.size.width : [[UIScreen mainScreen] bounds].size.height)

#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width < [[UIScreen mainScreen] bounds].size.height ? [UIScreen mainScreen].bounds.size.width : [[UIScreen mainScreen] bounds].size.height)
#define iPad ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad ? YES : NO)
#define AlertViewShowWith(msg) [[[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil] show];
#define iOS7 ([[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] intValue] == 7?YES:NO)

//判断3.5

#define iPhone4s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

//判断4.0

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

//判断4.7

#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)

//判断5.5

#define iPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)

//判断iphone6+ (放大)
#define iPhone6Plus_Scaled ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) : NO)

static NSString * const kUpdatedEventNotificaitonName = @"updatedEvents";

typedef NS_ENUM(NSUInteger, AXEventItemType) {
    AXEventItemTypeEvent,
    AXEventItemTypeTeam,
    AXEventItemTypePlayer,
    AXEventItemTypeStatistician,
    AXEventItemTypeDate,
    AXEventItemTypeTime
};

#endif /* KTApplicationGrand_h */
