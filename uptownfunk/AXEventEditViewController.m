//
//  AXEventEditViewController.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/5/4.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "AXEventEditViewController.h"
#import "OPLDataManager.h"
#import "AXPlayer.h"
#import "AXTeam.h"
#import "AXMatch.h"
#import "AXStatistician.h"
#import "AXEventsFilterItemCell.h"
#import "AXEventItemSelectViewController.h"
#import "OPFMatchStatisticians.h"
#import "AXFormation.h"
#import "AXEventDateAndTimeSelectView.h"

@interface AXEventEditViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)UITableView *contentTableView;
@property (nonatomic, strong)UIView *footerView;
@property (nonatomic, strong)NSMutableArray *formationsArray;
@property (nonatomic, strong)UIButton *saveButton;
@end

@implementation AXEventEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor defaultTextColor]];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"😵删除" style:UIBarButtonItemStylePlain target:self action:@selector(deleteEvent)];
    [rightBarButtonItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16.0],NSForegroundColorAttributeName:[UIColor redColor]} forState:UIControlStateNormal];
    [self.navigationItem setRightBarButtonItem:rightBarButtonItem];
    
    [self.view addSubview:self.contentTableView];
    [self.contentTableView reloadData];
    
    self.formationsArray = [NSMutableArray new];
    for (NSDictionary *dc in [KTAppSingleton sharedAppSingleton].currentMatchFormations) {
        AXFormation *formation = [AXFormation modelObjectWithDictionary:dc];
        [self.formationsArray addObject:formation];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_contentTableView reloadData];
    
    if ([AXStaticDataHelper matchEvent:[NSString stringWithFormat:@"%0.0f",_event.code]]) {
        
        [_event setPlayer:nil];
        [_contentTableView reloadData];
        
        if (!_event.statistician) {
            [self.saveButton setEnabled:NO];
        }else{
            [self.saveButton setEnabled:YES];
        }
    }else{
        if (!_event.statistician||!_event.player) {
            [self.saveButton setEnabled:NO];
        }else{
            [self.saveButton setEnabled:YES];
        }
    }
    
}

-(void)saveEvent
{
    [self.view showLoadingView];
    
    if (![_event listIdentifier]) {

        NSDictionary *teamId = @{@"id":_event.team.teamIdentifier};
        NSDictionary *playerId = @{@"id":_event.player?_event.player.playerIdentifier:[NSNull null]};
        NSDictionary *statisticianId = @{@"id":_event.statistician.statisticianIdentifier};
        NSDictionary *dataDic = @{@"team":teamId,@"player":playerId,@"statistician":statisticianId,@"code":[NSString stringWithFormat:@"%0.0f",_event.code],@"client_started_at":_event.clientStartedAt};
        
        NSArray *array = @[dataDic];
        
        [[OPLDataManager manager] addEventWithMatchId:_match.listIdentifier data:array response:^(id event, NSString *errorMessage) {
            [self.view hiddenLoadingView];
            if (errorMessage) {
                AlertViewShowWith(errorMessage);
            }else
            {
                [self.view showToast:@"添加成功"];
                [[NSNotificationCenter defaultCenter] postNotificationName:kUpdatedEventNotificaitonName object:nil];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
                
            }
        }];
    }else{
        [[OPLDataManager manager] saveEventWithEventId:_event.listIdentifier data:[_event dictionaryRepresentation] response:^(id event, NSString *errorMessage) {
            [self.view hiddenLoadingView];
            if (errorMessage) {
                AlertViewShowWith(errorMessage);
            }else
            {
                [self.view showToast:@"保存成功"];
                [[NSNotificationCenter defaultCenter] postNotificationName:kUpdatedEventNotificaitonName object:nil];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
                
            }
        }];
    }
}

-(void)deleteEvent
{
    [[UIAlertView bk_showAlertViewWithTitle:@"🗑事件你都敢删呢?"
                                    message:nil
                          cancelButtonTitle:@"不是我点的🙄️"
                          otherButtonTitles:@[@"删了！"]
                                    handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                        if (buttonIndex == alertView.cancelButtonIndex) {
                                            
                                        }else{
                                            [self.view showLoadingView];
                                            [[OPLDataManager manager] deleteEventWithEventId:_event.listIdentifier response:^(id event, NSString *errorMessage) {
                                                [self.view hiddenLoadingView];
                                                if (errorMessage) {
                                                    AlertViewShowWith(errorMessage);
                                                }else
                                                {
                                                    [self.view showToast:@"删除成功"];
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdatedEventNotificaitonName object:nil];
                                                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                        [self.navigationController popViewControllerAnimated:YES];
                                                    });
                                                }
                                            }];
                                        }
                                        
                                    }] show];

}

#pragma mark - UITableViewDelegate & UITaleViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [AXStaticDataHelper eventItemTitleArray][section];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXEventsFilterItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AXEventsFilterItemCell" forIndexPath:indexPath];
    [cell.teamLabel setFont:[UIFont systemFontOfSize:19.0]];
    switch (indexPath.section) {
        case AXEventItemTypeEvent:
        {
            [cell.teamLabel setText:[AXStaticDataHelper eventCodeForStringWithKey:[NSString stringWithFormat:@"%0.0f",_event.code]]];
        }
            break;
        case AXEventItemTypeTeam:
        {
            [cell.teamLabel setText:_event.team.name];
        }
            break;
        case AXEventItemTypePlayer:
        {
            if (!_event.player||!_event.player.playerIdentifier) {
                [cell.teamLabel setText:@"无"];
                [cell.teamLabel setTextColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.7]];
            }else{
                [cell.teamLabel setText:[NSString stringWithFormat:@"%@｜%0.0f号",_event.player.name,_event.shirtNumber]];
                [cell.teamLabel setTextColor:[UIColor defaultBackGroundColor]];
            }
        }
            break;
        case AXEventItemTypeStatistician:
        {
            if (!_event.statistician) {
                [cell.teamLabel setText:@"无"];
                [cell.teamLabel setTextColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.7]];
            }else{
                [cell.teamLabel setText:_event.statistician.name];
                [cell.teamLabel setTextColor:[UIColor defaultBackGroundColor]];
            }
        }
            break;
        case AXEventItemTypeDate:
        {
            NSArray *strArray = [_event.clientStartedAt componentsSeparatedByString:@"."];
            if ([strArray count] > 0) {
                NSString *startTime = [NSString formatYearMonthDayHourMinuteSecond:[NSDate dateStringToDate:strArray[0]].timeIntervalSince1970];
                [cell.teamLabel setText:[[startTime componentsSeparatedByString:@" "] count]>0?[startTime componentsSeparatedByString:@" "][0]:nil];
            }
        }
            break;
        case AXEventItemTypeTime:
        {
            NSArray *strArray = [_event.clientStartedAt componentsSeparatedByString:@"."];
            if ([strArray count] > 1) {
                NSString *startTime = [NSString formatYearMonthDayHourMinuteSecond:[NSDate dateStringToDate:strArray[0]].timeIntervalSince1970];
                [cell.teamLabel setText:[[startTime componentsSeparatedByString:@" "] count]>1?[startTime componentsSeparatedByString:@" "][1]:nil];
            }
        }
            break;
        default:
            break;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    AXEventItemSelectViewController *vc = [AXEventItemSelectViewController new];
    vc.event = _event;
    switch (indexPath.section) {
        case AXEventItemTypeEvent:
        {
            vc.dataSource = [AXStaticDataHelper eventStringArray];
            vc.formations = _formationsArray;
            vc.eventItemType = AXEventItemTypeEvent;
        }
            break;
        case AXEventItemTypeTeam:
        {
            vc.dataSource = @[_match.teamA,_match.teamB];
            vc.formations = _formationsArray;
            vc.matchStatisticians = _match.matchStatisticians;
            vc.eventItemType = AXEventItemTypeTeam;
        }
            break;
        case AXEventItemTypePlayer:
        {
            if ([AXStaticDataHelper matchEvent:[NSString stringWithFormat:@"%0.0f",_event.code]]) {
                return;
            }else{
                NSMutableArray *fArray = [NSMutableArray new];
                for (AXFormation *formation in _formationsArray) {
                    if ([_event.team.teamIdentifier isEqualToString:formation.teamId]) {
                        [fArray addObject:formation];
                    }
                }
                vc.dataSource = fArray;
                vc.eventItemType = AXEventItemTypePlayer;

            }
        }
            break;
        case AXEventItemTypeStatistician:
        {
            NSMutableArray *sArray = [NSMutableArray new];
            for (OPFMatchStatisticians *matchStatisticians in _match.matchStatisticians) {
                if ([_event.team.teamIdentifier isEqualToString:matchStatisticians.teamId]) {
                    [sArray addObject:matchStatisticians];
                }
            }
            vc.dataSource = sArray;
            vc.eventItemType = AXEventItemTypeStatistician;
            
        }
            break;
        case AXEventItemTypeDate:
        {
            NSArray *strArray = [_event.clientStartedAt componentsSeparatedByString:@"."];
            if ([strArray count] > 1) {
                AXEventDateAndTimeSelectView *dateSelectView = [[AXEventDateAndTimeSelectView alloc]initWithEventTime:[NSDate dateStringToDate:strArray[0]] eventItemType:AXEventItemTypeDate onView:self.view];
                [dateSelectView setCompleted:^(NSDate*date){
                    
                    [UIView animateWithDuration:0.3 animations:^{
                        [_contentTableView setContentOffsetY:0];
                    }];
                    
                    if (date) {
                        NSString *dateString = [NSDate dateToDateString:date];
                        [_event setClientStartedAt:dateString];
                        [_contentTableView reloadData];
                    }
                }];
                [dateSelectView show];
                [UIView animateWithDuration:0.3 animations:^{
                    [_contentTableView setContentOffsetY:80];
                }];
            }
            return;
        }
            break;
        case AXEventItemTypeTime:
        {
            NSArray *strArray = [_event.clientStartedAt componentsSeparatedByString:@"."];
            if ([strArray count] > 1) {
                AXEventDateAndTimeSelectView *timeSelectView = [[AXEventDateAndTimeSelectView alloc]initWithEventTime:[NSDate dateStringToDate:strArray[0]] eventItemType:AXEventItemTypeTime onView:self.view];
                [timeSelectView setCompleted:^(NSDate*date){
                    
                    [UIView animateWithDuration:0.3 animations:^{
                        [_contentTableView setContentOffsetY:0];
                    }];
                    
                    if (date) {
                        NSString *dateString = [NSDate dateToDateString:date];
                        [_event setClientStartedAt:dateString];
                        [_contentTableView reloadData];
                    }
                }];
                [timeSelectView show];
                [UIView animateWithDuration:0.3 animations:^{
                    [_contentTableView setContentOffsetY:150];
                }];
            }
            return;
            return;
        }
            break;
        default:
            break;
    }
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Setter
-(void)setEvent:(AXEvent *)event
{
    _event = event;
}

#pragma Mark - Getter

-(UITableView *)contentTableView
{
    if (!_contentTableView) {
        _contentTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, iPad?self.view.width - 320:self.view.width, self.view.height) style:UITableViewStyleGrouped];
        [_contentTableView setDelegate:self];
        [_contentTableView setDataSource:self];
        [_contentTableView setBackgroundColor:[UIColor clearColor]];
        [_contentTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_contentTableView registerClass:[AXEventsFilterItemCell class] forCellReuseIdentifier:@"AXEventsFilterItemCell"];
        [_contentTableView setTableFooterView:self.footerView];
        [_contentTableView setContentInsetTop:-20];
    }
    
    return _contentTableView;
}

-(UIView *)footerView
{
    if (!_footerView) {
        _footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, iPad?self.view.width - 320:self.view.width, 90)];
        [_footerView setBackgroundColor:[UIColor clearColor]];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(0, 10, _footerView.width, _footerView.height - 10)];
        [button setTitle:[_event listIdentifier]?@"保存事件👻":@"👻添加事件" forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont systemFontOfSize:20.0]];
        [button setBackgroundImage:[UIImage imageNamed:@"opf_btn_delete_bg_normal"] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"opf_btn_delete_bg_higlight"] forState:UIControlStateHighlighted];
        [button bk_addEventHandler:^(id sender) {
            [self saveEvent];
            
        } forControlEvents:UIControlEventTouchUpInside];
        
        [_footerView addSubview:button];
        self.saveButton = button;
    }
    return _footerView;
}



@end
