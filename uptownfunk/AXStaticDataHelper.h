//
//  AXDataHelper.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/19.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AXStaticDataHelper : NSObject
+ (NSString *)roleStringWithKey:(NSString*)key;
+ (NSString *)statisticianPositionStringWithKey:(NSString*)key;
+ (NSString *)statisticianRoleStringWithKey:(NSString*)key;
+ (NSString *)statusStringWithKey:(NSString*)key;
+ (NSString *)approvalStatusStringWithKey:(NSString*)key;
+ (NSString *)featuresStringWithKey:(NSString*)key;
+ (NSString *)genderStringWithKey:(NSString*)key;
+ (NSString *)periodStringWithKey:(NSString*)key;
+ (NSString *)matchStatusStringWithKey:(NSString*)key;
+ (NSString *)matchTypeStringWithKey:(NSString*)key;
+ (NSString *)matchStyleStringWithKey:(NSString *)key;
+ (NSString *)statsModeStringWithKey:(NSString*)key;
+ (NSString *)stoppedWatchStringWithKey:(NSString*)key;
+ (NSString *)eventCodeForStringWithKey:(NSString*)key;
+ (NSString *)operationStringWithKey:(NSInteger)k;
+ (NSInteger)matchTypeWithString:(NSString *)key;
+ (NSInteger)stoppedWatchValueWithString:(NSString *)key;

+ (NSDictionary *)eventCodeStringDic;
+ (NSArray *)eventQueriesCodeArray;
+ (NSArray<NSString*> *)eventItemTitleArray;
+ (NSString*)matchEvent:(NSString*)key;
+ (NSArray<NSString*>*)eventStringArray;
@end
