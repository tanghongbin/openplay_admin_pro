//
//  LCDetailContentViewController.m
//  Litchi
//
//  Created by Jarvis on 6/26/15.
//  Copyright (c) 2015 com.thongbin. All rights reserved.
//

#import "AXAddTestMatchViewController.h"
#import "AppDelegate.h"

#define __animationDuration 0.2

@interface ContentView : UIView

@end

@implementation ContentView

@end


@interface AXAddTestMatchViewController ()<UIGestureRecognizerDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    ContentView *contentView;
}

@property (nonatomic, strong) UITextField *homeTeamTextField;
@property (nonatomic, strong) UITextField *guestTeamTextField;
@property (nonatomic, strong) UITextField *matchDateTextField;
@property (nonatomic, strong) UITextField *matchTimeTextField;
@property (nonatomic, strong) UITextField *matchTypeTextField;
@property (nonatomic, strong) UITextField *ruleStopedWatchTextField;
@property (nonatomic, strong) UITextField *ruleHalfTimeTextField;
@property (nonatomic, strong) UITextField *ruleExtraTimeTextField;

@property (nonatomic, strong) UITextField *currentTextField;

@property (nonatomic, strong) UILabel *headingLabel;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIButton *saveButton;
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UILabel *currentTitleLabel;
@property (nonatomic, strong) UIButton *previousButton;
@property (nonatomic, strong) UIButton *nextButton;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSDate *matchDate;

@property (nonatomic, strong) NSArray<UITextField*> *textFieldArray;
@end

@implementation AXAddTestMatchViewController

-(UILabel *)headingLabel
{
    if(!_headingLabel){
        _headingLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 3, iPad?SCREEN_HEIGHT:SCREEN_WIDTH, 30)];
        [_headingLabel setText:@"建测试赛"];
        [_headingLabel setTextColor:[UIColor whiteColor]];
        [_headingLabel setTextAlignment:NSTextAlignmentCenter];
        [_headingLabel setFont:[UIFont systemFontOfSize:20.0f]];
    }
    return _headingLabel;
}

-(UIButton *)closeButton
{
    if (!_closeButton) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton.layer setCornerRadius:3];
        [_closeButton.layer setMasksToBounds:YES];
        [_closeButton setFrame:CGRectMake(0, 0, 60, 30)];
        [_closeButton setBackgroundImage:[UIImage imageFromColor:[[UIColor blackColor] colorWithAlphaComponent:0.16]] forState:UIControlStateNormal];
        [_closeButton setBackgroundImage:[UIImage imageFromColor:[[UIColor blackColor] colorWithAlphaComponent:0.86]] forState:UIControlStateHighlighted];
        [_closeButton setTitle:@"取消" forState:UIControlStateNormal];
        [_closeButton.titleLabel setFont:[UIFont systemFontOfSize:12.5]];
        [_closeButton setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.65] forState:UIControlStateNormal];
        [_closeButton setCenter:CGPointMake(10 + _closeButton.frame.size.width/2, _closeButton.frame.size.height/2 + 5)];
        [_closeButton addTarget:self action:@selector(closeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

-(UIButton *)saveButton
{
    if (!_saveButton) {
        _saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_saveButton.layer setCornerRadius:3];
        [_saveButton.layer setMasksToBounds:YES];
        [_saveButton setFrame:CGRectMake(0, 0, 60, 30)];
        [_saveButton setBackgroundImage:[UIImage imageFromColor:UIColorWithRGB_Alpha(0x40C0FC, 1.0)] forState:UIControlStateNormal];
        [_saveButton setBackgroundImage:[UIImage imageFromColor:UIColorWithRGB_Alpha(0x40C0FC, 0.65)] forState:UIControlStateHighlighted];
        [_saveButton setBackgroundImage:[UIImage imageFromColor:UIColorWithRGB_Alpha(0x40C0FC, 0.05)] forState:UIControlStateDisabled];
        [_saveButton setTitle:@"保存" forState:UIControlStateNormal];
        [_saveButton.titleLabel setFont:[UIFont systemFontOfSize:12.5]];
        [_saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_saveButton setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.35] forState:UIControlStateDisabled];
        [_saveButton setCenter:CGPointMake(contentView.width - 10 - 30, _closeButton.frame.size.height/2 + 5)];
        [_saveButton addTarget:self action:@selector(saveButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveButton;
}

-(void)closeButtonTapped:(UIButton*)btn
{
    [self.view endEditing:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
       [self dismissViewControllerAnimated:NO completion:nil];
    });
}

-(void)saveButtonTapped:(UIButton*)btn
{
    [self.view endEditing:YES];
    NSDictionary *data = @{@"team_a":@{@"id":[NSNull null],@"name":_homeTeamTextField.text},
                           @"team_b":@{@"id":[NSNull null],@"name":_guestTeamTextField.text},
                           @"team_a_players":@[],
                           @"team_b_players":@[],
                           @"stadium":@{@"id":[NSNull null],
                                        @"name":[NSNull null],
                                        @"type":[NSNull null],
                                        @"map_name":[NSNull null],
                                        @"address":[NSNull null],
                                        @"longitude":[NSNull null],
                                        @"latitude":[NSNull null],
                                        @"venues":@[]},
                           @"venue":@{@"id":[NSNull null],
                                      @"name":[NSNull null]},
                           @"match_type":@([AXStaticDataHelper matchTypeWithString:_matchTypeTextField.text]),
                           @"start_at":[NSDate dateToDateString:_matchDate],
                           @"rule_stopped_watch":@([AXStaticDataHelper stoppedWatchValueWithString:_ruleStopedWatchTextField.text]),
                           @"rule_half_time":@([_ruleHalfTimeTextField.text integerValue]),
                           @"rule_extra_time":@([_ruleExtraTimeTextField.text integerValue]),
                           @"stats_mode":@0
                           };
    NSArray *d = @[data];
    
    [self.view showLoadingView:@"添加比赛中.."];
    [[OPLDataManager manager] addTestMatchWithData:d response:^(id testMatch, NSString *errorMessage) {
        [self.view hiddenLoadingView];
        if (errorMessage) {
            AlertViewShowWith(errorMessage);
        }else
        {
            [self.view showToast:@"哈哈哈哈~爽了吧😏"];
            [[NSNotificationCenter defaultCenter] postNotificationName:kUpdatedEventNotificaitonName object:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self closeButtonTapped:nil];
            });
        }
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (iOS7) {
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        self.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    contentView = [[ContentView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT, iPad?SCREEN_HEIGHT:SCREEN_WIDTH, SCREEN_HEIGHT)];
    [contentView setBackgroundColor:[UIColor defaultTextColor]];
    [self.view addSubview:contentView];
    
    [contentView addSubview:self.headingLabel];
    [contentView addSubview:self.closeButton];
    [contentView addSubview:self.saveButton];
    
    [contentView addSubview:self.homeTeamTextField];
    [contentView addSubview:self.guestTeamTextField];
    [contentView addSubview:self.matchDateTextField];
    [contentView addSubview:self.matchTimeTextField];
    [contentView addSubview:self.matchTypeTextField];
    [contentView addSubview:self.ruleStopedWatchTextField];
    [contentView addSubview:self.ruleHalfTimeTextField];
    [contentView addSubview:self.ruleExtraTimeTextField];
    
    self.textFieldArray = @[self.homeTeamTextField,
                            self.guestTeamTextField,
                            self.matchDateTextField,
                            self.matchTimeTextField,
                            self.matchTypeTextField,
                            self.ruleStopedWatchTextField,
                            self.ruleHalfTimeTextField,
                            self.ruleExtraTimeTextField];
    
    [self.saveButton setEnabled:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self animeData];
    
    [_homeTeamTextField becomeFirstResponder];
    [_matchTypeTextField setText:@"五人制"];
    [_ruleStopedWatchTextField setText:@"不停表"];
    self.matchDate = [NSDate date];
}

-(void)animeData{
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCancel)];
    [self.view addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    
    [UIView animateWithDuration:__animationDuration * 2 animations:^{
        [contentView setTransform:CGAffineTransformMakeTranslation(0, -SCREEN_HEIGHT+(iPad?40:80))];
        self.view.backgroundColor = UIColorWithRGB_Alpha(0x000000, .6);
    }];
    
    self.presentingViewController.view.layer.anchorPoint = CGPointMake(0.5, 1);
    [self.presentingViewController.view setCenter:CGPointMake(iPad?SCREEN_HEIGHT/2:SCREEN_WIDTH/2, iPad?SCREEN_WIDTH:SCREEN_HEIGHT)];
    [UIView animateWithDuration:__animationDuration animations:^{
        
        CATransform3D transform = CATransform3DIdentity;
        transform.m34 = 1.0/-800;
        transform = CATransform3DRotate(transform, 15.0f * M_PI / 180.0f, 1.0f, 0.0f, 0.0f);
        self.presentingViewController.view.layer.transform = transform;
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:__animationDuration - 0.5 animations:^{
            CATransform3D transform = self.presentingViewController.view.layer.transform;
            transform.m34 = 1.0/-800;
            transform = CATransform3DRotate(transform, -15.0f * M_PI / 180.0f, 1.0f, 0.0f, 0.0f);
            transform = CATransform3DTranslate(transform, 0, iPhone4s?-90:iPhone5?-115:iPhone6?-140:-140, -200);
            self.presentingViewController.view.layer.transform = transform;
        }];
        
    }];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isMemberOfClass:[self.view class]]){
        return YES;
    }
    return NO;
}

-(void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    [UIView animateWithDuration:__animationDuration * 2 animations:^{
        [contentView setTransform:CGAffineTransformIdentity];
        [self.view setBackgroundColor:UIColorWithRGB_Alpha(0x000000, 0)];
    } completion:^(BOOL finished) {
        if (finished) {
            [super dismissViewControllerAnimated:flag completion:completion];
        }
    }];
    
    
    self.presentingViewController.view.layer.anchorPoint = CGPointMake(0.5, 1);
    [self.presentingViewController.view setCenter:CGPointMake(iPad?SCREEN_HEIGHT/2:SCREEN_WIDTH/2, iPad?SCREEN_WIDTH:SCREEN_HEIGHT)];
    [UIView animateWithDuration:__animationDuration animations:^{
        
        CATransform3D transform = self.presentingViewController.view.layer.transform;
        transform.m34 = 1.0/-800;
        transform = CATransform3DTranslate(transform, 0, iPhone4s?90:iPhone5?115:iPhone6?140:140, 200);
        transform = CATransform3DRotate(transform, 15.0f * M_PI / 180.0f, 1.0f, 0.0f, 0.0f);
        self.presentingViewController.view.layer.transform = transform;
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:__animationDuration - 0.5 animations:^{
            self.presentingViewController.view.layer.transform = CATransform3DIdentity;
        } completion:^(BOOL finished) {
            
        }];
        
    }];
    
}

-(void)tappedCancel
{
    [self dismissViewControllerAnimated:NO completion:nil];
}


#pragma mark - Private Method
-(void)previouButtonDidClicked:(UIButton*)button
{
    __block NSInteger index = 0;
    [self.textFieldArray enumerateObjectsUsingBlock:^(UITextField * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.isFirstResponder) {
            [obj resignFirstResponder];
            index = idx;
            *stop = YES;
        }
    }];
    
    if (index == 0) {
        index = _textFieldArray.count - 1;
    }else{
        index --;
    }
    [_textFieldArray[index] becomeFirstResponder];
}

-(void)nextButtonDidClicked:(UIButton*)button
{
    __block NSInteger index = 0;
    [self.textFieldArray enumerateObjectsUsingBlock:^(UITextField * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.isFirstResponder) {
            [obj resignFirstResponder];
            index = idx;
            *stop = YES;
        }
    }];
    
    if (index == _textFieldArray.count - 1) {
        index = 0;
    }else{
        index ++;
    }
    [_textFieldArray[index] becomeFirstResponder];
}

#pragma mark - Getter

-(UIView *)topView
{
    if (!_topView) {
        _topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
        [_topView setBackgroundColor:[[UIColor defaultBackGroundColor] colorWithAlphaComponent:0.86]];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
        [titleLabel setText:@"输入主队名称"];
        [titleLabel setFont:[UIFont systemFontOfSize:20.0]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setTextColor:[UIColor lightThemeColor]];
        [_topView addSubview:titleLabel];
        [titleLabel setCenterX:_topView.width/2];
        self.currentTitleLabel = titleLabel;
        
        UIButton *previousBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [previousBtn setFrame:CGRectMake(0, 0, 60, 40)];
        [previousBtn setTitle:@"上一项" forState:UIControlStateNormal];
        [previousBtn setTitleColor:[UIColor lightThemeColor] forState:UIControlStateNormal];
        [previousBtn setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.55] forState:UIControlStateDisabled];
        [previousBtn setTitleColor:[[UIColor lightThemeColor] colorWithAlphaComponent:0.55] forState:UIControlStateHighlighted];
        [previousBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [previousBtn bk_addEventHandler:^(id sender) {
            [self previouButtonDidClicked:sender];
        } forControlEvents:UIControlEventTouchUpInside];
        [_topView addSubview:previousBtn];
        self.previousButton = previousBtn;
        
        UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [nextBtn setFrame:CGRectMake(_topView.width - 60, 0, 60, 40)];
        [nextBtn setTitle:@"下一项" forState:UIControlStateNormal];
        [nextBtn setTitleColor:[UIColor lightThemeColor] forState:UIControlStateNormal];
        [nextBtn setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.55] forState:UIControlStateDisabled];
        [nextBtn setTitleColor:[[UIColor lightThemeColor] colorWithAlphaComponent:0.55] forState:UIControlStateHighlighted];
        [nextBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [nextBtn bk_addEventHandler:^(id sender) {
            [self nextButtonDidClicked:sender];
        } forControlEvents:UIControlEventTouchUpInside];
        [_topView addSubview:nextBtn];
        self.nextButton = nextBtn;
    }
    return _topView;
}

-(UITextField *)homeTeamTextField
{
    if (!_homeTeamTextField) {
        _homeTeamTextField = [self newTextfieldWithFrame:CGRectMake(0, _headingLabel.y+_headingLabel.height + 10, contentView.width, 50) placeholder:@"请输入主队名称" leftTitle:@"主队："];
        [_homeTeamTextField setBackground:[UIImage imageFromColor:UIColorWithRGB_Alpha(0x000000, 0.86)]];
    }
    return _homeTeamTextField;
}
-(UITextField *)guestTeamTextField
{
    if (!_guestTeamTextField) {
        _guestTeamTextField = [self newTextfieldWithFrame:CGRectMake(0, _homeTeamTextField.y+_homeTeamTextField.height, contentView.width, 50) placeholder:@"请输入客队名称" leftTitle:@"客队："];
        [_guestTeamTextField setBackground:[UIImage imageFromColor:UIColorWithRGB_Alpha(0xFFFFFF, 0.06)]];
    }
    return _guestTeamTextField;
}

-(UITextField *)matchDateTextField
{
    if(!_matchDateTextField)
    {
        _matchDateTextField = [self newTextfieldWithFrame:CGRectMake(0, _guestTeamTextField.y+_guestTeamTextField.height,  contentView.width/2 + 20, 50) placeholder:@"请选择比赛日期" leftTitle:@"日期："];
        [_matchDateTextField setBackground:[UIImage imageFromColor:UIColorWithRGB_Alpha(0x000000, 0.86)]];
        [_matchDateTextField setClearButtonMode:UITextFieldViewModeNever];
        [_matchDateTextField setInputView:self.datePicker];
    }
    return _matchDateTextField;
}

-(UITextField *)matchTimeTextField
{
    if(!_matchTimeTextField)
    {
        _matchTimeTextField = [self newTextfieldWithFrame:CGRectMake(_matchDateTextField.x+_matchDateTextField.width, _guestTeamTextField.y+_guestTeamTextField.height,  contentView.width/2-20, 50) placeholder:@"请选择时间" leftTitle:@"时间："];
        [_matchTimeTextField setBackground:[UIImage imageFromColor:UIColorWithRGB_Alpha(0x000000, 0.86)]];
        [_matchTimeTextField setClearButtonMode:UITextFieldViewModeNever];
        [_matchTimeTextField setInputView:self.datePicker];
    }
    return _matchTimeTextField;
}

-(UITextField *)matchTypeTextField
{
    if(!_matchTypeTextField)
    {
        _matchTypeTextField = [self newTextfieldWithFrame:CGRectMake(0, _matchTimeTextField.y+_matchTimeTextField.height,contentView.width/2, 50) placeholder:@"比赛类型" leftTitle:@"类型："];
        [_matchTypeTextField setBackground:[UIImage imageFromColor:UIColorWithRGB_Alpha(0xFFFFFF, 0.06)]];
        [_matchTypeTextField setClearButtonMode:UITextFieldViewModeNever];
        [_matchTypeTextField setInputView:self.pickerView];
    }
    return _matchTypeTextField;
}

-(UITextField *)ruleStopedWatchTextField
{
    if(!_ruleStopedWatchTextField)
    {
        _ruleStopedWatchTextField = [self newTextfieldWithFrame:CGRectMake(_matchTypeTextField.x+_matchTypeTextField.width, _matchTimeTextField.y+_matchTimeTextField.height,contentView.width/2, 50) placeholder:@"是否停表" leftTitle:@"停表："];
        [_ruleStopedWatchTextField setBackground:[UIImage imageFromColor:UIColorWithRGB_Alpha(0xFFFFFF, 0.06)]];
        [_ruleStopedWatchTextField setClearButtonMode:UITextFieldViewModeNever];
        [_ruleStopedWatchTextField setInputView:self.pickerView];
    }
    return _ruleStopedWatchTextField;
}

-(UITextField *)ruleHalfTimeTextField
{
    if(!_ruleHalfTimeTextField)
    {
        _ruleHalfTimeTextField = [self newTextfieldWithFrame:CGRectMake(0, _ruleStopedWatchTextField.y+_ruleStopedWatchTextField.height,contentView.width/2, 50) placeholder:@"常规半场时间" leftTitle:@"min："];
        [_ruleHalfTimeTextField setBackground:[UIImage imageFromColor:UIColorWithRGB_Alpha(0x000000, 0.86)]];
        [_ruleHalfTimeTextField setKeyboardType:UIKeyboardTypeNumberPad];
    }
    return _ruleHalfTimeTextField;
}

-(UITextField *)ruleExtraTimeTextField
{
    if(!_ruleExtraTimeTextField)
    {
        _ruleExtraTimeTextField = [self newTextfieldWithFrame:CGRectMake(_ruleHalfTimeTextField.x+_ruleHalfTimeTextField.width, _ruleStopedWatchTextField.y+_ruleStopedWatchTextField.height,contentView.width/2, 50) placeholder:@"加时半场时间" leftTitle:@"min："];
        [_ruleExtraTimeTextField setBackground:[UIImage imageFromColor:UIColorWithRGB_Alpha(0x000000, 0.86)]];
        [_ruleExtraTimeTextField setKeyboardType:UIKeyboardTypeNumberPad];
    }
    return _ruleExtraTimeTextField;
}

-(UIDatePicker *)datePicker
{
    if (!_datePicker) {
        _datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 200)];
        [_datePicker setDatePickerMode:UIDatePickerModeDate];
        [_datePicker setDate:[NSDate date]];
        [_datePicker addTarget:self action:@selector(dateChangeValue:) forControlEvents:UIControlEventValueChanged];
    }
    return _datePicker;
}

-(UIPickerView *)pickerView
{
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 200)];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
    }
    return _pickerView;
}

#pragma mark - UIPickerViewDelegate & DataSource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return (_matchTypeTextField.isFirstResponder)?3:(_ruleStopedWatchTextField.isFirstResponder)?2:0;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    self.titleArray = (_matchTypeTextField.isFirstResponder)?[NSArray arrayWithObjects:@"五人制",@"七人制",@"十一人制",nil]:(_ruleStopedWatchTextField.isFirstResponder)?[NSArray arrayWithObjects:@"不停表",@"停表",nil]:@[];
    return _titleArray[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([_currentTextField isEqual:_matchTypeTextField]) {
        [_matchTypeTextField setText:_titleArray[row]];
    }else if([_currentTextField isEqual:_ruleStopedWatchTextField]){
        [_ruleStopedWatchTextField setText:_titleArray[row]];
    }
}
#pragma mark - UITextField

-(UITextField*)newTextfieldWithFrame:(CGRect)frame placeholder:(NSString*)placeholder leftTitle:(NSString*)leftTitle
{
    UITextField* _textField = [[UITextField alloc]initWithFrame:frame];
    NSAttributedString *plaString = [[NSAttributedString alloc]initWithString:placeholder attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18.0],NSForegroundColorAttributeName:UIColorWithRGB_Alpha(0xFFFFFF, 0.35)}];
    [_textField setAttributedPlaceholder:plaString];
    [_textField setTextAlignment:NSTextAlignmentLeft];
    [_textField setFont:[UIFont systemFontOfSize:20.0]];
    [_textField setBorderStyle:UITextBorderStyleNone];
    [_textField setInputAccessoryView:self.topView];
    [_textField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_textField setTextColor:UIColorWithRGB_Alpha(0x40C0FC, 1)];
    [_textField setTintColor:UIColorWithRGB_Alpha(0x40C0FC, 1)];
    _textField.delegate = self;
    
    UILabel *leftTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [leftTitleLabel setText:leftTitle];
    [leftTitleLabel setTextColor:[UIColor lightThemeColor]];
    [leftTitleLabel setFont:[UIFont systemFontOfSize:14]];
    [leftTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [_textField setLeftViewMode:UITextFieldViewModeAlways];
    [_textField setLeftView:leftTitleLabel];
    return _textField;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.currentTextField = textField;
    if ([textField isEqual:_matchDateTextField]) {
        [self.datePicker setDatePickerMode:UIDatePickerModeDate];
    }else if([textField isEqual:_matchTimeTextField]){
        [self.datePicker setDatePickerMode:UIDatePickerModeTime];
    }else if([textField isEqual:_matchTypeTextField]||[textField isEqual:_ruleStopedWatchTextField])
    {
        [self.pickerView reloadAllComponents];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

-(void)textFieldDidChange:(UITextField*)textField
{
    __block NSInteger flag = 0;
    [_textFieldArray enumerateObjectsUsingBlock:^(UITextField * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.text trimText].length == 0 && ![obj isEqual:_ruleExtraTimeTextField]) {
            [_saveButton setEnabled:NO];
        }else{
            if ([_homeTeamTextField.text trimText].length > 3 && [_guestTeamTextField.text trimText].length > 3 && ![obj isEqual:_ruleExtraTimeTextField]) {
                flag++;
            }else{
                [_saveButton setEnabled:NO];
            }
        }
    }];
    if (flag >= [_textFieldArray count] - 1) {
        [_saveButton setEnabled:YES];
    }
}

#pragma mark - Setter
-(void)setCurrentTextField:(UITextField *)currentTextField
{
    _currentTextField = currentTextField;
    [self.currentTitleLabel setText:[_currentTextField.attributedPlaceholder string]];
}

-(void)setMatchDate:(NSDate *)matchDate
{
    _matchDate = matchDate;
    NSString *dateString = [NSDate timeStringFromDateWithoutSecond:_matchDate];
    [_matchDateTextField setText:[dateString componentsSeparatedByString:@" "][0]];
    [_matchTimeTextField setText:[dateString componentsSeparatedByString:@" "][1]];
}

#pragma mark - targetAction
-(void)dateChangeValue:(UIDatePicker*)datePicker
{
    self.matchDate = datePicker.date;
}

@end
