//
//  UIColor+appColor.m
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import "UIColor+appColor.h"

@implementation UIColor (appColor)
+(UIColor *)defaultBackGroundColor
{
    return UIColorWithRGB_Alpha(0xFFFFFF, 1.0);
}

+(UIColor *)darkThemeColor
{
    return UIColorWithRGB_Alpha(0x404040, 1.0);
}

+(UIColor *)lightThemeColor
{
    return UIColorWithRGB_Alpha(0xFD4D01, 1.0);
}

+(UIColor *)unableColor
{
    return UIColorWithRGB_Alpha(0xD8D8D8, 1.0);
}
+(UIColor *)defaultTextColor
{
    return UIColorWithRGB_Alpha(0x3A3A3A, 1.0);
}
@end
