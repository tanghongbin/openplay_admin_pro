//
//  AXMasterTableViewCell.h
//  uptownfunk
//
//  Created by Kelvin Tong on 16/4/18.
//  Copyright © 2016年 sponialtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXMatch.h"
#import "AXTeam.h"
#import "OPFVenue.h"
#import "OPFStadium.h"

@interface AXMasterTableViewCell : UITableViewCell
@property (nonatomic, strong) AXMatch *match;
@end
